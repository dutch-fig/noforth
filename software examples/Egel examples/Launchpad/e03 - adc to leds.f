( Analog to digital conversion with onboard ADC on MSP430 Launchpad )
( for noForth C&V2553 lp.0, Port output with MSP430G2553 at port-2. )

( * Port-2 must be wired to 8 leds, placed on the launchpad experimenters kit.
  Wire P2.0 to P2.7 to the anode of eight 3mm leds placed on the breadboard 
  the pinlayout can be found in the hardwaredoc of the launchpad. Connect
  all kathodes to each other, and connect them to ground using a 100 Ohm 
  resistor. 

  * P1.7 is used as analog input, it is wired to a 4k7 potmeter to 3,3 Volt 
  and ground )

( Address 029 p2out, port-2 output with 8 leds
  Address 02A p2dir, port-2 direction register
  Address 02E p2sel, port-2 selection register 
  Address 04A adc10ae0, ADC analog enable 0
  Address 1B0 adc10ctl0, ADC controle register 0
  Address 1B2 adc10ctl1, ADC controle register 1
  Address 1B4 adc10mem, ADC memory  )

hex
: >LEDS  ( b -- )  029 c! ;

\ ADC on and sample time at 64 clocks
: SETUP-ADC ( -- )
    02 1B0 *bic               ( Clear ENC )
    80 04A c!                 ( P1.7 = ADC in )
    1810 1B0 !                ( Sampletime 64 clocks, ADC on )
    ;

\ We need to clear the ENC bit before setting a new input channel
: ADC       ( +n -- u )
    02 1B0 *bic               ( Clear ENC )
    F000 and 80 or 1B2 !      ( Select input, MCLK/5 )
    03 1B0 *bis               ( Set ENC & ADC10SC )
    begin 1 1B2 bit* 0= until ( ADC10 busy? )
    1B4 @                     ( Read result )
    ;

: POTMETER      7000 adc ;	  ( Read level at P1.7 )

: SHOW 		( u -- )          ( Show with VU- or thermometer scale )
    80 /mod                  ( Scale value for 8 leds )
    swap 3F >  -              ( Round to next higher value )
    dup if                    ( Result greater than zero? )
        1 swap 1- lshift      ( Yes, make led position )
        dup 1-  or            ( Fill lower bits )
    then
    >leds                     ( Data to leds )
    ;

: FLASH       ( -- )          ( Visualise startup )
    -1 >leds  100 ms          ( All leds 250 ms on )
    00 >leds  100 ms          ( All leds 250 ms off )
    ;

: SHOW-ADC1   ( -- )          ( Show conversion on leds )
    -1 02A c!  0 02E c!       ( Make P2 all outputs )
    setup-adc  flash
    begin
        potmeter >leds        ( Read ADC and show binary result )
    key? until
    00 >leds				  ( Leds off )
    ;

: SHOW-ADC2      ( -- )       ( Show conversion on leds )
    -1 02A C!  0 02E C!       ( Make P2 all outputs )
    setup-adc  flash
    begin
        potmeter show         ( Read ADC and show VU-result )
    key? until
    00 >leds				  ( Leds off )
    ;

: SHOW-ADC3      ( -- )       ( Show conversion on leds )
    -1 02A C!  0 02E C!       ( Make P2 all outputs )
    setup-adc  flash
    begin
        potmeter .            ( Read ADC and show result on screen )
    key? until
    00 >leds				  ( Leds off )
    ;

freeze

						( End )
