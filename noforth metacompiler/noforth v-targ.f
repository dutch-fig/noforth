\ noforth v target code 20150330
\ copyright (c) 2015, albert nijhof & willem ouwerkerk
\ update april 2015

\    This program is free software: you can redistribute it and/or modify
\    it under the terms of the GNU General Public License as published by
\    the Free Software Foundation, either version 3 of the License, or
\    (at your option) any later version.
\
\    This program is distributed in the hope that it will be useful,
\    but WITHOUT ANY WARRANTY; without even the implied warranty of
\    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\    GNU General Public License for more details.
\
\    You should have received a copy of the GNU General Public License
\    along with this program.  If not, see <http://www.gnu.org/licenses/>.

only forth also definitions
-targ
marker -targ

notrace
hex \ throughout
cv? ?vversion
select-processor
\ hardware labels for msp chips, in host-forth
hex
meta definitions also forth

<<noforth a                                                     \ ===============a
\ voor msp430g2553
0200 to hot      0400 to ram-end
1080 to frozen   c000 to origin   ffde to ivecs
\ constants for flash programming and initialisation
a500 constant fkey              \ access to flash key
0002 constant fwis              \ erase flash command
0010 constant flock             \ close flash access command
0040 constant fwrite            \ write flash command
0128 constant fctrl1            \ flash control registers 1, 2 and 3
012a constant fctrl2
012c constant fctrl3
\ cold start labels
0000 constant intenable         \ basic interrupt setup
0002 constant int-flags
0120 constant wdctrl            \ watchdog
0056 constant dcoctrl           \ rc oscillator
0057 constant clkctrl
0021 constant p1out             \ io registers for port 1
0022 constant p1dir
0026 constant p1sel
0027 constant p1res
0041 constant p1sel2
\ rs232 (uart)
0003 constant uartctrl0         \ flags register
0061 constant uartctrl1         \ for setup of clock
0062 constant uartbaud0         \ baudrate control
0063 constant uartbaud1
0064 constant uartmodctrl       \ ucbrsx bit 2 needs te be set
0067 constant uart-out          \ data registers output and input
0066 constant uart-in
\ 0001 constant rx?              \ char received bit
\ 0002 constant tx?              \ hardware ready to send another char
>>all

<<noforth bde                                                   \ =============bde
0200 to hot      0a00 to ram-end
1000 to frozen   1100 to origin   ffde to ivecs
\ constants for flash programming and initialisation
a500 constant fkey              \ access to flash key
0002 constant fwis              \ erase flash command
0010 constant flock             \ close flash access command
0040 constant fwrite            \ write flash command
0128 constant fctrl1            \ flash control registers 1, 2 and 3
012a constant fctrl2
012c constant fctrl3
\ cold start labels
0000 constant ie1               \ basic interrupt setup
0002 constant ifg1
0003 constant ifg2
0004 constant me1
0005 constant me2
0120 constant wdctrl            \ watchdog
0056 constant dcoctrl           \ rc oscillator
0057 constant clkctrl1
0058 constant clkctrl2
0020 constant p1in              \ io registers for port 1
0021 constant p1out
0022 constant p1dir
0026 constant p1sel
0027 constant p1ren
0029 constant p2out             \ io registers for port 2
002a constant p2dir
0018 constant p3in              \ io registers for port 3
0019 constant p3out
001a constant p3dir
001b constant p3sel
\ 0027 constant p3ren
\ rs232 (uart0)
0070 constant u0ctl             \ flags register
0071 constant u0tctl            \ for setup of clock ????
0074 constant u0br0             \ baudrate control
0075 constant u0br1
0073 constant u0mctl            \ ucbrsx bit 2 needs te be set
0077 constant u0txbuf           \ data registers output and input
0076 constant u0rxbuf
\ rs232 (uart1)
0078 constant u1ctl             \ flags register
0079 constant u1tctl            \ for setup of clock ????
007c constant u1br0             \ baudrate control
007d constant u1br1
007b constant u1mctl            \ ucbrsx bit 2 needs te be set
007f constant u1txbuf           \ data registers output and input
007e constant u1rxbuf
>>all

<<noforth f                                                     \ ===============f
1c00 to hot      2000 to ram-end
18c0 to frozen   c200 to origin   ff7e to ivecs
>>all

<<noforth g                                                     \ ===============g
1c00 to hot      2400 to ram-end
18c0 to frozen   4400 to origin   ff7e to ivecs
>>all

<<noforth fg                                                    \ ===============f
\ voor msp430fr5739
\ cold start labels
0000 constant intenable         \ basic interrupt setup
0002 constant int-flags
0120 constant wdctrl            \ watchdog
0056 constant dcoctrl           \ rc oscillator
0057 constant clkctrl
0021 constant p1out             \ io registers for port 1
0022 constant p1dir
0026 constant p1sel
0027 constant p1res
0041 constant p1sel2
\ rs232 (uart)
05dc constant uartctrl0         \ flags register
0061 constant uartctrl1         \ for setup of clock
0062 constant uartbaud0         \ baudrate control
0063 constant uartbaud1
0064 constant uartmodctrl       \ ucbrsx bit 2 needs te be set
05ce constant uart-out          \ data registers output and input
05cc constant uart-in
\ 0001 constant rx?               \ char received bit
\ 0002 constant tx?               \ hardware ready to send another char
>>all                                                           \ ================

forth definitions

<----
-- compiler security numbers, names are only in the meta compiler
   forth
11 sys\if      see then ahead while
22 sys\begin   see until again
33 sys\do      see ?do loop +loop
44 sys\:       see : ; does> ;code :doer :noname
55 sys\code    see end-code ;code does> codedoer
   assembler
66 sys\if,      see then, ahead, repeat,
77 sys\begin,   see until, again, repeat,
---->
\ -----------------------------------------------------------------------
\ ------------------------ noforth target code --------------------------
\ -----------------------------------------------------------------------

:::noforth:::
\ noop = next routine ------ 15feb2014
\ read ip               function
\ any even number       normal next
\ odd [8001,8fff]       if until while distance [f800,7fe]
\ odd [9001,6fff]       number         in [-3800,37ff]
\ odd [7001,7fff]       ahead again    distance [f800,7fe]
\ forth: code noop
   ip )+ w mov
   #1 w bit   =?
   if,   w )+ pc mov            \ w even normal exit
   then,
   9000 # w cmp   >?
   if,                          \ w odd in [-7fff,-7001] [8001,8fff]
\ ----- if until while
       #0 tos cmp sp )+ tos mov \ flag=?
       =? if,                   \ jump on false
          8801 # w sub          \ w even in [f800,7fe] [-800,7fe]
          w ip add
       then, next
   then,
   7000 # w cmp   >?
   if,                          \ w odd in [-6fff,6fff] [9001,6fff]
\ ----- number
       w rra                    \ number all in [-3800,37ff] [c800,37ff]
       tos sp -) mov
       w tos mov next
   then,                        \ w odd in [7001,7fff]
\ ----- ahead again
   7801 # w sub                 \ w even in [-800,7fe] [f800,7fe]
   w ip add next
forth: header noop origin ,
\ ----- multiply & divide -----
\ --- a'dam
forth: code um* ( x y -- plo phi )    \ tos:y sun,moon:x tos,w:phi,plo
   #0 sun mov   sp ) tos cmp u<eq?
   if,   tos moon mov   sp ) day mov    \ day=lowest
   else, label-amsterdam                \ for ud*s
         sp ) moon mov   tos day mov
   then, #0 w mov   #0 tos mov          \ plo=phi=0
   begin, #1 sr bic   day rrc cs?
       if,   moon w add   sun tos addc          \ x +to prod
       then, moon moon add   sun sun addc       \ x lshift
       #0 day cmp
   =? until,   w sp ) mov   next
   end-code

extra: code du*s      ( xlo xhi y -- plo phi )
   sp )+ sun mov   amsterdam jmp
   end-code

\ --- a'dam r'dam
\ subroutine for um/mod en du/s
   label-amsterdam     \ tos:y sun,moon:xhi,xlo -> sun,moon:rest,quot
   tos sun cmp u<eq?   \ overflow?
   if,   #-1 sun mov   sun moon mov
         rp )+ pc mov ( return )        \ overflow quot=rest=-1
   then, label-rotterdam                \ for du/s no overflow test
   #1 day mov                           \ init counter
   begin, moon moon add   sun sun addc
       cc? if, tos sun cmp
       u<eq? if,
       2swap then,   tos sun sub   #1 moon add
       then,
          day day add cs?               \ bit comes out?
   until, rp )+ pc mov                  \ *** pc-fout?

forth: code um/mod    ( xlo xhi y -- rest quot )
   sp )+ sun mov
   sp ) moon mov
   amsterdam # call
   sun sp ) mov         \ rest
   moon tos mov         \ quot
   next
   end-code

extra: code du/s ( xlo xhi y -- qlo qhi rest )  \ rest in tos!
   #0 sun mov   sp ) moon mov                   \ sun,moon:0,xhi
   rotterdam # call
   moon sp ) mov        \ qhi
   2 sp x) moon mov     \ sun,moon:rest,xlo
   rotterdam # call
   moon 2 sp x) mov     \ qlo
   sun tos mov          \ rest
   next
   end-code

forth: code execute
   tos w mov   sp )+ tos mov   w )+ pc mov   end-code

\ --- a'dam
forth: code exit
   label-amsterdam   rp )+ ip mov   next end-code
extra: code ?exit       ( flag -- )
   #0 tos cmp   sp )+ tos mov
   =? amsterdam label-until,   next end-code \ *** pc-fout?

extra: code dive       ( -- )
   ip w mov   rp ) ip mov   w rp ) mov   next end-code
inside: code sn(        ( -- x )
   tos sp -) mov   ip )+ tos mov
   next end-code        \ see literal

\ ----- doers -----
\ for does> and :doer
   label-doesintro
   tos sp -) mov   w tos mov    \ databody >stack
   ip push
   -2 w x) ip mov       \ cfa@ to ip
   #4 ip add            \ skip 'doesintro pc mov'
   next                 \ execute hi-level does-part

inside: :doer dodoer
( ) label-thingumajig [ ffff , ] ;      \ needs doesintro
\ forward reference. the romallotted cell will be patched with !doer
\ the result is   :doer dodoer !doer ;

inside: codedoer docol   ip push   w ip mov   next end-code
inside: codedoer dobody  tos sp -) mov   w   tos mov   next end-code    \ docreate
inside: codedoer do@     tos sp -) mov   w ) tos mov   next end-code    \ dovar & docon
inside: codedoer do@@   tos sp -) mov   w ) tos mov   tos ) tos mov     \ doval
                         next end-code

extra: header me ' dobody 2 + , myname,
\ ----- input output -----

<<noforth afg                                                   \ =============afg
\ input and output for the launchpad's board.
extra: code key?)      ( -- f )
   tos sp -) mov   #1 uartctrl0 & bit
   #0 tos mov   <>? if,   #-1 tos mov   then,   next end-code
extra: code key)       ( -- c )
   begin,   #1 uartctrl0 & .b bit   <>? until,
   tos sp -) mov   uart-in & tos .b mov   next end-code
extra: code emit)      ( c -- )
   begin,   #2 uartctrl0 & .b bit   <>? until,
   tos uart-out & .b mov   sp )+ tos mov   next end-code
>>all                                                           \ ================

<<noforth b                                                     \ ===============b
\ noforth-b: input and output for the f149 board uart1
\ uart1 = ifg2=03 bit4=10 rx? -- bit5=20 tx?
extra: code key?) ( -- f )
    tos sp -) mov   10 #  ifg2 & .b bit
    #0 tos mov   cs? if,   #-1 tos mov   then,   next end-code
extra: code key)   ( -- c )
    begin,   10 #  ifg2 & .b bit   cs? until,
    tos sp -) mov   u1rxbuf & tos .b mov   next end-code
extra: code emit)   ( c -- )
    begin, 20 #  ifg2 & .b bit  cs? until,
    tos u1txbuf & .b mov   sp )+ tos mov   next end-code
>>all

<<noforth d                                                     \ ===============d
\ noforth-d: input and output for the f149 board uart0
\ uart0 = ifg1=02 bit6=40 rx? -- bit7=80 tx?
extra: code key?) ( -- f )
    tos sp -) mov   40 #  ifg1 & .b bit
    #0 tos mov   cs? if,   #-1 tos mov   then,   next end-code
extra: code key)   ( -- c )
    begin,   40 #  ifg1 & .b bit   cs? until,
    tos sp -) mov   u0rxbuf & tos .b mov   next end-code
extra: code emit)   ( c -- )
    begin, 80 #  ifg1 & .b bit  cs? until,
    tos u0txbuf & .b mov   sp )+ tos mov   next end-code
>>all

<<noforth e                                                     \ ===============e
\ noforth-d: input and output for the f149 board uart0
\ uart0 = ifg1=02 bit6=40 rx? -- bit7=80 tx?
extra: code key?) ( -- f )
    tos sp -) mov   40 #  ifg1 & .b bit
    #0 tos mov   cs? if,   #-1 tos mov   then,   next end-code
extra: code key)   ( -- c )
    begin,   40 #  ifg1 & .b bit   cs? until,
    tos sp -) mov   u0rxbuf & tos .b mov   next end-code
extra: code emit)   ( c -- )
    begin, 80 #  ifg1 & .b bit  cs? until,
    tos u0txbuf & .b mov   sp )+ tos mov   next end-code
>>all

\ ----- inline arguments -----
\ !!! to be used in words that read an inline argument
inside: code inl# ( -- x )      \ see compile(
\ put inline number on stack.
   tos sp -) mov
   rp ) w mov           \ r@
   w )+ tos mov         \ inl#
   w rp ) mov           \ skip inl#
   next end-code
inside: code inls ( -- csa )    \ see s" c" ." abort"
\ inline string -> stack (counted string)
   tos sp -) mov
   rp ) tos mov         \ counted str adr
   tos ) w .b mov       \ count
   #1 w add             \ bytes to be skipped
   #1 w bit             \ odd?
   #0 w addc            \ make even
   w rp ) add           \ skip the string
   next end-code

\ words that read inline arguments at the same level end in "("

\ ----- do loop primitives -----
\ dox = do-index register, only to be used by do-loops.
\ 3 elements on return stack: saved-dox   actual-dif   redoa
\ dif = 8000 - limit   ->   i = dox - dif
\ --- r'dam
inside: code do(        ( lim index -- )
   label-rotterdam                      \ for ?do
   dox push   \ = previous index
   8000 # w mov   sp )+ w sub   w push  \ = 8000 - limit = dif
   #2 ip add   ip push                  \ = redoa \ skip leavea
   tos dox mov   w dox add              \ index + dif = dox = runtime index
   sp )+ tos mov   next end-code
inside: code ?do(                       \ see ?do
   sp ) tos cmp
   =? rotterdam label-until,                  \ -> do(
   #2 sp add   sp )+ tos mov            \ 2drop
   ip ) ip mov next  end-code           \ do not loop -> go(

\ --- r'dam
forth: code leave       ( -- )
   rp ) w mov   #2 w sub   w ) ip mov   \ redoa 2 - @ = leavea
   label-rotterdam                      \ for loop) & unloop
   #4 rp add                            \ throw redoaa & dif
   rp )+ dox mov   next end-code        \ restore dox

forth: header unloop    ( -- )
   rotterdam ,

\ --- a'dam
inside: code loop)      ( -- )  \ see loop
   #1 dox add
   label-amsterdam              \ for +loop)
   100 # sr bit
   =? rotterdam label-until,          \ -> unloop
   rp ) ip mov   next end-code  \ redo
inside: code +loop)     ( x -- )        \ see +loop
   tos dox add
   sp )+ tos mov
   amsterdam jmp end-code       \ -> loop)

\ 3 nested loops on rstack ( idox = dox )
\ | ?dox kdif kredoa | kdox jdif jredoa | jdox idif iredoa |
\ 10   0e   0c       0a   08   06       04   02   00

forth: code i   ( -- i )                \ real index = dox - dif
   tos sp -) mov
   dox tos mov   2 rp x) tos sub        \ i = dox - idif
   next end-code

forth: code j   ( -- j )
   tos sp -) mov
   4 rp x) tos mov   8 rp x) tos sub    \ j = jdox - jdif
   next end-code

<----
forth: code k          ( -- k )
   tos sp -) mov
   0a rp x) tos mov   0e rp x) tos sub  \ k = kdox - kdif
   next end-code
---->

extra: 2        constant cell
forth: 20       constant bl
extra: hot      constant hot
extra: frozen   constant frozen
extra: ram-end  constant r0
extra: origin   constant origin
extra: ivecs    constant ivecs

\ ----- system data -----
\ 'cold-start' data in ram
( ) dictionary-threads cells ramallot

\ v-version
inside: variable pfx-link       \ prefix-list, the 1st var after the threads
inside: variable wid-link       \ wid-list
\ - - -

( ) ' emit) ramhere !   extra: value 'emit
( ) ' key?) ramhere !   extra: value 'key?
( ) ' key)  ramhere !   extra: value 'key
( ) ' noop  ramhere !   extra: value app

inside: value romp
forth: value here

<<noforth afg                                                   \ =============afg
( ) ram-end d0 -  ramhere !   extra: value tib  \ terminal input buffer
( ) ram-end 80 -  ramhere !   extra: value tib/ \ tib-end & s-bottom
( ) ram-end 40 -  ramhere !   extra: value s0   \ s-top & r-bottom
>>all

<<noforth bde                                                   \ =============bde
( ) ram-end 150 - ramhere !   extra: value tib  \ terminal input buffer
( ) ram-end 100 - ramhere !   extra: value tib/ \ tib-end & s-bottom
( ) ram-end 80 -  ramhere !   extra: value s0   \ s-top & r-bottom
>>all

<<noforth g                                                     \ ===============g
( ) ram-end 150 - ramhere !   extra: value tib  \ terminal input buffer
( ) ram-end 100 - ramhere !   extra: value tib/ \ tib-end & s-bottom
( ) ram-end 80 -  ramhere !   extra: value s0   \ s-top & r-bottom
>>all                                                           \ ================
( ) 1c0     ramhere !   extra: value ms)       \ see ms
( ) 7       ramhere !   extra: value ok         \ see .ok
( ) 10      ramhere !   forth: variable base

\ ----- only warm -----
forth: variable state   \ ( -- adr )
forth: variable >in     \ ( -- adr )
extra: value #ib        \ inputbuffer len
extra: value ib         \ formal inputbuffer adr

\ v-version only
inside: value vp        \ pointer to search-stack
( ) 8 ramallot          \ space for search-stack
inside: variable v0     \
\ - - -

extra: value created   \ lfa of last created header in dictionary.
inside: value cp        \ : {fly fp to cp ; : fly} romp to cp ;
inside: variable fp     \ see fhere = circular buffer pointer
inside: value hld       \ see hold <# #>
inside: value err?      \ see ok -- the last system value
( ) ramhere ' here >body @ !

\ ----- memory operations -----
\ for values
inside: code to(   ip )+ w mov   tos w ) mov   sp )+ tos mov   next end-code
inside: code +to(  ip )+ w mov   tos w ) add   sp )+ tos mov   next end-code
inside: code incr( ip )+ w mov   #1 w ) add   next end-code
trace
to#   pfx-for-value to(         \ 0
+to#  pfx-for-value +to(        \ 2
incr# pfx-for-value incr(       \ 4
notrace
\ inside: code !s0
\ ( ) ' s0 >body @ & sp mov   next end-code
inside: code !r0
( ) ram-end # rp mov   next end-code
inside: code r? ( -- n )                \ for .ok
( ) tos sp -) mov   ram-end # tos mov
    rp tos sub   10 # tos sub    next end-code
\ store and fetch
forth: code c!   sp )+ w mov   w tos ) .b mov   sp )+ tos mov   next end-code
forth: code !    sp )+ tos ) mov   sp )+ tos mov   next end-code
\ forth: code 2!   sp )+ tos ) mov   #2 tos add   sp )+ tos ) mov
\                  sp )+ tos mov   next end-code
forth: code +!   sp )+ tos ) add   sp )+ tos mov   next end-code

\ extra: code c+!   sp )+ w mov   w tos ) .b add   sp )+ tos mov   next end-code
\ extra: code 1+!   #1 tos ) add   sp )+ tos mov   next end-code

forth: code c@     tos ) tos .b mov   next end-code
forth: code @      tos ) tos mov   next end-code
\ forth: code 2@   tos )+ w mov   tos )+ sp -) mov
\                  w tos mov   next end-code
forth: code count  tos w mov   w )+ tos .b mov   w sp -) mov   next end-code
extra: code @+     tos w mov w )+ tos mov   w sp -) mov   next end-code

\ ----- return stack -----
forth: code >r    tos push   sp )+ tos mov   next end-code
forth: code r>    tos sp -) mov   rp )+ tos mov   next end-code
forth: code 2>r   sp )+ push   tos push   sp )+ tos mov   next end-code
forth: code 2r>   #4 sp sub   tos 2 sp x) mov   rp )+ tos mov
                  rp )+ sp ) mov   next end-code
forth: code r@    tos sp -) mov   rp ) tos mov   next end-code
forth: code 2r@   #4 sp sub   tos 2 sp x) mov   rp )+ tos mov
                  rp ) sp ) mov   #2 rp sub   next end-code
extra: code rdrop #2 rp add   next end-code

\ ----- stack operations -----
forth: code 2drop   #2 sp add   sp )+ tos mov   next end-code
forth: code 2dup   #4 sp sub   4 sp x) sp ) mov
                   tos 2 sp x) mov   next end-code
forth: code 2nip   sp ) w mov   #4 sp add   w sp ) mov   next end-code
forth: code 2over  #4 sp sub   tos 2 sp x) mov   8 sp x) sp ) mov
                   6 sp x) tos mov   next end-code
forth: code 2swap  4 sp x) w mov   sp ) 4 sp x) mov   w sp ) mov
                   2 sp x) w mov   tos 2 sp x) mov   w tos mov next end-code
\ : 2tuck   2swap 2over ; ( x1 x2 x3 x4 -- x3 x4 x1 x2 x3 x4 )
\ : 2rot    2>r 2swap 2r> 2swap ;
\ : -2rot   2swap 2>r 2swap 2r> ;
\ --- a'dam
forth: code dup    label-amsterdam   tos sp -) mov   next end-code
forth: code ?dup   #0 tos cmp
                   =? amsterdam label-until,
                   next end-code \ *** pc-fout?
forth: code drop   sp )+ tos mov next end-code
forth: code over   sp ) w mov   tos sp -) mov
                   w tos mov   next end-code
forth: code swap   sp ) w mov   tos sp ) mov   w tos mov   next end-code
forth: code tuck   sp ) w mov   w sp -) mov   tos 2 sp x) mov next end-code
forth: code nip    #2 sp add   next end-code
forth: code pick   tos tos add   sp tos add   tos ) tos mov next end-code
forth: code rot    2 sp x) w mov   sp ) 2 sp x) mov   tos sp ) mov
                   w tos mov   next end-code
\ forth: code -rot
\ tos w mov   sp ) tos mov   2 sp x) sp ) mov   w 2 sp x) mov next end-code

\ ----- comparing -----
\ a'dam \ r'dam
forth: code min    sp )+ w mov   tos w cmp
                   label-amsterdam
                   >? if,   w tos mov   then, next end-code
forth: code max    sp )+ w mov   w tos cmp   amsterdam jmp   end-code
forth: code umin   sp )+ w mov   tos w cmp
                   label-rotterdam
                   u>? if,   w tos mov then, next end-code
forth: code umax   sp )+ w mov   w tos cmp   rotterdam jmp   end-code

\ a'dam \ r'dam
forth: code true  tos sp -) mov   label-amsterdam   #-1 tos mov  next end-code
forth: code false tos sp -) mov   label-rotterdam   #0 tos mov   next end-code
forth:
code =   sp )+ tos cmp    =? rotterdam label-until,   amsterdam jmp end-code
code <>  sp )+ tos cmp   <>? rotterdam label-until,   amsterdam jmp end-code
code >   sp )+ tos cmp    >? rotterdam label-until,   amsterdam jmp end-code
code u>  sp )+ tos cmp   u>? rotterdam label-until,   amsterdam jmp end-code
code 0=  #0 tos cmp       =? rotterdam label-until,   amsterdam jmp end-code
code 0<> #0 tos cmp      <>? rotterdam label-until,   amsterdam jmp end-code
code 0<  #0 tos cmp       >? rotterdam label-until,   amsterdam jmp end-code
code <   sp )+ w mov
         tos w cmp        >? rotterdam label-until,   amsterdam jmp end-code
code u<  sp )+ w mov
         tos w cmp       u>? rotterdam label-until,   amsterdam jmp end-code
\ code 0>  #0 w mov
\         tos w cmp       >? rotterdam label-until,   amsterdam jmp end-code

forth: code within
( ) sp ) tos sub   sp )+ sp ) sub   ' u< @ jmp   end-code
forth: code s>d
( ) tos sp -) mov   ' 0< @ jmp   end-code

\ ----- logical operations -----
forth: code and   sp )+ tos and>  next end-code
forth: code or    sp )+ tos bis   next end-code
forth: code xor   sp )+ tos xor>  next end-code
forth: code invert #-1 tos xor>   next end-code

forth: code lshift ( x1 n -- x2 ) \ qqq
   tos w mov
   sp )+ tos mov
   #0 w cmp <>?
   if,   begin,   tos tos add
                  #1 w sub   =?
         until,
   then,
   next end-code
forth: code rshift ( x1 n -- x2 ) \ qqq
   tos w mov
   sp )+ tos mov
   #0 w cmp <>?
   if,   begin,   clrc   tos rrc
                  #1 w sub   =?
         until,
   then,
   next end-code

extra: code ><   tos swpb   next end-code ( swap bytes )

\ 16-bit bitset and bitclear operators (noforth style)
code **bis  ( m a -- )   sp )+ tos ) bis  sp )+ tos mov  next  end-code
code **bic  ( m a -- )   sp )+ tos ) bic  sp )+ tos mov  next  end-code
code **bix  ( m a -- )   sp )+ tos ) xor>  sp )+ tos mov  next  end-code
code bit**  ( m a -- f ) tos ) tos mov  sp ) tos and> #2 sp add  next  end-code

extra: code *bis        ( mask addr -- )
   sp ) tos ) .b bis   #2 sp add   sp )+ tos mov   next end-code
extra: code *bic        ( mask addr -- )
   sp ) tos ) .b bic   #2 sp add   sp )+ tos mov   next end-code
extra: code *bix        ( mask addr -- )
   sp ) tos ) .b xor>  #2 sp add   sp )+ tos mov   next end-code
extra: code bit*        ( mask addr -- x )
   tos ) tos .b mov    sp ) tos .b and>   #2 sp add   next end-code

\ ----- arithmetic -----
\ --- a'dam
forth: code 1+   #1 tos add   next end-code
forth: code 1-   #1 tos sub   next end-code
forth: code 2*   label-amsterdam   tos tos add   next end-code
forth: header cells  amsterdam ,        \ end-code
forth: code 2/   tos rra   next end-code
forth: code +    sp )+ tos add   next end-code
forth: code -    sp )+ w mov   tos w sub   w tos mov   next end-code
forth: code d2*  sp ) sp ) add   tos tos addc   next end-code
forth: code d2/  tos rra   sp ) rrc   next end-code
extra: code du2/ clrc   tos rrc   sp ) rrc   next end-code

\ --- r'dam
forth: code negate
   label-rotterdam   #-1 tos xor>   #1 tos add next end-code
extra: code ?negate     ( x1 y -- x2 )
   #0 tos cmp   sp )+ tos mov
   pos? rotterdam label-until,   next end-code       \ *** pc-fout?
forth: code abs   #0 tos cmp
   pos? rotterdam label-until,   next end-code       \ *** pc-fout?

\ extra: : ?negate        ( x1 y -- x2 )   0< if negate then ;
\ forth: : abs   dup ?negate ;

\ --- r'dam
forth: code dnegate    ( xlo xhi -- ylo yhi )
   label-rotterdam
   #-1 tos xor>   #-1 sp ) xor>   #1 sp ) add   #0 tos addc   next end-code
extra: code ?dnegate
   #0 tos cmp   sp )+ tos mov
   pos? rotterdam label-until,   next end-code       \ *** pc-fout?
forth: code dabs   #0 tos cmp
   pos? rotterdam label-until,   next end-code       \ *** pc-fout?

\ extra: : ?dnegate ( xlo xhi y -- xlo2 xhi2 )   0< if dnegate then ;
\ forth: : dabs   dup ?dnegate ;

forth: code d+   ( dx dy -- dz )
   sp )+ 2 sp x) add   sp )+ tos addc   next end-code

forth: : m*     ( n1 n2 -- d )
   over abs over abs um*
   2swap xor ?dnegate ;

forth: : fm/mod ( d1 n1 -- n2 n3 )
   >r tuck              \ dhi dlo dhi
   dabs r@ abs um/mod   \ dhi r quot
   swap r@ ?negate      \ dhi quot r*
   swap rot r@ xor 0<   \ r quot neg?
   if   negate over     \ r quot* r
   if   1-              \ r quot-1
   r@ rot - swap        \ n-r quot-1
   then
   then rdrop ;
forth: : *      ( x y -- x*y )   m* drop ;
forth: : /mod   >r s>d r> fm/mod ;
forth: : /   /mod nip ;
forth: : mod   /mod drop ;
forth: : */mod   >r m* r> fm/mod ;
forth: : */   */mod nip ;

\ ----- moving -----
forth: code fill        ( c-addr u char -- )
   sp )+ sun mov        \ count
   sp )+ w mov          \ addr
   #0 sun cmp   <>? if,
   begin,   tos w ) .b mov   #1 w add   #1 sun sub
   =? until,   then,
   sp )+ tos mov   next end-code
forth: code move
   sp )+ w mov   sp )+ sun mov  \ sun) -> w)
   #0 tos cmp   <>? if,         \ tos = length
   w sun cmp   u>? if,          \ backward
   tos sun add   tos w add
   begin,   #1 sun sub   #1 w sub
     sun ) w ) .b mov   #1 tos sub
   =? until,
   else,                        \ forward
   begin,   sun )+ w ) .b mov
     #1 w add   #1 tos sub
   =? until,
   then,
   then,
   sp )+ tos mov   next end-code
extra: code bounds      ( a n -- a+n a )
   tos w mov   sp ) tos mov   w sp ) add   next end-code
\ extra: : bounds       ( a n -- a+n a )   over + swap ;

\ high level versie erbij als documentatie voor deze drie woorden ***
\ an 16jul12 -- not the usual parameters!
extra: code skip       ( end adr1 ch -- end adr2 )
   tos day mov   sp )+ tos mov
   ahead,
   begin, 2swap   tos ) day .b cmp
   =? while,   #1 tos add
   then,   sp ) tos cmp
   2swap   u<eq? until, then,   next end-code   \ *** pc-fout?
<----
inside: : skip  ( end adr1 ch -- end adr2 )
   >r
   begin 2dup u>
   while dup c@ r@ =
   while 1+
   again then then rdrop ;
---->

extra: code scan       ( end adr1 ch -- end adr2 )
   tos day mov   sp )+ tos mov
   ahead,
   begin, 2swap   tos ) day .b cmp
   <>? while,   #1 tos add
   then,   sp ) tos cmp
   2swap   u<eq? until,   then,   next end-code         \ *** pc-fout?
<----
inside: : scan  ( end adr1 ch -- end adr2 )
   >r
   begin 2dup u>
   while dup c@ r@ <>
   while 1+
   again then then rdrop ;
---->
extra: code s<> ( a1 n1 a2 n2 -- t/f )
  sp )+ sun mov
  sp )+ day mov
  sp )+ moon mov
  day tos cmp
  #-1 tos mov
  =? if,                                        \ n1=n2 ?
         #0 day cmp
         <>? if,                                \ n1=n2=nonzero ?
                 begin, sun )+ moon ) .b cmp    \ ch1=ch2
                        =? while, #1 moon add
                                  #1 day sub    \ ready?
                 =? until,
         2swap then,                            \ n1=n2=0 ?
         #0 tos mov
                        then,                   \ ch1<>ch2
  then,                                         \ n1<>n2
  next end-code

inside: : {fly fp to cp ;
inside: : fly}
( ) [ ' romp >body @ ] literal to cp ;

\ ----- catch & throw -----
\ a'dam
forth: code throw       \ find catch-frame and restore sp and rp.
   label-amsterdam
   #0 tos cmp =? if, sp )+ tos mov   next   then,
( ) ram-end # w mov
   begin,   w rp cmp u>?        \ rp r0 u<
   while,   rp )+ rp cmp =?
   until,   rp )+ sp mov        \ restore sp
   rp )+ ip mov
   next                         \ throw# in tos
   then,

( ) ' s0 >body @ & sp mov       \ init sp
( ) w rp mov                    \ init rp
( ) ' noop & nxt mov            \ set nxt register
    #0 sp -) mov
( ) lastname # tos mov
( ) label-nocatchframe
( ) ffff # ip mov               \ -> nocatchframe in quit
   next end-code

inside: code ?abort(    ( flag -- )
   ip )+ w mov   #0 tos cmp   <>?
   if,   w tos mov   amsterdam jmp   then,   \ -> throw
   sp )+ tos mov   next   end-code

<----
\ inside: code {catch ( -- )   \ push sp/rp frame
   sp push
   rp w mov   w push           \ rp push is not ok
   next end-code
\ inside: code catch} ( -- 0 )
   #4 rp add                   \ discard sp/sp frame
   tos sp -) mov   #0 tos mov next end-code   \ ok-zero

\ forth: : catch ( xt -- )   {catch execute catch} ;
---->

\ --- a'dam \ --- r'dam
   label-amsterdam              \ {catch
( ) romhere 2 + ,
   sp push
   rp w mov   w push            \ rp push is not ok
   next
   label-rotterdam              \ catch}
( ) romhere 2 + ,
   #4 rp add                    \ discard sp/rp frame
   tos sp -) mov   #0 tos mov next
forth: : catch [ amsterdam  , ] execute [ rotterdam  , ] ;
forth: : abort  ( ? -- )   -1 throw ;

<<noforth abde                                                  \ ============abde
\ ----- flashrom -----
inside: code {w ( -- )          \ activate flash write sequence
   dint
   fkey #   fctrl3 & mov
   fkey fwrite + # fctrl1 & mov
   next end-code
inside: code w}   ( -- )        \ end flash write sequence
   fkey # fctrl1 & mov
   fkey flock + # fctrl3 & mov
   eint
   next end-code
\ blocks are erased in quantities of 0200 or 040 bytes.
inside: code recycle    ( addr -- )     \ recycle one block of flash
   dint
   fkey #   fctrl3 & mov
   fkey fwis + #   fctrl1 & mov
   #0   tos ) mov       ( dummy write to erase )
   sp )+   tos mov
( ) ' w} >body jmp
   end-code
extra: : rom!          ( x a -- )    {w !   w} ;
extra: : romc!         ( ch a -- )   {w c! w} ;
extra: : rommove ( a1 a2 len -- )   bounds ?do   count i romc! loop   drop ;
>>all

<<noforth fg                                                    \ ==============fg
extra: header rom!      ( x a -- )      ' ! >body ,
extra: header romc!     ( ch a -- )     ' c! >body ,
>>all                                                           \ ================

extra: : CHERE  ( -- a )        cp @ ;
inside: : CHERE?  ( -- chere )  cp @ dup ivecs 1- u< ?exit true ?abort ;
forth: : ,      ( x -- )        chere? rom! cell cp +! ;
forth: : C,     ( ch -- )       chere? romc! 1 cp +! ;

\ allot the string a,u. no count, no align.
extra: : m,     ( a u -- )
   4f over u< ?abort bounds ?do   i c@ c,   loop ;

forth: : align  ( -- )  chere 1 and if true c, then ;

\ v-version
extra: code lfa>n       ( lfa -- nfa )   3 # tos add   next end-code
\ --- a'dam
extra: code lfa>        ( lfa -- cfa )
   3 # tos add           \ lfa>n
   tos )+ day .b mov   7f # day and>
   day tos add
   label-amsterdam
   \ #1 tos bit  #0 tos addc  next end-code     \ *** an 12mei2013
   #1 day mov   tos day and>
   day tos add   next end-code
\ - - -

forth: header aligned  amsterdam ,      \ end-code

\ --- a'dam
forth: code >body     label-amsterdam
   #2 tos add   next end-code
forth: header cell+   amsterdam ,       \ end-code
\ : lfa>n   3 + ;        \ v-version
\ : >body   2 + ;
\ : cell+   2 + ;
\ : cell-   2 - ;

extra: code cell-       #2 tos sub   next end-code

\ nieuw 08apr2015
inside: : fhere ( -- a )        \ less than hx 20 free bytes? -> reset buffer
   tib fp @ 20 + u< if here aligned fp ! then fp @ ;   \ 13

\ copy counted string to ram if there is enough space

\ nieuw 08apr2015
inside: : >fhere        ( a n -- csa )  \ save at FHERE (unprotected)
                                        \ or abort if lack of free RAM
        fhere                   \ a n fhere
        tuck c!                 \ a fhere - store count
        tuck                    \ fhere a fhere
        count                   \ fhere a fhere+1 count
        2dup + tib u> ?abort    \ enough free ram?
        move ;  \ 13

\ msb=0 -> immediate,  lsb=1 not immediate
inside: : @imm  ( lfa -- 1|-1 ) lfa>n c@ 7f > 2* 1+ ;

\ v-version only
inside: : @hom ( lfa -- ? )     cell+ c@ 80 < ;
inside: : @voc ( lfa -- wid )   cell+ c@ 7f and ;
\ - - -

\ ----- input/output -----
forth: : key    ( -- c )   'key execute ;
forth: : key?   ( -- t/f ) 'key? execute ;
forth: : emit   ( c -- )   'emit execute ;
forth: : cr     ( -- )     0d emit 0a emit ;
forth: : space  ( -- )     bl emit ;
forth: : spaces ( n -- )   false ?do space loop ;
forth: : type   ( a n -- ) false ?do count emit loop drop ;
\ extra: : bspace       ( -- )   8 bl over emit emit emit ;

forth: : accept         ( a n -- n2 )
   over + >r                    \ r: enda s: a
   dup                          \ s: a a        \ starta & actual-position
   begin key   0d over -        \ s: a a c f    \ continue?
   while 8 over =               \ s: a a c f    \ backspace?
   if   drop 2dup u<            \ s: a a f      \ not 1st position?
   if 8 bl over emit emit emit 1- then  \ s: a a
   else over r@   u<                    \ s: a a c f    \ enough space?
   if bl max dup emit over c! 1+        \ a a
   dup                          \ a a x
   then drop                    \ a a
   then
   repeat rdrop drop - negate ; \ n2

\ ----- number output -----
extra: code >dig        ( n -- ch )
   0a # tos cmp   <eq? if,   7 # tos add   then,
   [char] 0 # tos add   next end-code
<----
extra: : >dig   ( n -- ch )
   dup 0a < 7 and + [char] 0 + ;
---->
inside: code gnirts     ( adr len -- )  \ retro-string, see #>
   sp )+ w mov                          \ w: adr of first chr
   #2 tos cmp u<eq?
   if,   w tos add   #1 tos sub         \ tos: adr of last chr
   begin, tos ) day .b mov
   w ) tos ) .b mov
   day w ) .b mov
   #1 w add   #1 tos sub
   tos w cmp u<eq? ( cs? )              \ halfway?
   until,
   then, sp )+ tos mov   next end-code

forth: : <#     ( -- )          fhere to hld ;
forth: : hold   ( ch -- )       hld c! incr hld ;
forth: : sign   ( hi -- )       0< if [char] - hold then ;
forth: : #      ( lo hi -- lo2 hi2 )    base @ du/s >dig hold ;
forth: : #s     ( lo hi -- 0 0 )        begin # 2dup or 0= until ;
forth: : #>     ( lo hi -- a n )        2drop fhere hld over - 2dup gnirts ;

inside: : du.str        ( du -- a n )   <# #s #> ;

inside: : d.str ( dn -- a n )   tuck dabs <# #s rot sign #> ;
extra: : rtype  ( a n r -- )    2dup min - spaces type ;
extra: : du.    ( du -- )       du.str type space ;     \ los erbij
forth: : d.     ( d -- )        d.str   type space ;
forth: : u.     ( u -- )        false   du. ;
forth: : .      ( n -- )        s>d d.   ;
forth: : u.r    ( u r -- )      >r false   du.str r> rtype ;
forth: : .r     ( n r -- )      >r s>d d.str   r> rtype ;

forth: : decimal   0a base ! ;
forth: : hex       10 base ! ;

inside: : !input        ( ib #ib >in@ -- )   >in !   to #ib   to ib ;
inside: : @input        ( -- ib #ib >in@ )   ib #ib >in @ ;

\ forth: : source         ( -- adr n )   ib #ib ; \ current input buffer

forth: code /string     ( a n i -- a+i n-i )
   tos w mov   sp )+ tos mov   w tos sub   w sp ) add   next end-code

\ forth: : /string        ( a n i -- a+i n-i )   tuck - >r + r> ;

inside: code upc)       ( adr -- )      \ capitalize chr at ramadr
   tos ) day .b mov
   char a 1 - # day cmp <eq?   if,   char z 1 + # day cmp >? if,
   20 # day bic   day tos ) .b mov   then, then,
   sp )+ tos mov   next end-code
<----
inside: : upc) ( adr -- ) \ capitalize chr at ramadr
   dup c@
   dup [char] a [ char z 1+ ] literal within
   if bl - swap c! exit then 2drop ;
---->
extra: : upper ( a n -- )   over + swap ?do i upc) loop ;

\ ----- parsing -----
extra: code ?stack      ( -- )
   #-1 day mov
( ) ' tib/ >body @ & w mov   sp w cmp   u>?             \ s over
( ) if, ' s0 >body @ & w mov   sp w cmp   u<eq?         \ s under
       if,   rp w cmp   u>?                             \ r over
( )       if, ram-end # w mov   rp w cmp   u<eq?   \ r under
   if, next
   then, then, then, then,
   #2 ip mov   pc ip add   next
   ] true ?abort                \ lo -> hi
   [ end-code

inside: : ?base           ( -- )
    base @ [ 61 7 - 30 - ] literal 2 within
( ) if [ ' base >body @ hot - frozen + ] literal @ base !
       true ?abort
    then ;
extra: : ?comp         ( -- )   state @ 0= ?abort ;
inside: : compile(      ( -- )   inl# , ;

forth: : [      ( -- )   false state ! ; immediate
forth: : ]      ( -- )   true state ! ;

\ flyer for state smart words
extra: : flyer         ( -- )
   state @ ?exit
   fhere r> 2>r         \ address of temp. hi-level code
   {fly                 \ set chere to fhere
   ] dive               \ continue caller in fly-mode, then return here.
   postpone exit        \ close temp. code with an exit.
   postpone [ fly}      \ stop compiling & repair chere
   ;    \ this exit jumps to the temp. code

forth: code depth       ( -- n )
( ) ' s0 >body @ & w mov
   sp w sub
   tos sp -) mov
   w tos mov   tos rra   next end-code

inside: : s"(   ( -- a n )      inls count ;
inside: : ."(   ( -- )          inls count type ;
\ inside: : c"(   ( -- csa )      inls ;
\ inside:: abort"(       ( flag -- ) if inls ( -2 ? ) throw then inls drop ;

inside: : .ok   ( -- )
   ?base   ?stack
   err? 0=
   if ok 1 and if state @ if ."  ok" else ."  ok" then then     \ ok ok
      ok 2 and if ." ." depth [char] 0 + emit then              \ depth
   then
   cr
   state @ 0=
   if ok 4 and
      if r? ?dup if 0< if [char] - else [char] + then emit then \ rdepth
         base @ [char] 0 + emit ." )"                   \ base
      then
   then
   ok 0< if err? if 15 else 6 then emit       \ ack/nak
         then 0 to err?
    ;

forth: : refill ( -- t/f )
   false tib ib - ?exit
   >in !   .ok
   tib tib/ over -
   ok 2* 0<
   if 11 emit accept 13 emit     \ xon/xoff
   else accept
   then   to #ib space
   true ;
forth: code parea ( -- a2 a1 ) \ 14
   tos sp -) mov
   ' >in >body @ # w mov
   w )+ tos mov     \ >in @
   w )+ day mov     \ #ib
   w ) tos add      \ a1
   w ) day add      \ a2
   day sp -) mov
   next end-code
forth: : word ( ch -- csa ) \ 19
  >r
  parea        \ a2 a1
  r@ skip tuck \ van a2 van
  r> scan tuck \ van tot a2 tot
label-amsterdam
  1+ umin      \ van tot tot(+1?)
  ib - >in !   \ van tot
  over -       \ a n
  >fhere ;     \ csa
extra: : bl-word ( -- csa ) \ 21
  bl
  >r
  begin parea     \ a2 a1
     r@ skip tuck \ van a2 van
     2dup =       \ nulstring?
  while 2drop drop
     refill 0=
  until fhere dup dup
  then            \ van a2 van
  r> scan tuck    \ van tot a2 tot
amsterdam goto (;)

forth: : char   ( <name> -- ch )   bl-word count 0= ?abort c@ ;
forth: : parse ( ch -- a n ) \ 16
  >r
  parea     tuck \ van a2 van
  r> scan tuck \ van tot a2 tot
  1+ umin      \ van tot tot(+1?)
  ib - >in !   \ van tot
  over - ;
extra: : beyond ( ch -- ) \ 21
  >r
  begin parea     \ a2 a1
     r@ scan tuck \ tot a2 tot
     =            \ nodelim?
  while drop
     refill 0= ?abort
  repeat          \ tot
  rdrop
  1+
  ib - >in ! ;
forth: : (    [char] ) beyond ;   immediate
forth: : .(     ( <txt> -- )   [char] ) parse type ; immediate
extra: : ?abort ( -- ) flyer postpone ?abort(   created lfa>n   , ; immediate

forth: : s"     ( ccc" -- a n | - )
   flyer postpone s"(
   label-rotterdam [char] " parse dup c, m, align ; immediate
forth: : ."     ( ccc" -- )
   flyer postpone ."(  rotterdam goto (;) immediate

\ : c"    ( ccc" -- a | - )  flyer postpone c" ( rotterdam goto (;) immediate
\ : abort"  ( <txt"> -- ) flyer postpone abort"( rotterdam goto (;) immediate

\ print something between parenthesis
extra: : (.)   ( -- )  ." (" dive ." )" ;

\ ----- inputstream -----

inside: code thread     ( bl-word -- adr )      \ thread addr
   tos )+ day .b mov    \ count
   tos )+ tos .b mov    \ ch1
   day tos xor>         \ x
   dictionary-threads 1- # tos and>             \ n   [0,f]
   tos tos add          \ 2n
( ) hot # tos add       \ hot+2n = threada
   next end-code
<----
inside: : thread        ( bl-word -- adr )      \ thread addr
   count swap c@ xor [ dictionary-threads 1- ] literal and 2* hot + ;
---->

inside: : cfok ( cfa -- )       \ fill cfa if empty
   dup @                \ cfa   fffe or ffff or written
   cell+                \ cfa      0 or 1    or written
   dup cell u<          \ 0 or 1 ?
( ) if if   [ ' dobody >body  ] literal         \ ffff
( )    else [ ' do@    >body  ] literal         \ fffe
    then swap rom!
        exit
   then 2drop ;

\ v-version
inside: : name?         ( csa lfa -- csa lfa|0 )
   begin dup
\    while 2dup lfa>n cs<>
   while 2dup lfa>n count 7f and rot count s<>
   while @
   again then then
   ;
inside: : find) ( csa v0 vp -- wa 0 | xt imm )  \ 56
   2>r 0 over           \ csa f?  csa  r: v0 vp
\   dup c@ 0= ?abort                             \ moet dit?
   dup count upper      \ ..  ..  csa
   dup thread @         \ ..  ..  csa lfa
   2r> 2swap
   begin                \ csa f?  v0 vp  csa lfa
      name?             \ csa f?  v0 vp  csa lfa|0
      2>r               \ ..  ..  .. ..  r: csa lfa|0
      r@                                      \ not end-of-list?
   while                \ ..  ..  .. ..  r: csa lfa
      tuck r@ @voc      \ ..  ..  vp v0 vp wid
      scan                                    ( end adr1 ch -- end adr2 )
                        \ ..  ..  vp v0 v'
      tuck <>           \ ..  ..  vp v' <>?     \ wid in order?
      if 2nip 2r@ 2swap \ csa f?' vp v'         \ save actual-lfa into f?
      then swap         \ csa f?  v' vp         \ wid not found
      r@ @hom                                   \ homonyms?
   while
      2dup <>                                   \ order not empty?
   while 2r> @          \ ..  ..  v2 vp  csa lfa2|0
   again then then then \ ..  ..  v2 vp  r: csa lfa|0

   2r> 2drop 2drop      \ csa f?
   dup                                          \ found?
   if nip               \ lfa
      dup lfa>          \ lfa cfa
      dup cfok
      swap @imm         \ cfa imm
   then ;               \ cfa imm | csa 0
forth: : find ( csa -- wa 0 | xt imm )
  v0 vp find) ;
\ - - -

forth: : literal        ( x -- ? )
\   state @ if postpone sn( , then ; immediate
   state @
   if dup 3800 c800 within
   if   postpone sn(
   else 2* 1+
   then ,
   then ; immediate
forth: : 2literal       ( lo hi -- ? )
   state @
   if swap postpone literal postpone literal
   then ; immediate

\ ----- string>number -----
extra: code dig?        ( ch base@ -- n true | char false )
   sp ) w mov           \ ch
   tos day mov          \ base@
   #0 tos mov
   3a # w cmp                   \ 9 <
   <eq? if,   41 # w cmp        \ a >
   u>? if, ( ||| )  next        \ 9..a gap      \ *** pc-fout?
   else,   7 # w sub
   then,
   then,
   30 # w sub                   \ ch-30
   day w cmp                    \ base <
   u>? if,   w sp ) mov   #-1 tos mov   \ ok
   then,
   next end-code

forth: : >number        ( ulo uhi a n -- ulo uhi a n )  \ string>number
   begin dup
   while over c@ base @ dig?
     0= if drop exit then
     >r 2swap base @ du*s
     r> false d+ 2swap
     1 /string
   repeat ;

<---- 18nov13 ---->
\ minus-char allowed as 1st char, comma accepted.
inside: : number?  ( a n -- lo hi false | ? ? true   )  \ signed, 0 = ok
   dup
   if over c@ [char] - = dup >r if 1 /string            \ a n   r: sign
   then
   begin dup while over c@ [char] , = while 1 /string
   again then then
   dup
   if false dup 2swap                                   \ lo hi a+ n-
      begin >number dup while over c@ [char] , =
      while 1 /string dup 0=
      until then then                                   \ lo hi a+ n-
      nip r> swap
      >r ?dnegate r>   0<> exit
   then rdrop
   then true ;
inside: : dnum?  ( a n -- lo hi flag )  \ 0=ok
   number? ?dup ?exit postpone 2literal 0 ;
inside: : snum?  ( a n -- x flag )      \ 0=ok
   number? ?dup ?exit s>d - ?dup ?exit postpone literal 0 ;
extra: : dn     \ double number input
   bl-word count 2dup upper dnum? ?abort ; immediate

\ make newest word findable.
extra: : reveal         \ no problem if used twice on the same word
   created dup lfa>n thread ! ;         \ make findable


<<noforth fg                                                    \ ==============fg
extra: : vec!  rom! ;
>>all                                                           \ ================


<<noforth abde                                                  \ ============abde
inside: : tidy) ( a1 a2 n -- )
   >r   2dup r@ rommove         \ save a1,n at a2
   over recycle                 \ recycle a1-block
   swap r> rommove ;            \ restore a1,n

extra: code romtest     ( -- adr )      \ *** highlevel als commentaar
   tos sp -) mov
   ffde # tos mov   #-1 w mov
   begin,   #2 tos sub   tos ) w cmp   <>? until,
   #2 tos add   next end-code
<----
extra: : romtest        ( -- adr )   ffde begin cell- dup @ until cell+ ;
---->
\ inside: -200 constant -200 ( -rom )
inside: : blocka        ( a1 -- a2 )
   1- -200 ( -rom ) and 200 ( -rom negate ) + ; \ a2 = block after a1
inside: : {vv   ivecs tib/ over negate move ;
inside: : vv}   tib/ ivecs dup negate rommove ;

\ extra: : .byte  ( u -- )  ff and 10 /mod  >dig emit  >dig emit ;
\ extra: : .word  ( u -- )  dup >< .byte .byte ;
extra: : vec!   ( i-routine vectorlocation -- )
   dup ivecs u< ?abort
   2dup @ over and = if rom! exit then
   -200 romp u< ?abort
   {vv  tib/ + ivecs - !
   -200 recycle vv} ;
>>all                                                           \ ================

inside: : tidy  ( -- )

<<noforth abde                                                  \ ============abde
   romtest   romp
   dup  1ff ( -rom invert ) and >r      \ #restbytes in here.block
   2dup u>                              \ not (1)or(2)   h u< testh
   if   blocka swap blocka over
   \ h+ testh+   not(3)   h not in last
   if over -200 ( -rom ) =              \ (4)(6)   h in last-1
     over 0=                            \ (4)(5)   testh in last
     or dup >r                          \ flag: vectors involved?
     if  {vv -200 ( -rom ) recycle
        -200 ( -rom ) or                \ fe00-block already done
     then 2dup swap
     ?do i recycle 200 ( -rom negate ) +loop
     drop r>                            \ h+ vectors?
     r@                                 \ restbytes
     if over   dup -200 ( -rom ) +   over  \ h+ h h+
        r@ tidy)   recycle
     then
     dup if vv} then
   then
   then rdrop 2drop
   romtest  ( cr ." romtest " dup .word )  to romp
>>all                                                           \ ================
\ vectortest
   chere 'emit u< if ['] emit) to 'emit then
   chere 'key? u< if ['] key?) to 'key? then
   chere 'key  u< if ['] key)  to 'key  then
   chere app   u< if ['] noop  to app   then
   ivecs
   begin cell+ dup -2 u<                \ vector location
   while dup @ chere ivecs within       \ forgotten?
         if ivecs over vec! then
   repeat drop ;
extra: : freeze
   hot
   [ ' state 2 + @ hot - ] literal
   frozen over
   s<> 0= ?exit

   hot frozen
   [ ' state 2 + @ hot - ]       \ state is de eerste warm-only
   literal

<<noforth bde                                                   \ =============bde
   >r
   dup r@ + fhere               \ buffera = fhere
   80 r@ -   tidy)
   r> rommove ;
>>all

<<noforth a                                                     \ ===============a
   >r
   dup r@ + fhere               \ buffera = fhere
   40 r@ -   tidy)
   r> rommove ;
>>all

<<noforth fg                                                    \ ==============fg
   move ;
>>all                                                           \ ================
\ ----- ok loop -----
extra: : interpret
   begin bl-word dup c@
   while find dup
     if   state @ = if , else execute then
     else drop count
          snum? ?abort         \ 18nov13
     then
   repeat drop ;
inside: : swallow       ( -- )  \ ignore rest of a file sent without (n)ack
   ok 0< ?exit                                                  \ ***
   cr   c000
   begin 1+ ?dup
   while key? if drop key 0d =
                 if  [char] , emit
                 then c000
              then
   repeat [char] . emit ;

inside: : !created      ( -- )
   false
   hot 10 bounds
   do   i @ umax   cell +loop   to created ;

forth: : quit
   cr 0 to err?                                                 \ ***
   begin !r0            \ empty the return stack
   !created
   tib false dup ( ib #ib >in@ ) !input
   postpone [
   fly}
   ['] interpret catch
   dup -38 = if drop quit then
[
( ) romhere nocatchframe 2 + !
]
   cr >in @ spaces
   dup origin romp within
   if dup 1 and
      if ." msg from " then     \ ?abort nfa = odd
      dup count 7f and 20 min type
   then true over -
   if ."  \ error # " dup u. then       \ type throw# when not -1
   drop
   -1 to err? swallow                                         \ ***
   again
(;)
\ v-version
inside: : order,        ( -- )          \ compile order as counted string
        vp v0 1+ over -         \ a,n
        dup c, m, align ;
inside: : >order        ( adr -- adr2 ) \ restore compiled order
        count                   \ a,n
        v0 1+ over - to vp
        2dup vp swap move
        + aligned ;

inside: :doer doonly    4 + >order drop ;       \ ( only only : only )
inside: :doer dovoc     cell+ c@ vp c! ;
forth: 0 vocabulary only
only: 1 vocabulary forth
only: 2 vocabulary inside
only: 3 vocabulary extra
only: 4 vocabulary assembler

only: : fresh   sn( [ ffff , ]  >order drop ;   \ patched by shield noforth
only: : also    7 v0 vp - < ?abort                      \ overflow?
                vp c@   -1 +to vp   vp c! ;
only: : previous      v0 vp - 2 < ?abort incr vp ;      \ underflow?
only: : definitions   vp c@   v0 c! ;

\ - - -
\ v-version
inside: : create)       \ the new word is not yet linked,
                        \ has an empty cfa.
                        \ reveal is done in ;  end-code  create
                        \ msb in count byte is 1 (not immediate)
   here 1 and if incr here then
   align bl-word                \ csa (at fhere)
   dup c@ 20 1 within ?abort    \ nul-string or too long, is dit nodig?
   dup count upper      \ csa
   dup thread @         \ lfa of previous word
   chere to created     \ for reveal
    dup ,               \ csa lfa
   name?                \ csa name-exists?
   if space dup count type ."  is not new "
      0                 \ name exists (homonym)
   else 80              \ name is unique
   then
   v0 c@ or c,          \ hvoc-byte
   count dup 80 or      \ not immediate
   c,                   \ icount-byte
   m,                   \ name
   align
   true , ;             \ codefield
\ - - -
forth: : create  create) reveal ;

\ v-version
\ curtail cuts cell-link lists
inside: : curtail       ( fence linkholder   --   )     \ see domarker
   tuck @       ahead
   begin @ ( lnk@ )  /then
         2dup u>
   until                \ linkholder fence link u>?
   nip swap ! ;

inside: :doer domarker  \ ramhere and romhere order changed an 07/05/2013
   >order @+ to romp   @ aligned to here
   romp hot
   [ dictionary-threads 2 + ]   \ + pfx-link + wid-link
   literal
   false do 2dup curtail   cell+ loop 2drop  \ xxx
   !created tidy freeze
   ;

only: : marker create domarker order,
   created ( lfa ) ,    \ chere
   here , ;             \ here
only: : shield create domarker order,
   chere cell+ cell+ ,
   here , ;
\ - - -

extra: \ s? ( -- x ) \ x=0 -> switch s2 pressed

<<noforth a     : s?    08 20  bit* ;   \ sw=p3.4       >>all
<<noforth b     : s?    08 18  bit* ;   \ sw=p3.4       >>all
<<noforth d     : s?    08 20  bit* ;   \ sw=p1.4       >>all
<<noforth e     : s?    02 20  bit* ;   \ sw=p1.1       >>all
<<noforth f     : s?    02 221 bit* ;   \ sw=p4.1       >>all
<<noforth g     : s?    20 221 bit* ;   \ sw=p4.5       >>all

<<noforth bde   extra: : !leds  ( u -- )  29 c! ;       >>all   \ =============bde

<---- 18nov13 ---->
inside: : pret?  ( bl-word-find -- flag )    \ 0=ok
   dup
   if state @ =
      if ,
      else execute
      then 0 exit
   then drop count snum? ;

label-amsterdam                 \ cold - hilevel part
   ]

<<noforth abde  w}      >>all           \ flash write off       \ ============abde
   frozen  hot
   state over - move            \ cold users -> warm
   !created                     \ lfa of last build word
   fresh        \ v-version only
   here fp !                    \ reset fhere
   fly}
   s? if app execute then       \ start user application
   tidy
   cr me count type             \ show startup message
   quit
   [
extra: code cold        ( -- )  \ lolevel part
romhere fffe !
\ prepare for executing hilevel forth
amsterdam # ip mov
( ) ' s0 >body @
( ) hot - frozen + & sp mov     \ init sp
( ) ram-end # rp mov            \ init rp
( ) ' noop & nxt mov            \ set nxt register
\ hardware initialisation

<<noforth a     \ launchpad msp430g2553 hardware stuff          \ ===============a
\ stop watchdog & set oscillator freq.
   5a80 # wdctrl & mov          \ stop watchdog
   10fd & clkctrl & .b mov      \ set dco to 8 mhz
   10fc & dcoctrl & .b mov
\ uart setup starts with i/o pin direction
   6 # tos mov
   tos p1sel & .b mov           \ init p1.1 & p1.2
   tos p1sel2 & .b mov
   80 # uartctrl1 & .b bis      \ uart use smclk
   41 # uartbaud0 & .b mov      \ 8 mhz, baudrate 9600
   3 # uartbaud1 & .b mov       \ idem
   #4 uartmodctrl & .b mov      \ modulation ucbrsx = 2
   #1 uartctrl1 & .b bic        \ initialize usci state machine
   #0 intenable & .b mov        \ erase interrupt flags ***
\ flash initialisation
   a550 # fctrl2 & mov          \ mclk/20 for flash timing ***
\ activate i/o ports, leds & switch
   71 # 22 & .b bis             \ was 41, set output dir for p1.0 and p1.6
   79 # 21 & .b bis             \ was 49, set p1.0, 4, 5 en ..
   #8 27 & .b bis               \ p1 pullup p1.3
   #-1 2a & .b bis              \ p2 all outputs
\ updated version for two port launchpad
\   f1 # 22 & .b bis            \ set input dir for p1.1 to p1.3
\   41 # 21 & .b bis            \ set p1.0 and p1.6
\   #8 27 & .b bis              \ activate p1.3 resistor
\   #-1 2a & .b bis             \ p2 to all outputs
>>all

<<noforth b                                                     \ ===============b
    5a80 # wdctrl & mov     \ watchdog uit
    #0 ie1 & .b mov         \ erase interrupt flags

\ activate i/o ports, leds & switch
    40 # p3dir & .b mov     \ output dir for p3
    ff # p2dir & .b mov     \ output dir for p2
\   11 #  32 & .b bis       \ p5.4 & p5.0 = output

\   #1  31 & .b bis         \ p5.0 high
\ klok selectie en test
    80 # clkctrl1 & .b bic  \ xt2 aan
    begin,                  \ wacht tot osc op gang is.
        #2 ifg1 & .b bic    \ reset osc. foutvlag
        ff # sun mov        \ start wachtlus
        begin,
            #-1 sun add
        =? until,
        #2 ifg1 & .b bit    \ stop als de vlag nul is
    cc? until,
    88 # clkctrl2 & .b bis  \ mclk = smclk = xt2
    3c # p2out & .b mov     \ set 4 leds on

\ 8 mhz op p5.4 naar buiten
\   10 #  33 & .b bis       \ p5.4 = mclk option select
\   #1  31 & .b bic         \ p5.0 low

\ rs232
    c0 # p3sel & .b bis     \ use uart-1
    30 # me2 & .b mov       \ uart-1 aan
    10 # u1ctl & .b bis     \ 8-bit karakters
    20 # u1tctl & .b bis    \ uclk = smclk
    41 # u1br0 & .b mov     \ 8 mhz: baudrate 9600
    03 # u1br1 & .b mov     \ idem
    09 # u1mctl & .b mov    \ adjust modulation
\   45 # u1br0 & .b mov     \ 8 mhz: baudrate = 115200
\   #0 u1br1 & .b mov
\   #0 u1mctl & .b mov      \ ...
    #1 u1ctl & .b bic       \ init. uart

\ flash initialisation
    a554 # fctrl2 & mov     \ mclk/xx for flash timing ***
>>all

<<noforth d                                                     \ ===============d
    5a80 # wdctrl & mov     \ watchdog uit
    #0 ie1 & .b mov         \ erase interrupt flags

\ activate i/o ports, leds & switch
\   40 # p3dir & .b mov     \ output dir for p3
    ff # p2dir & .b mov     \ output dir for p2

\   11 #  32 & .b bis       \ p5.4 & p5.0 = output
\   #1  31 & .b bis         \ p5.0 high
\ klok selectie en test
    80 # clkctrl1 & .b bic  \ xt2 aan
    begin,                  \ wacht tot osc op gang is.
        #2 ifg1 & .b bic    \ reset osc. foutvlag
        ff # sun mov        \ start wachtlus
        begin,
            #-1 sun add
        =? until,
        #2 ifg1 & .b bit    \ stop als de vlag nul is
    cc? until,
    88 # clkctrl2 & .b bis   \ mclk = smclk = xt2
    3c # p2out & .b mov      \ set 4 leds on

\ 8 mhz op p5.4 naar buiten
\   10 #  33 & .b bis        \ p5.4 = mclk option select
\   #1  31 & .b bic          \ p5.0 low

\ rs232
    30 # p3sel & .b bis      \ use uart-0
    c0 # me1 & .b mov        \ uart-0 aan
    10 # u0ctl & .b bis      \ 8-bit karakters
    20 # u0tctl & .b bis     \ uclk = smclk
    41 # u0br0 & .b mov      \ 8 mhz: baudrate 9600
    03 # u0br1 & .b mov      \ idem
    09 # u0mctl & .b mov     \ adjust modulation
\   45 # u0br0 & .b mov      \ 8 mhz: baudrate = 115200
\   #0 u0br1 & .b mov
\   #0 u0mctl & .b mov       \ ...
    #1 u0ctl & .b bic        \ init. uart

\ flash initialisation
    a554 # fctrl2 & mov      \ mclk/xx for flash timing ***
>>all

<<noforth e                                                     \ ===============e
    5a80 # wdctrl & mov     \ watchdog uit
    #0 ie1 & .b mov         \ erase interrupt flags

\ activate i/o ports, leds & switch
\   40 # p3dir & .b mov     \ output dir for p3
    ff # p2dir & .b mov     \ output dir for p2

\   11 #  32 & .b bis       \ p5.4 & p5.0 = output
\   #1  31 & .b bis         \ p5.0 high
\ klok selectie en test
    80 # clkctrl1 & .b bic  \ xt2 aan
    begin,                  \ wacht tot osc op gang is.
        #2 ifg1 & .b bic    \ reset osc. foutvlag
        ff # sun mov        \ start wachtlus
        begin,
            #-1 sun add
        =? until,
        #2 ifg1 & .b bit    \ stop als de vlag nul is
    cc? until,
    88 # clkctrl2 & .b bis   \ mclk = smclk = xt2
    06 # p2out & .b mov      \ set 2 leds on

\ 8 mhz op p5.4 naar buiten
\   10 #  33 & .b bis        \ p5.4 = mclk option select
\   #1  31 & .b bic          \ p5.0 low

\ rs232
    30 # p3sel & .b bis      \ use uart-0
    c0 # me1 & .b mov        \ uart-0 aan
    10 # u0ctl & .b bis      \ 8-bit karakters
    20 # u0tctl & .b bis     \ uclk = smclk
    41 # u0br0 & .b mov      \ 8 mhz: baudrate 9600
    03 # u0br1 & .b mov      \ idem
    09 # u0mctl & .b mov     \ adjust modulation
\   45 # u0br0 & .b mov      \ 8 mhz: baudrate = 115200
\   #0 u0br1 & .b mov
\   #0 u0mctl & .b mov       \ ...
    #1 u0ctl & .b bic        \ init. uart

\ flash initialisation
    a554 # fctrl2 & mov      \ mclk/xx for flash timing ***
>>all

<<noforth f     \ msp430fr5739 hardware stuff                   \ ===============f
\ cold code is now 23 instructions and all i/o is initialised
   5a80 # 15c & mov             \ stop watchdog
   a5 # 161 & .b mov            \ set dco to 8 mhz
   6 # 162 & bis
   133 # 164 & mov
   #0 166 & mov
   #1 161 & .b mov
\   #8 1b0 & mov                \ bandgap ref. off (saves power)
   3 # 223 & .b mov             \ p4: switches
   3 # 227 & .b mov
   f8 # 224 & .b mov            \ p3: 4 leds, ldr & accelerometer
   0f # 324 & .b mov            \ pj: 4 leds
   f0 # 326 & .b mov
   f0 # 322 & .b mov
   #-1 206 & .b mov             \ p1: bit 4 is ntc, rest nc
   #-1 202 & .b mov
   80 # 205 & .b mov            \ p2: rs232 & ntc/ldr voeding bit 7
   7f # 203 & .b mov
   7c # 207 & .b mov
   3 # 20d & .b bis             \ set uart function to pins
\ configure uart:
   80 # 5c0 & .b mov            \ select clock source
   34 # 5c6 & .b mov            \ set baud 0
   #0 5c7 & .b mov              \ set baud 1
   4911 # 5c8 & mov             \ set modulation control
>>all

<<noforth g     \ msp430fr5969 hardware stuff                   \ ===============g
\ cold code is now 23 instructions and all i/o is initialised
   5a80 # 15c & mov             \ stop watchdog
   a5 # 161 & .b mov            \ set dco to 8 mhz
   6 # 162 & bis
   133 # 164 & mov
   #0 166 & mov
   #1 161 & .b mov
\   #8 1b0 & mov                \ bandgap ref. off (saves power)

   20 # 223 & .b mov            \ p4.5 switch 1
   20 # 227 & .b mov
   02 # 202 & .b mov            \ p1.1 switch 2
   02 # 206 & .b mov

   40 # 223 & .b bis           \ led 1 on p4.6
   40 # 225 & .b bis
   01 # 202 & .b bis           \ led 2 on p1.0
   01 # 204 & .b bis

\  #fe 206 & .b mov             \ p1: bit 4 is ntc, rest nc
\  #-1 202 & .b mov
\  80 # 205 & .b mov            \ p2: rs232 & ntc/ldr voeding bit 7
\  7f # 203 & .b mov
\  7c # 207 & .b mov
   03 # 20d & .b bis            \ set uart function to pins
\ configure uart:
   80 # 5c0 & .b mov            \ select clock source
   34 # 5c6 & .b mov            \ set baud 0
   #0 5c7 & .b mov              \ set baud 1
   4911 # 5c8 & mov             \ set modulation control
>>all                                                           \ ================

next end-code   \ jump to cold - hilevel part

forth: : evaluate       ( a n -- )
   @input ( ib #ib >in@ ) >r 2>r
   false ( ib #ib >in@ ) !input
   ['] interpret catch
   2r> r> ( ib #ib >in@ ) !input
   throw ;
forth: : '      ( <name> -- xt | abort )   bl-word find 0= ?abort ;
forth: : [char] ( <name> -- )   char postpone literal ; immediate
extra: : ch     ( <name> -- )   char postpone literal ; immediate

\ extra: : ctrl   char 1f and postpone literal ; immediate

forth: : \      ( <tekst> -- )   #ib >in ! ;   immediate

\ nieuw 07apr2015
forth: : allot  ( n -- )        \ 07apr2015
   here over +   tib 20 - u> ?abort
   created lfa> @+ + 1+ chere =         \ cfa@ is -1 and chere is pfa
   if -2 chere cell- rom!
   here ,
   then +to here
   fp @ here umax aligned fp !  ;       \ 32

forth: : :      ( <name> -- sys\: )
   create)
( ) [ ' docol >body ] literal
   chere cell- dup s0 ! rom!
   0 44 ] ;             \ s0 for recurse
forth: : constant       ( x <name> -- )   create do@ , ;        \ *** an 12jun13
forth: : variable       ( <name -- )   create cell allot ;

forth: : value  ( <name> -- )   variable do@@ ;
forth: : code   ( <name> -- sys\code )
   create) chere -2 cp +! , 0 55 assembler ;    \ v-version
\ v-version only
forth: : wordlist      ( -- wid )
                wid-link @   dup        \ top-link
                chere wid-link !
                ,                       \ link
                cell+ @ 1+ dup c,       \ new wid
                align ;
only: : vocabulary    create dovoc wordlist drop
                chere created lfa>n - chere 1-
                romc! ;
extra: : .voc          ( voc# -- )
                wid-link @
                begin ?dup 0= if ." voc#" . exit then
                      2dup cell+ c@ <>   while @
                again then              \ voc# voc-cfa
                3 + count
                ff over =
                if 2drop ." voc#" .
                else - count 7f and type space drop
                then ;
only: : order         v0 vp - 9 1 within if fresh then
                (.)   space v0 vp
                begin  2dup > while  dup c@ .voc 1+ repeat
                drop ." : " c@ .voc ;                   \ .current
\ - - -

\ v-version
\   | link | pfx-id | action-xt(-1?) |
inside: :doer doprefix  ( pfx# <name> -- )
   @ ' >r                       \ pfx# data-xt
   r@ @ +                       \ pfx-id
   pfx-link @                   \ pfx-id lfa
   begin dup 0= ?abort
         2dup cell+ @ <>
   while @
   repeat nip                   \ lfa
   cell+ cell+ @                \ action ( xt-1 if imm )
   r> >body swap                \ rombody action
   dup aligned tuck =           \ rombody action even=not-imm?
   flyer
   if , @ ,                     \ compile action and ramlocation
   else execute                 \ execute action ( with rombody on stack)
   then ;
\ - - -

\ global prefixes
forth: to#    prefix to   \ 0
forth: +to#   prefix +to  \ 2
forth: incr#  prefix incr \ 4

\ ----- conditionals -----

\ --- a'dam
forth: : if     ( -- a sys\if ) chere 8fff , 11 ; immediate
forth: : ahead  ( -- a sys\if ) chere 7fff , 11 ; immediate

\ ---
forth: : while  ( a x-id -- a sys\if a x-id )   postpone if 2swap ; immediate
inside: : dist ( x -- )    801 - dup -1000 u< ?abort ;
extra: : ?pair ( x y -- )      - ?abort ;

forth: : then   ( a sys\if -- )  \ &&&
   ?comp 11 ?pair
   chere over -         \ distance, positive
   dist                 \ [f800,0)
   over @ and swap rom!
   ; immediate

forth: : begin  ( -- a sys\begin ) ?comp chere 22 ; immediate
forth: : until  ( a sys\begin -- )
   8fff
   label-amsterdam
   >r  22 ?pair
   chere -              \ distance, negative
   dist                 \ [f000,f800)
   r>  and ,  ; immediate
forth: : again  ( a sys\begin -- )
   7fff amsterdam goto (;) immediate
forth: : else   ( a sys\if1 -- a sys\if2 )
   postpone ahead 2swap postpone then ; immediate
forth: : repeat ( a sys\if a sys\begin -- )
   postpone again postpone then ; immediate

\ --- a'dam
forth: : do     ( -- adr sys\do )
   postpone do(
   label-amsterdam
   chere 33 true , ; immediate
forth: : ?do    ( -- adr sys\do )
   postpone ?do(   amsterdam goto (;) immediate

\ --- a'dam
forth: : loop   ( sys\do -- )
   postpone loop)
   label-amsterdam
   33 ?pair   chere swap rom!
   ; immediate

forth: : +loop  ( sys\do -- )
   postpone +loop)   amsterdam goto (;) immediate
\ ---

forth: : ;      ( sys\: -- )
   44 ?pair postpone exit   postpone [
   [ immediate ]   \ !!! to prevent ; to be compiled at the end of this word
   ?exit reveal ;               \ :noname has sys = -1 44
forth: : end-code       ( sys\code -- )
   previous also 55 ?pair ?exit reveal ;        \ v-version

inside: : !doer ( doera -- )
   created lfa>
   dup @ -2 u< ?abort   \ cf already filled
   rom! ;
( ) ' !doer thingumajig !       \ patch in dodoer (forward reference)

forth: : immediate
   created lfa>n 7f over c@ and swap romc! ;

inside: :doer doffbase  base @ >r   @ base !
   bl-word find pret? r> base ! ?abort ;
extra: : ffbase ( tempbase <name> -- ) create , doffbase immediate ;

inside: : does>)        ( -- ) r> !doer ;
\ inside: : does,       ( -- )
\ 4030 ( # mov pc ) , [ doesintro ] literal , ;
forth: : does>  ( sys\: -- sys\: )
   dup 44 ?pair   postpone does>)
   4030 ( # pc mov ) , [ doesintro ] literal ,    \ does,
   0 s0 !           \ for recurse
   ; immediate

forth: : [']    ( <name> -- )   ' postpone literal ; immediate
forth: : postpone       ( <name> -- )
   ?comp
   bl-word find dup 0= ?abort
   0< if compile( compile(   then   , ; immediate

forth: : ms    ( n -- ) 0 ?do ms) 0 ?do loop  loop ;
forth: 10 ffbase hx
forth: 0a ffbase dm
forth: 02 ffbase bn
only: : v: ; immediate
only: : c: postpone \ ; immediate

\ v-version
forth: shield noforth
\ - - -
\ ---------------------------------- noforth v tools

extra: : stop?     ( - true/false )
   key? dup 0= ?exit  drop
   key  bl over = if drop key then
   01b over = ?abort
   bl <> ;
\ .ch is needed for dump msee & see
extra: : .ch   ( x -- ) dup 7f < and bl max emit ;

forth: : .s        ( -- )
   ?stack (.) space
   depth false
   ?do  depth i - 1- pick
      base @ 0a = if . else u. then
   loop ;

\ ----- words
\ v-version only
only: : words     ( -- )
   false >r                     \ counting
   fhere pfx-link hot -  hot dup
   2over move drop              \ threads > fhere
   bounds
   cr
   begin false dup              \ threada lfa
      2over
      do
         dup i @ u< if 2drop i dup @ then       \ threada lfa
      2 +loop dup stop? 0= and
   while        \ threada lfa
        dup @voc vp c@ =        \ the right vocabulary?
        if r> 1+ >r             \ counting
           dup lfa>n count 1f and
           0c over 2swap space type
           - dup 0<   r@ 6 mod 0=   or       \ too long or too many
           if drop cr else spaces then
        then
        @ swap !          \ unlink
   repeat
   2drop 2drop r> (.) 0 .r  ;
\ - - -

forth: : dump  ( a n -- )       \ n must be in [0,7fff]
   over + 1- >r                                 \ r: enda-1
   0ff s>d du.str nip 1+ swap                   \ .r a
   base @ swap
   begin cr dup 2over drop 2* u.r ." : "        \ .r base a
      over s>d do count 2over drop .r
               loop over - ."  |"               \ .r base a
      over s>d do count .ch loop ." |"          \ .r base a+
      r@ over - 0< stop? or
   until r> 2drop 2drop ;

\ ----- see & msee
extra: : >nfa          ( a -- nfa | 0 )
   dup origin
   chere within                 \ in noforth rom?
   if dup 1 and 0=              \ even?
   if 1- dup c@ ff = +          \ skip alignment char
   0 begin over c@ 21 7f within \ char?
     while -1 /string 20 over < \ walk backwards through name
     until then ?dup            \ nfa? count-non-zero?
   if over c@ 7f and =          \ count ok?
   if dup 1 and ?exit           \ nfa odd -> ok
   then then then then drop 0 ;
extra: : decom         ( a -- )
   cr dup 6 u.r space   \ .adr
   dup count .ch c@ .ch \ .2chars
   dup @ 6 u.r space    \ .contents
   dup >nfa ?dup
   if ." ----- " dup 1- c@ 7f and .voc  \ v-version only
      count tuck 7f and type            \ .name
      80 < if ."  (imm)" then
      @ cell- >nfa ?dup
      if ."  does " count 7f and type   \ .doer
      then exit                         \ ----
   then dup @                           \ contents
   dup 1 and                            \ odd
   if dup -7800 < if ." until -" else
      dup -7000 < if ." if +"   else
      dup  7000 < if 2/ ." #" u. drop exit then    \ a number
      dup  7800 < if ." again -" else ." ahead +"
      then then then fff and 7ff - + ." > " u. exit
   then nip   dup >nfa ?dup
   if count 7f and type drop exit       \ .name of compiled word
   then dup hot here within             \ ram location?
   if origin
      begin begin cell+ chere over u<
                  if 2drop exit
                  then 2dup @ =
            until dup cell- >nfa ?dup
            if count 7f and type ."   ram location "
            then
      again
   then
   4f00 = if ." nxt jmp "  then
   ;
forth: : msee  ( a -- ) fffe and begin dup decom cell+ stop? until drop ;
forth: : see   ( <name> -- )  ' msee ;

extra: : many  ( -- )  >in @ stop? and >in ! ;

forth: shield tools\
( ) romhere ' romp >body @ !
\ - - -

;;;noforth;;;
ihx
<---- until the end

   ans:
throw#   reserved for

 -1   abort
 -2   abort"
 -3   stack overflow
 -4   stack underflow
 -5   return stack overflow
 -6   return stack underflow
 -7   do-loops nested too deeply during execution
 -8   dictionary overflow
 -9   invalid memory address
-10   division by zero
-11   result out of range
-12   argument type mismatch
-13   undefined word
-14   interpreting a compile-only word
-15   invalid forget
-16   attempt to use zero-length string as a name
-17   pictured numeric output string overflow
-18   parsed string overflow
-19   definition name too long
-20   write to a read-only location
-21   unsupported operation (e.g., at-xy on a too-dumb terminal)
-22   control structure mismatch
-23   address alignment exception
-24   invalid numeric argument
-25   return stack imbalance
-26   loop parameters unavailable
-27   invalid recursion
-28   user interrupt
-29   compiler nesting
-30   obsolescent feature
-31   >body used on non-created definition
-32   invalid name argument (e.g., to xxx)
-33   block read exception
-34   block write exception
-35   invalid block number
-36   invalid file position
-37   file i/o exception
-38   non-existent file
-39   unexpected end of file
-40   invalid base for floating point conversion
-41   loss of precision
-42   floating-point divide by zero
-43   floating-point result out of range
-44   floating-point stack overflow
-45   floating-point stack underflow
-46   floating-point invalid argument
-47   compilation word list deleted
-48   invalid postpone
-49   search-order overflow
-50   search-order underflow
-51   compilation word list changed
-52   control-flow stack overflow
-53   exception stack overflow
-54   floating-point underflow
-55   floating-point unidentified fault
-56   quit
-57   exception in sending or receiving a character
-58   [if], [else], or [then] exception
---->
