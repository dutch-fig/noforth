\ noForth meta assembler (cross assembler)
\ Copyright (C) 2015, Albert Nijhof & Willem Ouwerkerk
\
\    This program is free software: you can redistribute it and/or modify
\    it under the terms of the GNU General Public License as published by
\    the Free Software Foundation, either version 3 of the License, or
\    (at your option) any later version.
\
\    This program is distributed in the hope that it will be useful,
\    but WITHOUT ANY WARRANTY; without even the implied warranty of
\    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\    GNU General Public License for more details.
\
\    You should have received a copy of the GNU General Public License
\    along with this program.  If not, see <http://www.gnu.org/licenses/>.

<----
This 430 assembler is part of the meta compiler.
It produces code at romHERE
---->

 hex   \ until the end
\ ----- multiple definitions
<----
: ?input    abort"  End of input " ;
: bl-word ( -- countedstring )   \ met refill
   begin    bl word  dup c@ 0=
   while    drop refill   0= ?input
   repeat ;
---->

' constant value definer
: multi ( <def-word> -- )  ' to definer
   begin    bl-word count  over c@ [char] \ <>
   while    evaluate    definer execute
   repeat   2drop ;

\ ----- assembler output
<----
\  use t! and x, from metacompiler
: t!      hosta 2dup cr swap FFFF and 2B .r  ." >"    .  \ show
          over 8 rshift over 1+ c! c! ;            \ do
: x,      dup cr romhere 20 .r    ." : "   FFFF and . \ show
          dup c, 8 rshift c, ;                     \ do
---->

meta: also
mass: also
\ ----- adressing
mass:
: ?addr  abort"  Addressing error " ;
: ?reg   0F u> abort"  Register expected" ;

meta:
multi constant
  00 pc   01 rp    02 sr   03 cg
  04 sp   05 ip    06 w    07 tos
  08 day  09 moon  0A sun  0B xx
  0C yy   0D zz    0E dox  0F nxt  -40 .b
  -1 x)   -2 )     -3 )+   -4 #    -5 -)   \\

: #4  sr ) ;     : #2  cg ) ;
: #8  sr )+ ;    : #-1 cg )+ ;
: #0  cg ;       : #1  0 cg x) ; \ no extension!
: &   sr x) ;

mass:
multi value  4 a.pos    8 r.pos
  0 d.ext    0 d.ext?   0 s.ext   0 s.ext?    \\
\ a.pos = location of addresmode in opcode. (bit#0)
\ r.pos = location of register# in opcode.  (bit#0)

: dst ( opcode -- opcode )
   >r
   s>d 0=      if 0 then                                \ reg.direct
   dup ) =     if drop 0 swap x) then                   \  reg )  ->  0 reg x)
   dup x) =    if rot to d.ext  true to d.ext? then     \ index>extension
   -2 over <   if negate a.pos lshift  r> or swap
                  dup ?reg r.pos lshift or  exit then   \ reg  and   reg x)
   true ?addr ;

: src ( opcode -- opcode )
   >r
   s>d 0=      if 0 then                                   \ reg.direct
   dup x) =    if rot to s.ext  over cg <>                 \ ext ?
                  if true to s.ext? then then              \ not for #1
   dup # =     if drop to s.ext  true to s.ext? pc )+ then \ xxxx #
   -4 over <   if negate a.pos lshift  r> or swap
                  dup ?reg r.pos lshift or   exit then     \ all addrmodes
   true ?addr ;

: ass.code, ( opcode -- )  x,
   s.ext?  if s.ext        x, then
   d.ext?  if d.ext        x, then ;
0 value 'sub
: no.ext    0 to s.ext?  0 to d.ext?  ;
: .b?   over .b = if swap negate or then ;
: -)?   over -) =
        if nip >r r@ 40 and swap >r     \ byte or cell?
           if #1 else #2 then r@
           'sub execute                 \ insert decrement instruction
           0 r> x) r>                   \ followed by main instruction
        then ;
\ ----- mnemocodes
: a&r to r.pos to a.pos ;
-41 value retro  \ expect reversed order of src and dst on stack,
                 \ for emul.instr.
: 1op    create ,
   does>    @ .b? -)? no.ext   4 0 a&r src   ass.code, ;
: 2op    create ,
   does>    @ .b? -)? no.ext   over retro =
   if nip   4 8 a&r src   7 0 a&r dst
   else     7 0 a&r dst   4 8 a&r src
   then ass.code,  ;

meta:
multi 1op      1000 rrc    1080 swpb   1100 rra
   1180 sxt    1200 push   1280 call  \\  ( 1300 reti  )

: reti 1300 , ;

multi 2op      4000 mov    5000 add    6000 addc
   7000 subc   8000 sub    9000 cmp    A000 dadd
   B000 bit    C000 bic    D000 bis    E000 xor>   F000 and>   \\
' sub to 'sub

\ Macros

forth definitions
0007 value codelength

meta:
: NEXT  nxt pc mov ;
<----
: ret   rp )+ pc mov ;
: br    # pc mov ; \ branch, addr in extension
: setz  #2 sr bis ;   : clrz  #2 sr bic ;
: setn  #4 sr bis ;   : clrn  #4 sr bic ;
---->
: setc  #1 sr bis ;   : clrc  #1 sr bic ;
: eint  #8 sr bis ;   : dint  #8 sr bic ; \ Toegevoegd ***

<----
: pop  rp )+ retro mov ;    : clr    #0 retro mov ;
: dec     #1 retro sub ;    : inc    #1 retro add ;
: --      #2 retro sub ;    : ++     #2 retro add ;
: adc    #0 retro addc ;    : sbc    #0 retro subc ;

: inv   #-1 retro xor> ;
---->

<----
mass:
: src=dst ( dst -- dst dst )
  dup .b =
  >r r@ if drop then
  dup x) = if 2over nip 2over nip 2over nip
  else s>d if 2dup
         else dup then then
  r> if .b then ;
meta:
: rla ( dst -- ) src=dst add ;                 \ dst dst add
: rlc ( dst -- ) src=dst addc ;                \ dst dst addc
---->


\ ----- assembler conditionals
<----
safety numbers
11 sys\if     for then ahead while repeat
22 sys\begin  for until again repeat
33 sys\do     for ?do loop +loop
44 sys\:  for : ; does> ;code :doer
55 sys\code   for end-code ;code does> codedoer
  Assembler
66 sys\if,     for then, ahead, repeat,
77 sys\begin,  for until, again, repeat,
---->

\ vbb:   <? if,     0<>? until,
meta:
multi constant
   2000 =?     2400 <>?      2800 cs?    2C00 cc?
   3000 pos?   3400 >?       3800 <eq?   3C00 never
   2800 u<eq?  2C00 u>?
   0066 sys\if,  0077 sys\begin, 3FF <offset>  \\

\ <offset>     = masker for offset -> then and until
\ never        = cond for always.jump -> ahead, again
\              = masker for condition -> see ?cond

: ?pair ( x y -- )   - abort"  Conditionals not paired. " ;
: ?cond ( cond -- )  never invert and        \ niet waterdicht
   abort"  Condition needed. " ;

: if, ( cond -- ifloc ifcond )   dup ?cond sys\if,  or romhere swap  -1 x, ;
: begin, ( -- beginloc sys\begin, )  romhere sys\begin, ;

: then, ( ifloc ifcond -- )
   dup   never invert and  sys\if, ?pair
   never and   dup ?cond   >r
   romhere over 2 + - 2/ <offset> and  r> or    swap t! ;
: until, ( beginloc sys\begin, cond -- )
   dup ?cond   >r
   ( cs> cs> )    sys\begin, ?pair
   romhere 2 + - 2/ <offset> and    r> or    x, ;

: again,    never until, ;
: else,     never if, 2swap then, ;
: while,    if, 2swap ;
: repeat,   never until, then, ;
: ahead,    never if, ;
: jmp       sys\begin, again, ;  \ jump, relative addr in opcode
\ : |||       day sun mov  ;      \ no-op
: ?advance, ( label cond -- ) sys\begin, swap until, ;
: go-by,    ( cond label -- ) sys\begin, rot  until, ;
: LABEL-UNTIL,    ( cond label -- ) sys\begin, rot  until, ;

<----
\ ------ voorbeeldjes
: `   0 parse   cr 2dup type   evaluate ;


---->
cr .(    assembler loaded    )


\ <><>
