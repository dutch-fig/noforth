( For noForth C&V2553 lp.0, Port output with MSP430G2553 at port-2. )

( Port-2 must be wired to 8 leds, placed on the launchpad experimenters kit.
  Wire P2.0 to P2.7 to the anode of eight 3mm leds placed on the breadboard 
  the pinlayout can be found in the hardwaredoc of the launchpad. Connect
  all kathodes to each other, and connect them to ground using a 100 Ohm 
  resistor. )
  
( The most hard to find data are those for the selection registers. 
  To find the data for the selection register of Port-2 here 02E one got to 
  go to the "Port Schematics". This starts on page 42, for P2 the tables are 
  found from page 50 and further. These tables say wich function will be on 
  each I/O-bit at a specific setting of the registers. )

HEX
( Address 029 p2out, port-2 output with 8 leds
  Address 02A p2dir, port-2 direction register
  Address 02E p2sel, port-2 selection register )

\ Store pattern b at leds
: >LEDS         ( b -- )    029 c! ;

: FLASH         ( -- )  \ Visualise startup
    FF >leds  064 ms    \ All leds on
    00 >leds  064 ms ;  \ All leds off

: SETUP-PORTS   ( -- )
    00 02E c!           \ Port-2 all bits I/O
    -1 02A c! ;         \ All bits of P2 are outputs

: COUNTER       ( -- )  \ Binary counter
    setup-ports  flash
    0                   \ Counter on stack
    begin
        1+              \ Increase counter
        dup >leds
        020 ms          \ Wait
    key? until
    ;

: RUNNER        ( -- )  \ A running light
    setup-ports  flash
    begin
        8 0 do          \ Loop eight times
            1 i lshift  >leds   \ Make bitpattern
            064 ms      \ Wait
        loop
    key? until          \ Until a key is pressed
    ;

freeze
