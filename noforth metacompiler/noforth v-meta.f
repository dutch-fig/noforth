\ noForth V metacompiler
\ Copyright (C) 2015, Albert Nijhof & Willem Ouwerkerk
\ update april 2015

\    This program is free software: you can redistribute it and/or modify
\    it under the terms of the GNU General Public License as published by
\    the Free Software Foundation, either version 3 of the License, or
\    (at your option) any later version.
\
\    This program is distributed in the hope that it will be useful,
\    but WITHOUT ANY WARRANTY; without even the implied warranty of
\    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\    GNU General Public License for more details.
\
\    You should have received a copy of the GNU General Public License
\    along with this program.  If not, see <http://www.gnu.org/licenses/>.

\ META I
only forth also definitions
marker -META
hex

\ V-version
char V constant CV?
: ?Vversion [char] V <> abort"  V version? " ;

\ targ addresses
 0 value HOT            \ dictionary
 0 value RAM-END
 0   value ROM-END
 0 value ORIGIN         \ noForth ROM starts here.
 0 value FROZEN
 0 value IVECS
 8 constant DICTIONARY-THREADS    \ 2, 4, 8 or hx 10 threads
\ V-version
\ Attention, imediately after the threads:
: PFX-LINK    hot dictionary-threads 2* + ;     \ see pfx-id, pfx-action
: WID-LINK    hot dictionary-threads 2* 2 + + ; \ see voc#
\ - - -

0 constant TO#
2 constant +TO#
4 constant INCR#

: INFO-NXT    frozen 38 + ;     \ 16, info block with noforth system data

   0 value IMAGE        \ 32
   0 value xCREATED     \ lfa16
   0 value CURRENTVOC   \ wid08
   0 value ready?       \ flag
TRUE value TRACER       \ flag
 200 value RAMHERE      \ noForth ram pointer16, moving while metacompiling
   0 value WORDCOUNT    \ n
variable xSTATE   0 xstate !    \ flag

\ Error messages
: ?NOTYET     abort"  Not defined " ;
: ?UNKNOWN    abort"  What's this? " ;
: ?INPUT      abort"  No input " ;
: ?USERSTOP   abort"  Userstop " ;
: ?PFX        abort"  Prefix not accepted " ;
: ?PAIR ( x y - )  <> abort"  Unpaired " ;
: ?STACK     DEPTH 0< abort"  Empty stack " ;
: ?NAMELENGTH abort"  Name too long " ;

\ <---- Intro ---->

: UPPER        ( a32 n -- )   over + swap
   ?do i c@ 61 7B within if   i c@ 20 - i c! then
   loop ;
: LOWER        ( a32 n -- )   over + swap
   ?do i c@ 41 5B within if   i c@ 20 + i c! then
   loop ;

\ Allot text as a counted string. See IGNORE xHEADER x" x."
: STRING,      ( a32 n -- )
   here swap c,   count   dup allot   move   align ;

: BL-WORD      ( -- countedstring32 )   \ Read next word, REFILL if necessary.
   begin bl word dup c@ if   exit then
        drop refill 0= ?input
   again ;

: doIGNORE      \ Ignore text until end-string is encountered.
   does> count begin bl-word count 2dup upper 2over compare 0= until 2drop ;

: IGNORE       ( <start-string> <end-string> -- )
   create immediate doignore bl word count 2dup upper string, ;

IGNORE <---- ---->

<---- skips text until ---->

<----   doIGNORE
is an example af a DOES-part that is defined before
the definition of the CREATE-part. This method makes it
possible to avoid forward references while meta-compiling.
For example:
When docol exists you can compile hi-level definitions
in the Target before : is defined in the target.
---->

<----  IMAGE
IMAGE is the address(32b) of 64k free space on the host.
In that space the image of noforth will be built.
IMAGE is calculated automatically when meta-compiling starts,
see :::NOFORTH:::

IMAGE corresponds with address 0000 in the target

address types
HOSTA converts a 16b Target address into a 32b Host address.
TARGA converts a 32b Host address into a 16b Target address.
---->

: (16)          ( x -- y )      FFFF and ;
: ?TARGA        ( a16 -- )      FFFF over u<
   if u.  true abort"  address not in targ " then drop ;
: ?HOSTA        ( a32 -- )      FFFF over u<
   if image + u.  true abort"  address not in image " then drop ;
: HOSTA        ( a16 -- a32 )   dup ?targa image + ;
: TARGA        ( a32 -- a16 )   image - dup ?hosta ;

\ Destructive MARKER
: ANEW ( <name> -- )   \ see :::noforth:::
   >IN @ >R
   BL WORD FIND IF DUP EXECUTE THEN DROP
   R> >IN ! MARKER ;

: WIN   ( <word> -- ? ) bl-word count evaluate ; \ winterpret

<----   Interim BASE and CH
while handling the next word in the input stream
---->
: doFFBASE             \ see FFBASE below
   DOES>  base @ >r
      @ base ! ['] win catch
      r> base !  throw ;
: FFBASE       ( tempbase -- )
   create , immediate doffbase ;
hex     10 ffbase HX    0A ffbase DM    2 ffbase BN
: CH    char state @ if postpone literal then ; immediate

<---- select processor board ---->
\ string operations, no overflow testing!
\ a2 = counted string address
: PLACE ( a n a2 -- )   \ store a,n as counted string at a2
  2dup c! 1+ swap move ;
: +TEXT ( a n a2 -- )   \ append text
  >r tuck r@ count + swap move r@ c@ + r> c! ;
: +CH ( ch a2 -- )      \ append a character
  >r r@ count + c! r@ c@ 1+ r> c! ;

: PRESENT? ( ch a n -- ? )       \ ch in string a,n?
  2>r false 2r>
  bounds ?do over i c@ = + loop
  nip ;

create MYNAME 0 c, 30 allot align
create MYFILE 0 c, 30 allot align
char 2 value BOARD?

: !MYNAME ( -- )
  s" noForth V" myname place
  BOARD?
  ch A over = if s" 2553 Lp.0" else
  ch B over = if s" 149 Mc.1" else
  ch D over = if s" 149 Du.0" else
  ch E over = if s" 149 Mv.0" else
  ch F over = if s" 5739 Ex.0" else
  ch G over = if s" 5969 Ex.0" else s" ???"
  then then then then then then
  myname +text
  bl myname +ch
  drop
  time&date [ decimal ] 100 mod
  3 0 do
  10 /mod ch 0 + myname +ch     [ hex ]
          ch 0 + myname +ch
  loop 2drop drop
  myname count myfile place
  s" .a43" myfile +text
  myfile count lower    \ <-----------
;

: SELECT-PROCESSOR ( -- )
cr cr ." *                                                              *"
cr cr ."   A   noForth V2553 lp.0  for MSP430G2553 Launchpad"
cr cr ."   B   noforth V149 mc.1   for MSP430F149 Minim Core board"
cr cr ."   D   noForth V149 du.0   for MSP430F149 Dupont board"
cr cr ."   E   noForth V149 mv.0   for MSP430F149 Mini-V3 board"
cr cr ."   F   noForth V5739 ex.0  for MSP-EXP430FR5739 Experimenter Board"
cr cr ."   G   noForth V5969 ex.0  for MSP-BNDL-FR5969LCD"
cr cr ." *                                                              *"
cr cr ."           choose (A B D E F G):  "
   key 1F and 40 +
   dup emit
   dup s" ABDEFG" present?
   0= abort"  Bad choice "
   cr to BOARD?
   !myname ;

<---- tracing ---->
: WAIT/GO       ( -- )  \ Voor .HERE TARGDUMP
   key? 0= if exit then
   2 0 do key 1b = loop
   or ?userstop ;       \ Stop bij [ESC]
: .STACK       ( -- )  \ Voor .HERE
   ?STACK
  ."       ( "
   depth if 0 depth 2 - do i pick . -1 +loop then
   ." ) " ;
: DOTCELL      ( x -- )   0 <# # # # # #> type space ;
: DOTBYTE      ( x -- )   0 <# # # #> type space ;
: RTYPE        ( adr len r -- )    over max over - >r type r> spaces ;

\ Host cel = 32 bits, Target cel = 16 bits.
\ Target needs alignment.

: TRACE true to tracer ;
: NOTRACE false to tracer ;

: .HERE: xstate @ if exit then cr here (16) 4 .r 0b spaces ;
: ROMHERE ( -- targ ) here targa ;
: .CELL        ( x -- )       tracer if dotcell exit then drop ;
: .CELL,       ( x -- )       tracer if  dotcell ." , " exit then drop ;
: .CELLSTORE   ( x host -- )  tracer if swap dotcell
                                        dotcell ." ! " exit then 2drop ;
: .BYTE        ( ch -- )      tracer if dotbyte exit then drop ;
: .BYTE,       ( ch -- )      tracer if dotbyte ." c, " exit then drop ;
: .BYTESTORE   ( x host -- ) tracer if swap dotbyte
                              dotcell ." c! " exit then 2drop ;
: .NAME        ( a32 -- )   tracer if count 8 rtype space exit then drop ;
: .STRING      ( a n -- ) tracer if .here: dup dotbyte [char] " emit
                          type [char] " emit 2 spaces exit then 2drop ;
: .M,          ( a n -- ) tracer if [char] " emit type
                                    [char] " emit ."  m,  " exit then 2drop ;
: .HERE        ( -- )     tracer if .stack cr romhere .
                                    xstate @ if 3 spaces then
                          then wait/go ;
: .RAMHERE     ( -- ) tracer if ."  RAMhere " ramhere . then ;

<---- DUMP ---->
: STOP?        ( - true/false )
   key? dup 0= ?exit            \ 0
   drop
   key  bl over =               \ key ?
   if drop key then            \ key
   01B over =
   abort" user interrupt "
   bl <> ;                      \ key bl <> ?

: DUMP ( a32 n -- ) dup 0= if 2drop exit then
   20 base @ / 3 min 2 max >r                   \ width
   begin 2dup base @ umin                       \ a n a q
        cr over 6 .r ." : "                    \ .adres
        2dup
        begin dup while swap                   \ q a
              over base @ 2/ = if space space then
              count r@ 0 do base @ /mod loop drop      \ de cijfers
              r@ 0 do 0 .r loop space          \ q a+
              swap 1-                          \ a+ q-
        repeat 2drop                           \ a n a q
        ." |"
        2dup
        begin dup while swap                   \ q a
\              over base @ 2/ = if ." |" then
              count bl max 7E min emit         \ q a+
              swap 1-
        repeat 2drop                           \ a+ q-
        ." | "                                 \ a n a q
        nip /string 0 max                      \ a+ n-
        stop? 0= and
        dup 0=
  until 2drop r> drop ;

: TDUMP ( a16 n -- ) swap hosta swap dump ; \ debugging tool

<---- x-words and t-words
are words that do not behave the same way in noforth as in the host-forth.

16bits  f e t c h   &   s t o r e  &  c o m m a
these words operate on targ (virtual 16 bit) addresses!
---->
: ?ADRES.OK  [ ' -meta 2 + ] literal > ?exit
   dup . abort"  Adresfout " ;
: ! ( n a -- ) dup ?adres.ok ! ;
: C! ( n a -- ) dup ?adres.ok c! ;

: xALIGNED dup 1 and + ;
: t@   ( a16 -- n )     hosta count swap c@ 8 lshift or ;
: tC@  ( a16 -- n )     hosta c@ ;
: t!   ( n a16 -- )     hosta over 8 rshift over 1+ c! c! ;
: x,   ( n -- )         dup .cell, romhere t! 2 allot ;   \ n = 16-bits
: t!   ( n a16 -- )     2dup .cellstore t! ;            \ now with tracer
: xC, ( ch -- )         dup .byte, c, ;
: tC! ( n targ -- )     hosta 2dup .bytestore c! ;
: xALIGN here 1 and if 0FF xc, then ;

: xM,   ( a32 n -- )     2dup .m, here swap dup allot move ;
: xSTRING,     ( a32 n -- )      \ Allot as counted string, needs ALIGN
               dup xc, xm, ;
: xCELLS       ( n -- 2n ) 2* ;
: xCELL+       ( a -- a+2 ) 2 + ;
2 constant xCELL
: t>BODY       ( xt -- body )   xcell+ ;

\ V-version
: LFA>N         ( lfa -- nfa )  3 + ;
: LFA>          ( lfa -- cfa )  lfa>n count 7F and + xaligned ;
: tLFA>         ( lfa16 -- cfa16 )      hosta lfa> targa ;
\ tLNK@ is for cell-links
: tLNK@         ( lfa16 -- lfa2-targ )  t@ ;
\ - - -

forth definitions
vocabulary META
vocabulary MASS        \ aux words for meta-assembler
: MASS: mass definitions ;
: META: meta definitions ;
only forth also definitions
mass also
meta also

include noforth-meta-asm.f
hex
<---- END OF META1 ---->
\ safety numbers
11 constant SYS\IF      \ for then ahead while
22 constant SYS\BEGIN   \ for until again
33 constant SYS\DO      \ for ?do loop +loop
44 constant SYS\:       \ for : ; does> ;code :doer
55 constant SYS\CODE    \ for end-code ;code does> codedoer
\  See Assembler
\ 66 constant SYS\IF,     \ for then, ahead, repeat,
\ 77 constant SYS\BEGIN,  \ for until, again, repeat,

ignore <<NOTFORME >>ALL
meta:
: >>ALL ; immediate
: <<noforth    ( <name> -- )
   BOARD?
   bl word count ( 16aug2014 ) 2dup upper
   present? ?exit
   postpone <<NOTFORME ;

\ -----------------------------  meta II -------------------------

<---- META II - an - 19mei04 ---->

forth definitions
hex
: xTHREADS hot dictionary-threads 2* + hot ;
: @IMM  ( lfa -- 1/-1 )
        lfa>n hosta c@ 80 and if true else 1 then ; \ msb: 0=imm
create WORDA    22 allot align     \ Voor de opslag vh resultaat van BL WORD
: >WORDA ( a n -- )
   1F over < ?namelength
   dup worda c! worda 1+ swap move ;
: THREAD#       ( bl-word -- nr )
   count swap c@ xor dictionary-threads 1- and ;
: THREAD        ( bl-word -- targ )             \ nnn
   thread# xcells hot +  ;                      \ dictionary
: S<>   ( a1 a2  -- <>? )
   over c@ 1+ 0 do over c@ 7F and over c@ 7F and
   = while 1 dup d+
   loop   2drop false exit
   then unloop 2drop true ;

\ V-version
: FIND-NAME ( worda32 lfa16 -- worda 0 | xt16 imm )
   begin dup 0= ?exit           ( w32 lfa16 )    \ end of list?
      2dup lfa>n hosta          ( w32 lfa16 w32 nfa32 )
      s<>       \ unequal?
   while tlnk@                  ( w32 lfa16 )
   repeat                       ( w32 lfa-yes16 )
   nip dup tlfa>                ( lfa16 xt )
   swap @imm ;                  ( cfa imm )

: tFIND   ( worda32  -- worda32 0 | xt16 imm )
   dup count upper
   dup thread                   \ contains topword lfa
   t@                           \ worda32 toplfa16
   find-name ;
: LNK,  ( link-holder16 -- )
   romhere
   over t@ x,
   swap t! ;

: xHEADER       \ builds header WITHOUT codefield
   depth if cr .stack then
   xalign
   tracer
   if ( cr cr )
   else wordcount 0a mod 0= if cr then
   then
   1 wordcount + to wordcount
   .here bl-word count >worda
   worda  tfind nip
   if   cr ." --- redefining:"
   then
   tracer if ." <<<<< " then
space worda count type space
   tracer if ."  >>>>> " .here: then      \ print name
   romhere to xcreated
   worda thread  lnk,
   .here tracer if 0a spaces then
   currentvoc 80 or xc,                         \ hvoc
   worda count dup 80 or xc, xm,                 \ icount & name
   xalign ;
\ - - -

\ ----- i n t e r p r e t -----
: xCOMPILE,     ( a16 - )        x, ;

: NAME:         ( <name> -- )   bl-word count >worda
                                worda count postpone sliteral ;
: FIND-IT       ( a n -- xt16 ) >worda worda .name worda tfind ?exit
                                abort"  not found " ;
: COMPILE-IT    ( a n -- )      find-it xcompile, ;
: DOES-IT       ( a n -- )      find-it 2 + x, ;

: TOKEN:        ( a n -- xt )   name: postpone find-it ; immediate
: COMPILE:      ( a n -- )      name: postpone compile-it ; immediate
: DOES:         ( <name> -- )   name: postpone does-it ; immediate

: xLITERAL     ( x -- ? )
  (16) xstate @
  if dup 3800 C800 within
     if   compile: SN(
     else 2* 1+
     then x,
  then ;

<----  Late binding (avoiding forward refences)
NAME: ccc compiles ccc as a string in the meta-compiler.
At target compile time
   TOKEN: ccc
   COMPILE: ccc
   DOES: ccc
             search for ccc.
---->

meta definitions
get-current          cr .stack .( get-current )
forth    definitions
constant META-WID    cr .stack

: IN-META?      ( worda -- xt imm? -- 0 )       count meta-wid search-wordlist ;

\ --------------------------------------

\ Converting a string into a  SINGLE number, the minus sign is accepted.
: -SIGN?        ( a n -- a n false | a+1 n-1 true )
   dup if  over c@ [char] - =   if 1 /string true exit then  false then ;
: NUM?  ( blword -- xlo true | ? false )
   count -sign? >r
   dup if   false dup 2swap >number 0= nip nip then     \ xlo t/f
   r> if swap negate swap then ;
: >NUM  ( blword -- x )         \ Counted string --> -single number
   num? 0= ?unknown ;

: 2N    (  )    compile: DN( bl word >num bl word >num , , ;

: WINTERPRET    ( bl-word -- )
   count >worda                         \ result in worda
   worda .name
   xstate @
   if worda tfind
      0< if xcompile, exit then         \ Compile when not immediate
      drop                              \ Immediate or NOT found
   then
   worda in-meta?  if execute exit then \ Only execute meta words
   worda >num xliteral ;                \ Number? -> compile if necessary

: METACOMPILING         ( -- )
   begin .here
         bl-word winterpret
         ready?
   until  ready? 1- to ready?
   ready? if  ."  R e a d y "  then ;

<---- metacompiler start / stop ---->
: RAMALLOT      ( n -- )        .ramhere ramhere + to ramhere ;

: :::NOFORTH:::         ( -- )
   forth
   0 to ready?
   s" anew -noforth" evaluate            \ Destructieve marker
   here FFFF0000 and 10000 + to image
   here image over - dup allot   0 fill \ Start op eerstvolgende HEX xxxx0000
   here    origin    dup allot   0 fill
   cr myname count type
   cr ." targ:  HERE    " here .
   cr ." host:  ROMHERE " romhere .
      ."  ORIGIN " origin .
   cr ." DICTIONARY-THREADS (hex) " dictionary-threads .
   cr ." type . if you want to continue "
      [char] . key <> ?userstop         \ continue?
   cr
   origin hosta origin negate (16) FF fill
   0 xstate !                           \ Initiation, anything forgotten?
   hot hosta 40 0 fill                  \ make toplinks zero
   frozen hosta 40 0FF fill
   hot to ramhere
   METACOMPILING ;

: MYNAME, myname count dup xc, xm,  xalign ;

\ METACOMPILING will stop when READY? has become True (in ;;;NOFORTH;;;)

: ;;;NOFORTH;;;
   1300 ivecs FFFE over 2 + do dup i t! 2 +loop
   dup u. t!
   true to ready?               ( quit )
\   freeze
   hot hosta   frozen hosta
   s" t' STATE" evaluate
   2 + t@ hot - move            \ warm -> cold
   cr ." H O T       " hot .
   cr ." H E R E     "  ramhere .
   cr ." F R O Z E N " frozen .
   cr ." O R I G I N " origin .
   cr ." C H E R E   " romhere dup hosta to rom-end .
;

<----
 :::NOFORTH:::   start meta compiling
 ;;;NOFORTH;;;   finish meta compiling
 ( first and last commando of the Target compiler code )
---->

: xHX   postpone hx xliteral ; immediate
: xDM   postpone dm xliteral ; immediate
: xBN   postpone bn xliteral ; immediate

CR  .(   END of META II )

\ -----------------------------  meta III -------------------------
\ Meta III - an - 19mei04

<---- d e f i n i n i n g  w o r d s ---->

\ 4 labels
meta:
0 value AMSTERDAM
0 value ROTTERDAM
0 value THINGUMAJIG
0 value DOESINTRO
0 value NOCATCHFRAME

: LABEL-AMSTERDAM       romhere to amsterdam ;
: LABEL-ROTTERDAM       romhere to rotterdam ;
: LABEL-THINGUMAJIG     romhere to thingumajig ;
: LABEL-DOESINTRO       romhere to doesintro ;
: LABEL-NOCATCHFRAME    romhere to nocatchframe ;
forth definitions

: xIMMEDIATE    xcreated lfa>n dup hosta c@ 7F and swap  tc! ;
: x[    0 xstate !  ;
: x]    -1 xstate !  ;
: x:    ( <name> -- )   xheader does: DOCOL    x]   sys\: ;

: x;    ( -- )          sys\: ?pair  compile: EXIT  x[ ;
: x:DOER        ( <name> -- sys\: )
   xheader does: DODOER
   4030 ( # mov pc) x, doesintro  x,
   x] sys\: ;

\ IGNORE NO-CODING END-CODE  \ voor xCODE en CODEdoer
\ : xCODE xheader  postpone no-coding ;  \ Als er geen assembler is
\ : xCODEdoer ( <name> -- ) xheader does: DODOER postpone no-coding ;

\  Normal version
: xEND-CODE     sys\code ?pair ;
: xCODE         xheader romhere 2 + x, sys\code ;
: xCODEdoer     xheader does: DODOER sys\code ;
: xVARIABLE     ( <name> -- )
  xheader does: DO@ ramhere x,   2 ramallot ;
: xVALUE        ( <name> -- )
  xheader does: DO@@ ramhere x,   2 ramallot ;
\ : RAMCONSTANT   ( n <name> -- )
\  xheader does: DO@@ ramhere x, ramhere t!   2 ramallot ;
: xCONSTANT     ( n <name> -- )
  xheader does: DO@ x, ;
: xFFBASE       ( tempbase <name> -- )
  xheader does: DOFFBASE x, ximmediate ;
\ : INFOCONSTANT  ( n <name> -- )
\  xheader does: DO@@  dup x, t! ;
: PREFIX        ( pfx# <name> -- )
  xheader does: DOPREFIX x, ximmediate ;
: t'   ( <ccc> -- cfa-targ )
   bl-word   dup .name   tfind 0= ?notyet ;

: xRECURSE     ( -- )   xcreated tlfa> xcompile, ;

: ONLY:         0 to currentvoc ;
: FORTH:        1 to currentvoc ;
: INSIDE:       2 to currentvoc ;
: EXTRA:        3 to currentvoc ;
: ASSEMBLER:    4 to currentvoc ;

<---- [430]
 De getallen 0,4,8...7C zijn 'wid's
---->

\ ---- a n d e r e  r o d e  w o o r d e n ----

: xPOSTPONE    ( <ccc> -- )
   bl-word dup .name
   tfind dup 0= ?notyet
   0< if compile: COMPILE(
   then xcompile, ;

: xS"  ( <ccc"> -- ) compile: S"( ch " parse xstring, xalign ;
: x."  ( <ccc"> -- ) compile: ."( ch " parse xstring, xalign ;
: LASTNAME ( -- a ) xcreated lfa>n ;
: ?ABORT        compile: ?ABORT( LASTNAME x, ;
: xchar        ( -- ch )
   bl-word dup .name
   count 0= ?input
   c@ ;

: x[CHAR]       ( -- ch ) ( -- )
   xchar xliteral ;
: x['] ( <name> -- ) t'  xliteral  ;

\ V-version
: PFX-FOR-VALUE         ( pfx# <name> -- sys\code )     \ define local pfx
   pfx-link lnk,
   token: do@@ t>body  + x,     \ pfx-id
   t' xcompile, ;

\ compile prefixed values, for ex. TO LISA
: PFXOBJECT,   ( pfx# <ccc> -- data-xt doval ) \ test op DOVAL
   t'   tuck t@                 \ 'LISA to# doval?
   token: do@@ t>body           \ 'LISA to# doval? doval
   over <> ?pfx                 \ LISA is not a value
   +  pfx-link                  \ 'LISA pfx-id pfx-link
   t@                           \ 'LISA pfx-id lfa
   ahead
   begin tlnk@                  \ 'LISA pfx-id lfa?
   [ 1 cs-roll ] then
       dup 0= ?pfx              \ 'LISA pfx-id lfa
       2dup 2 + t@ =            \ 'LISA pfx-id lfa \ pfx-id correct?
   until nip 4 +  t@ xcompile,  \ compile to-action
   2 +  t@ x,                   \ 'LISA >body @ is ram-address
     ;

: xTO   to#  pfxobject, ;       ( 0 <ccc> -- )
: x+TO  +to#  pfxobject, ;      ( 2 <ccc> -- )
: xINCR incr# pfxobject, ;      ( 4 <ccc> -- )

\ V-version
: xVOCABULARY
   xheader
   dup 0=
   if does: DOONLY
      0 wid-link t!
   else does: DOVOC
   then
   wid-link t@                          \ top-link
   romhere wid-link t!
   x,                                   \ link
   dup xc,                              \ wid
   romhere 1+ xcreated 3 + - xc,        \ offset to nfa
   0= if 3 xc, 0 xc, 0 xc, 0 xc, then ;
: SHIELD xheader does: DOMARKER
   romhere token: FRESH 4 +
   dup t@ FFFF = if t! else 2drop then          \ patch in FRESH
   5 xc, 1 xc, 1 xc, 3 xc, 0 xc, 1 xc,  \ forth forth extra only : forth
   romhere 4 + x, ramhere x, ;

\ - - -
<----  immediate compiler words ---->

: ?DIST         ( x -- )                -1000 u< abort"  too far away " ;
: xIF           ( -- IFa sys\if )       romhere 8FFF x,  sys\if ;
: xAHEAD        ( -- aheada sys\if )    romhere 7FFF x,  sys\if ;
: xTHEN         ( IFa sys\if -- )
  sys\if ?pair
  romhere over -                \ distance, positive
\   2 -  800 -  1 or             \ [F800,0)
   801 -  dup ?dist
   over t@ and  swap t! ;
: x/THEN       ( IFa sys\if x y -- )  2swap xthen ;

: xBEGIN       ( -- BEGINa sys\begin ) romhere sys\begin ;
: xUNTIL       ( a 22 -- )
   8FFF  >r
   sys\begin ?pair
   romhere -            \ distance, negative
\  2 - 800 - 1 or
   801 -  dup ?dist     \ [F000,F800)
   r>  and x,  ;
: xAGAIN       ( a id -- )
   7FFF
\ and continue as in UNTIL
   >r
   sys\begin ?pair
   romhere -            \ distance, negative
\   2 - 800 - 1 or
   801 -  dup ?dist     \ [F000,F800)
   r>  and x,  ;

: xELSE         ( IFa sys\if -- AHEADa sys\if )         xahead x/then ;
: xWHILE        ( x n -- IFa sys\if x n )               xif 2swap ;
: xREPEAT       ( WHILEa sys\if BEGINa sys\begin -- )   xagain xthen ;
: xGOTO         ( BEGINa -- )           sys\begin xagain ;      \ for labels

\ Formal end of colon definition, without compiling EXIT
: (;)  ( sys\: -- )  sys\: ?pair  ( reveal ) x[ ;

\ DO..LOOP

: xDO  ( -- adr sys\do )  compile: DO(   romhere 0 x, sys\do ;
: x?DO ( -- adr sys\do )  compile: ?DO(  romhere 0 x, sys\do ;

: LOOPCODA     ( DOadr sys\do -- )
   sys\do ?pair   ROMHERE SWAP t! ;

: xLOOP        ( DOadr sys\do -- )  compile: LOOP)  loopcoda ;
: x+LOOP       ( DOadr sys\do -- )  compile: +LOOP) loopcoda ;

\ ============= herdefinities in META ==================

: DOMETA: does> @ execute ;

: META-WORDS:  \ List, on every line: new (META) name & FORTH name
   meta-wid set-current
   begin  postpone \
      >in @ source nip u< 0= if refill 0= ?input then \ hhh
      create dometa: ' dup ,
      ['] ;;;noforth;;; =
   until  definitions ;

\ Wil je iets veranderen? LET OP:
\ Begin op iedere regel met een META-naam en een FORTH-woord,
\ de rest van de regel wordt niet gelezen. Geen lege regels!.
\ De laatste META-naam moet ;;;NOFORTH;;; zijn.

<----
 You want to change something in this list?
 ATTENTION:
 Start every line with the META name followed by the already defined FORTH word.
 The rest of the line will be ignored.
 No empty lines!
 The last META name in the list must be ;;;NOFORTH;;;

WHEN META-COMPILING:
 Only meta-words can be found&executed.
 Only words that already extist in the target can be found&compiled.

---->
\ meta-name     forth-name   (Redefine all these Forth words in META)
META-WORDS:
  <----         <----
  \             \
  (             (
  -             -
  ,             x,
  ;             x;
  :             x:
  :DOER         x:DOER
  !             t!
  ?ABORT        ?ABORT
  ?DO           x?DO
  .             .
  ."            x."
  .STACK        .S
  '             t'
  (;)           (;)
  [             x[
  [']           x[']
  [CHAR]        x[CHAR]
  [IF]          [IF]
  [THEN]        [THEN]
  ]             x]
  @            t@
  /THEN         x/THEN
  +             +
  +LOOP         x+LOOP
  +TO           x+TO
  +TO#          +TO#
  >BODY         t>BODY
  0=            0=
  1-            1-
  2*            2*
  2N            2N
  2SWAP         2SWAP
  AGAIN         xAGAIN
  AHEAD         xAHEAD
  AND           AND
  ASSEMBLER:    ASSEMBLER:
  BEGIN         xBEGIN
  BN            xBN
  C,            xC,
  C!            tC!
  CELLS         xCELLS
  CHAR          xCHAR
  CODE          xCODE
  CODEdoer      xCODEdoer
  CODEPFX       CODEPFX
  CONSTANT      xCONSTANT
  CREATED       xCREATED
  DICTIONARY-THREADS DICTIONARY-THREADS
  DM            xDM
  DO            xDO
  DROP          DROP
  DUP           DUP
  ELSE          xELSE
  END-CODE      xEND-CODE
  EXTRA:        EXTRA:
  FFBASE        xFFBASE
  FORTH:        FORTH:
  FROZEN        FROZEN
  GOTO          xGOTO
  HEADER        xHEADER
  HOSTA         HOSTA
  HOT           HOT
  HX            xHX
  IF            xIF
  IMAGE         IMAGE
  IMMEDIATE     xIMMEDIATE
  INCR          xINCR
  INCR#         INCR#
  INFO-NXT      INFO-NXT
  INFOCONSTANT  INFOCONSTANT
  INSIDE:       INSIDE:
  INVERT        INVERT
  IVECS         IVECS
  LASTNAME      LASTNAME
  LITERAL       xLITERAL
  LOOP          xLOOP
  VOCABULARY  xVOCABULARY       \ V-version
  MYNAME,       MYNAME,
  NOTRACE       NOTRACE
  ONLY:         ONLY:
  OR            OR
  ORIGIN        ORIGIN
  OVER          OVER
  PFX-FOR-VALUE PFX-FOR-VALUE
  PFX-LINK      PFX-LINK
  POSTPONE      xPOSTPONE
  PREFIX        PREFIX
  RAM-END       RAM-END
  RAMALLOT      RAMALLOT
  RAMCONSTANT   RAMCONSTANT
  RAMHERE       RAMHERE
  RECURSE       xRECURSE
  REPEAT        xREPEAT
  ROMALLOT      ALLOT
  ROMHERE       ROMHERE
  ROMP          HERE
  RSHIFT        RSHIFT
  S"            xS"
  SHIELD        SHIELD
  TARGA         TARGA
  THEN          xTHEN
  TO            xTO
  TO#           TO#
  TRACE         TRACE
  UNTIL         xUNTIL
  VALUE         xVALUE
  VARIABLE      xVARIABLE
  WHILE         xWHILE
  ;;;NOFORTH;;;   ;;;NOFORTH;;;

\ ;;;NOFORTH;;; MUST be the last item in the list.

hex
0 value CHK
20 value B/LINE
0 value fileid
: sluit fileid close-file cr  ." close-file " . ;
: zend ( a n -- )  ( 2dup type ) fileid write-file drop
  \ ?dup if dup . sluit then abort" err "
 ;
: .XX   ( n -- )        dup chk + to chk  0 <# # # #> zend ;
: .XXXX ( n -- )        dup 8 rshift .xx .xx ;
create NEWLINE hex 0D c, 0A c, align
: INTELHEX ( begina count -- )  \ hosta
cr .s
   over + swap                  \ enda begina
   begin 2dup - b/line umin     \ enda begina len

   dup                          \ #bytes
   while                        \ 1 line
      s" :" zend  0 to chk       \ record mark
      dup .xx                   \ reclen
      over .xxxx                \ load offset \ address
      0 .xx                     \ rectyp
      0 do count .xx loop       \ data
      chk negate .xx            \ chksum
      newline 2 zend
  repeat drop 2drop ;

: ihx ( -- )
  myfile count
  cr 2dup type
  w/o create-file cr ." create file " .
  to fileid
  FROZEN hosta 40 intelhex
  origin hosta rom-end over - intelhex
  ivecs hosta ivecs negate (16)  intelhex
  s" :00000001FF" zend
  newline 2 zend
  fileid close-file  cr  ." close-file " .
  ;

only forth also definitions
marker -TARG

cr .(   metacompiler loaded   )

