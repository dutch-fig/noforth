( For noForth C2553 lp.0, C&V version: Port input at P1 & output at P2 
  with MSP430G2553 )

( Port-2 must be wired to 8 leds, placed on the launchpad experimenters kit.
  Wire P2.0 to P2.7 to the anode of eight 3mm leds placed on the breadboard 
  the pinlayout can be found in the hardwaredoc of the launchpad. Connect
  all kathodes to each other, and connect them to ground using a 100 Ohm 
  resistor. Port-1 bit 3 holds a switch on the Launchpad board.
  )

HEX
( Address 020 = p1in, port-1 input register
  Address 022 = p1dir, port-1 direction register
  Address 029 = p2out, port-2 output with 8 leds
  Address 02A = p2dir, port-2 direction register
  Address 02E = p2sel, port-2 selection register 
)

\ Use bit 3 to make a delay value (S?)
: WAIT          ( -- )      008 ( bit-3 ) 020 ( p1in ) bit*  10 * ms ;  
: >LEDS         ( b -- )    029 ( p2out ) c! ;  \ Store pattern b at leds

: FLASH         ( -- )   \ Visualise startup
    -1 >leds  064 ms     \ All leds on
    00 >leds  064 ms ;   \ All leds off

: SETUP-PORTS   ( -- )
    08 022 ( p1dir ) *bic  \ Port-1 bit 3 is input (S?)
    00 02E ( p2sel) c!     \ Port-2 all bits I/O
    -1 02A ( p2dir ) c!    \ All bits of P2 are outputs
    ;

: COUNTER       ( -- )  \ Binary counter
    setup-ports  flash
    0                   \ Counter on stack
    begin
        1+              \ Increase counter
        dup >leds  wait
    key? until          \ Until key pressed
    ;

: RUNNER        ( -- )  \ A running light
    setup-ports  flash
    begin
        8 0 do          \ Loop eight times
            1 i lshift  \ Make bitpattern
            >leds  wait
        loop
    key? until          \ Until a key is pressed
    ;

freeze
