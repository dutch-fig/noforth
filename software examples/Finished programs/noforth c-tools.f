\ Tools for noForth C
\ (C) 2015, Albert Nijhof & Willem Ouwerkerk

\ updated april 2015

HEX     \ until end of file!

\ STOP? is needed for WORDS DUMP MANY MSEE & SEE
: STOP?     ( - true/false )
   key? dup 0= ?EXIT  drop
   key  bl over = if drop key then
   01B over = ?abort
   bl <> ;

\ .CH is needed for DUMP MSEE & SEE
: .CH   ( x -- ) dup 7F < and bl max emit ;

: .S        ( -- )
   ?stack (.) space
   depth false
   ?do  depth i - 1- pick
      base @ 0A = if . else u. then
   loop ;

\ C-version only
\ ----- WORDS IWORDS using STOP?
: WORDS     ( -- )
   sn( [ ' 0= , ]
   \   begin ( for IWORDS and ALLWORDS )
   >r
   false >r                     \ counting
   fhere pfx-link hot - hot dup
   2over move drop              \ threads > fhere
   bounds
   cr
   BEGIN false dup              \ threada lfa
      2over
      do
         dup i @ u< if 2drop i dup @ then       \ threada lfa
      2 +loop dup stop? 0= and
   WHILE        \ threada lfa
        dup 1+ c@ 40 and
        2r@ drop execute        ( 0= for words, noop for iwords *** )
        if r> 1+ >r             \ counting
           dup lfa>n count 1F and
           0C over 2swap space type
           - dup 0<   r@ 6 mod 0=   or       \ too long or too many
           if drop cr else spaces then
        then
        lnk@ swap !          \ unlink
   REPEAT
   2drop 2drop 2r> (.) 0 .r drop ;

: IWORDS         ['] noop [ ' words 6 + 22 ] again ;
\ : ALLWORDS       ['] 1+   [ ' words 6 + 22 ] again ;

: DUMP  ( a n -- )       \ n must be in [0,7FFF]
   over + 1- >r                                 \ r: enda-1
   0FF s>d du.str nip 1+ swap                   \ .r a
   base @ swap
   BEGIN cr dup 2over drop 2* u.r ." : "        \ .r base a
      over s>d do count 2over drop .r
               loop over - ."  |"               \ .r base a
      over s>d do count .ch loop ." |"          \ .r base a+
      r@ over - 0< stop? or
   UNTIL r> 2drop 2drop ;

\ ----- SEE & MSEE
: >NFA          ( a -- nfa | 0 )
   dup origin
   chere within                 \ in noForth ROM?
   IF dup 1 and 0=              \ even?
   IF 1- dup c@ FF = +          \ skip alignment char
   0 begin over c@ 21 7F within \ char?
     while -1 /string 20 over < \ walk backwards through name
     until then ?dup            \ nfa? count-non-zero?
   IF over c@ 3F and =          \ count ok?
   IF dup 1 and ?EXIT           \ nfa odd -> ok
   THEN THEN THEN THEN drop 0 ;
: DECOM         ( a -- )
   cr dup 6 u.r space   \ .adr
   dup count .ch c@ .ch \ .2chars
   dup @ 6 u.r space    \ .contents
   dup >nfa ?dup
   if ." ----- "
      count tuck 1F and type            \ .name
      80 < if ."  (imm)" then
      @ cell- >nfa ?dup
      if ."  does " count 1F and type   \ .doer
      then EXIT                         \ ----
   then dup @                           \ contents
   dup 1 and                            \ odd
   if dup -7800 < if ." UNTIL -" else
      dup -7000 < if ." IF +"   else
      dup  7000 < if 2/ ." #" u. drop EXIT then    \ a number
      dup  7800 < if ." AGAIN -" else ." AHEAD +"
      then then then FFF and 7FF - + ." > " u. EXIT
   then nip   dup >nfa ?dup
   if count 1F and type drop EXIT       \ .name of compiled word
   then dup hot here within             \ RAM location?
   if origin
      begin begin cell+ chere over u<
                  if 2drop EXIT
                  then 2dup @ =
         until dup cell- >nfa ?dup
            if count 1F and type ."   RAM location "
            then
      again
   then
   4F00 = if ." nxt jmp "  then
   ;
: MSEE  ( a -- ) FFFE and begin dup decom cell+ stop? until drop ;
: SEE   ( <name> -- )  ' msee ;

: MANY  ( -- )  >in @ stop? and >in ! ;

shield TOOLS\ freeze
\ <><>
