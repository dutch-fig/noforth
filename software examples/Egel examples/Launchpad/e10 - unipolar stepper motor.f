( For noForth C2553 lp.0, C&V version: Unipolar four phase stepper motor control 
  Port input at P1 & output at P2 with MSP430G2553 )

( Port-2 must be wired to 4 transistors, placed on the launchpad experimenters
  kit. Wire P2.0 to P2.3 to the basis of four transistors placed on the 
  breadboard the pinlayout can be found in the hardwaredoc of the launchpad. 
  Connect a stepper motor to the four collectors of the transistors.
  Port-1 bit 3 holds a switch on the Launchpad board.
  )

HEX
( Address 020 = p1in, port-1 input register
  Address 021 = p1out, port-1 input register
  Address 022 = p1dir, port-1 direction register
  Address 029 = p2out, port-2 output with 8 leds
  Address 02A = p2dir, port-2 direction register
  Address 02E = p2sel, port-2 selection register )

( 029 - motor      Stepper motor output P2.0 - P2.3
  02A - motordir
  020 - input      Switch input P1.3
  021 - inputdir )

hex
: DIR?      08 020 bit* ;           \ Forward/backward selection P1.3

value STEP                          \ Next output word
value STEP#                         \ Table mask

create 1PHASE  8 c, 4 c, 2 c, 1 c,                     \ Single phase
create 2PHASE  9 c, C c, 6 c, 3 c,                     \ Double phase
create HALF    9 c, 8 c, C c, 4 c, 6 c, 2 c, 3 c, 1 c, \ Halfstep

\ Select one of three stepper methods in the word ONE-STEP
: ONE-PHASE     ( -- )      step 1phase + c@  029 c!  3 to step# ;
: TWO-PHASE     ( -- )      step 2phase + c@  029 c!  3 to step# ;
: HALF-STEP     ( -- )      step half  + c@   029 c!  7 to step# ;
: WAIT          ( -- )      20 ms ;
: ONE-STEP      ( -- )      one-phase  ( two-phase ) ( half-step ) ;

: FORWARD       ( -- )              \ Motor one step forward
    step 1+  step# and  to step  one-step  wait ;

: BACKWARD      ( -- )              \ Motor one step backward
    step 1-  step# and  to step  one-step  wait ;

: SETUP-STEP    ( -- )
    0F 02A *bis                     \ P2 low 4-bits are output
    18 021 *bic  18 027 *bis        \ Bit 3+4 of P1 are input with pullup
    3 to step#                      \ Set default table mask
    0 to step ;                     \ Start with first step

: TURN          ( -- )              \ Choose motion direction of motor
    dir? if  forward exit  then  backward ;

: STEPPER       ( -- )              \ Control a 4 phase stepper motor
    setup-step  begin  turn  key? until ;

' setup-step  to app  freeze

                            ( End )
