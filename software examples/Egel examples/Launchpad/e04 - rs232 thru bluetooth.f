( For noForth C&V2553 lp.0, Port output with MSP430G2553 at port-2. )

( RS232 via USB or Bluetooth in- and output met, with a copy at the LEDS.
  Use Bluetooth instead of USB serial connection... for MSP430G2553 version-A. 
  Change KEY to show characters from Bluetooth or USB at the LEDS, the 
  Launchpad has only uart0, so no fancy tricks here. Use the HC06 bluetooth
  module here only four wires need to be connected to the module. )
  
( For Bluetooth two jumpers need to be removed, the TX and RX jumpers at J3 
  Connect the power for the HC06 {pin12+13} with Launchpad J6 {VCC+GND}, 
  TX & RX from HC06 {pin1+2} with Launchpad J1 {pin3+4} )

HEX
( Address 029 p2out, port-2 output with 8 leds
  Address 02A p2dir, port-2 direction register
  Address 02E p2sel, port-2 selction register 
  )

\ This key shows also the binary chracter pattern on the leds
: >LEDS         ( u -- )    029 c! ;    \ Store pattern u at leds
: KEY*          ( c -- )    key)  dup >leds ;


: FLASH         ( -- )                  \ Visualise startup
    FF >leds  064 ms                    \ All leds on
    00 >leds  064 ms ;                  \ All leds off

: SETUP         ( -- )
    00 02E c!                           \ Port-2 all bits I/O
    -1 02A c! ;                         \ All bits of P2 are outputs

: STARTUP       ( -- )
    setup  flash                        \ Show new boot sequence
    ['] key* to key ;                   \ Install new KEY

' startup to app  freeze

\ End
