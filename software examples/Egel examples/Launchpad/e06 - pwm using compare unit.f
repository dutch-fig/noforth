( For noForth 2553 lp.0, C&V version: Port input using switches
  at P1.4 & P1.5 . Using the onboard P1.0 led as output.
  Pulswidth power control with 4 KHz PWM at P2.4 or P2.5 
  with a resolution of 2000 steps )

( Port-1 must be wired to 2 switches, placed on the launchpad 
  experimenters kit, Wire P1.4 & P1.5 to the switches on the breadboard.
  The pinlayout can be found in the hardwaredoc of the launchpad.
  Port-1 bit 3 holds a switch on the Launchpad board. )

( P2.4 must be wired to a logic power FET like the BUK101 using
  a suppression diode like 1N4001. Any 5 or 6 Volt DC motor
  or lamp may be used. Take care for the maximum USB current! )

HEX
( Address 020 = p1in, port-1 input register
  Address 022 = p1dir, port-1 direction register
  Address 029 = p2out, port-2 output with 8 leds
  Address 02A = p2dir, port-2 direction register
  Address 02E = p2sel, port-2 selection register 
  Address 180 = ta1ctl, timer a1 compare mode 
  Address 186 = ta1cctl2, timer a1 output mode 
  Address 192 = ta1ccr0, timer a1 period timing 
  Address 196 = ta1ccr2, timer a1 Duty cycle )

( FEDCBA9876543210 bit-numbers
  0000000000010000 - Choose output bit4 or bit5
  0000000011000000 - toggle-set output
  0000001000110100 - TA clear, up/down, SMCLK, no presc. )

\ Period length is 2000 clock cycles ( 2x#CYCLUS )
dm 1000 constant #CYCLUS
value POWER

: STOP-TIMER1       0 180 ! ;
: SET-PWM           #cyclus umin  196 ! 010 029 *bic ;  ( 0 to #CYCLUS -- )
: PWM-OFF           stop-timer1  010 02E *bic ; 

\ PWM AT 2.4 or P2.5
: SETUP-PWM         ( -- )
    038 022 *bic 038 020 *bis   \ P1.3 to P1.5 are inputs
    038 027 *bis                \ with pullup
    010 02E *bis 010 02A *bis   \ Set PWM to output pin P2.4
    stop-timer1  #cyclus 192 !  \ Set period time
\   0C0 186 !                   \ Set output mode negative pulse
    040 186 !                   \ Set output mode positive pulse
    234  180 !                  \ Activate timer
    0 to power ;

: LOWER             power if  -1 +to power  then ;
: HIGHER            power #cyclus u< if  1 +to power  then ;

\ The switch on P1.4 is the increase speed key
\ The switch on P1.5 is the decrease speed key
: SET-POWER         ( -- )
    010 020 bit* 0= if  higher  then
    020 020 bit* 0= if  lower   then
    power set-pwm  2 ms  power .
    ;
            
\ Two demonstrations of pulsewidth modulation power control
: CYCLUS        setup-pwm  #cyclus 1+ 0 ?do  i set-pwm 02 ms  loop  pwm-off ;
: POWERCONTROL  setup-pwm  begin  set-power  key? until  pwm-off ;

pwm-off  freeze

                              ( End )
