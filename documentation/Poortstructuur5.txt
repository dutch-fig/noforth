Poortstructuur van MSP serie 5 en hoger:

De I/O poorten van de MSP430 hebben een vaste structuur.
Een basisadres en een offset tot elk besturingsregister.
De basis adressen voor een MSP vindt je in zijn persoonlijke datasheet.
De offset van de IO-poorten zijn:

Functie     P1, P3, etc.    P2, P4, etc.    Label naam
-------------------------------------------------------
Ingang          00              01              PxIN
Uitgang         02              03              PxOUT
Richting        04              05              PxDIR
Weerstand aan   06              07              PxREN
Select 0        0A              0B              PxSEL0
Select 1        0C              0D              PxSEL1
Port complement 10              11              PxSELC
Int. edge aan   18              19              PxIES
Int. aan        1A              1B              PxIE
Int. vlag       1C              1D              PxIFG
-------------------------------------------------------

De meeste registers worden op nul geinitialiseerd, lees
het datasheet van de gebruikte MSP voor meer details.

Het is ook mogelijk de poorten te koppelen tot 16-bits,
P1 en P2 vormen zo samen een 16-bits poort genaamd PA.
PB wordt gevormd door P3 en P4 samen, etc. daarom vindt 
je ze boven in de tabel terug op even en oneven adressen.

Als een I/O-pin niet aangesloten wordt beveelt TEXAS aan
de pin als uitgang te configureren.


De drie registers PxDIR, PxREN en PxOUT worden gebruikt 
om een I/O-pin als volgt te configuren:

PxDIR   PxREN   PxOUT   Pin configuratie
-------------------------------------------------
  0       0       x      Zwevende ingang
  0       1       0      Ingang met R naar massa
  0       1       1      Ingang met R naar plus
  1       x       x      Uitgang
-------------------------------------------------


De registers PxSEL0 en PxSEL1 worden gebruikt om een
speciale functie aan een I/O-pin te koppelen. Hiermee
kan b.v. de ADC of de UART ingeschakeld worden. In het
datasheet van de gebruikte MSP is meer info te vinden.
Als volgt:

PxSEL0  PxSEL1    I/O-functie
------------------------------------------
  0       0         Gewone I/O
  0       1         Basis extra functie
  1       0         Tweede extra functie
  1       1         Derde extra functie
------------------------------------------

 
Interrupt vectoren MSP430FR5739 (Ontwikkelkit)
FFCC    - EINDE FLASH
FFCE    - RTC
FFD0    - P4
FFD2    - P3
FFD4    - TIMER B2 CCR1 CCR2
FFD6    - TIMER B2 CCR0
FFD8    - P2
FFDA    - TIMER B1 CCR1 CCR2 
FFDC    - TIMER B1 CCR0
FFDE    - P1
FFE0    - TIMER A1 CCR1 CCR2
FFE2    - TIMER A1 CCR0
FFE4    - DMA
FFE6    - USCI 1 RX/TX
FFE8    - TIMER A0 CCR1 CCR2
FFEA    - TIMER A0 CCR0
FFEC    - ADC10
FFEE    - USCI 0 RX/TX 
FFF0    - USCI 0 RX/TX 
FFF2    - WATCHDOG
FFF4    - TIMER B0 CCR1 CCR2
FFF6    - TIMER B0 CCR0
FFF8    - COMPARATOR
FFFA    - NMI USER
FFFC    - NMI SYSTEM
FFFE    - RESET


\ IO-poort layout MSP430FR5739 (Ontwikkelkit)
P1.0    - ...
P1.1    - ...
P1.2    - ...
P1.3    - ...
P1.4    - NTC
P1.5    - ...
P1.6    - ...
P1.7    - ...

P2.0    - RX
P2.1    - TX
P2.2    - ...
P2.3    - ...
P2.4    - ...
P2.5    - ...
P2.6    - ...
P2.7    - ...

P3.0    - XOUT
P3.1    - YOUT
P3.2    - ...
P3.3    - LDR
P3.4    - LED5
P3.5    - LED6
P3.6    - LED7
P3.7    - LED8

P4.0    - S1
P4.1    - S2

PJ.0    - LED1
PJ.1    - LED2
PJ.2    - LED3
PJ.3    - LED4
PJ.4    - XINB
PJ.5    - XOUTB
