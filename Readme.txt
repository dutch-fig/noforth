Launchpad example files:

This Launchpad map is devided in four maps.

1) Latest noForth binary's

2) Informative files about Launchpad and MSP430

3) Cross assembler for Win32Forth, to build code
   words for use in noForth without having to add
   the noForth assembler too.


4) Software examples, divided in two sections:

  a) Finished programs (may only be changed by A.N. or W.O.)
    a1) Finished programs are divided in four sections
      1) Universal programs that run on all versions
      2) Launchpad MSP430G2553 programs
      3) MSP430FR5739 programs
	  4) MSP430F149 programs

  b) Experimental programs (programs under development)
    b1) Experimental programs have several subdirectory's.
        Each user may add one for the development of a 
        specific piece of software. That may or may not 
        be MSP430 hardware related.

