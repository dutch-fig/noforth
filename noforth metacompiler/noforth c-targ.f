\ noForth C target code 20150330
\ Copyright (C) 2015, Albert Nijhof & Willem Ouwerkerk
\ update april 2015

\    This program is free software: you can redistribute it and/or modify
\    it under the terms of the GNU General Public License as published by
\    the Free Software Foundation, either version 3 of the License, or
\    (at your option) any later version.
\
\    This program is distributed in the hope that it will be useful,
\    but WITHOUT ANY WARRANTY; without even the implied warranty of
\    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\    GNU General Public License for more details.
\
\    You should have received a copy of the GNU General Public License
\    along with this program.  If not, see <http://www.gnu.org/licenses/>.

\ 27jan2015 -- inside words made simpler -- an

only forth also definitions
-targ
marker -targ

notrace
HEX \ throughout
CV? ?Cversion
select-processor
\ Hardware labels for MSP chips, in host-forth
HEX
META definitions also forth

<<noforth A                                                     \ ===============A
\ Voor MSP430G2553
0200 to HOT      0400 to RAM-END
1080 to FROZEN   C000 to ORIGIN   FFDE to IVECS
\ constants for Flash programming and initialisation
A500 constant fkey              \ Access to Flash key
0002 constant fwis              \ Erase flash command
0010 constant flock             \ Close Flash access command
0040 constant fwrite            \ Write Flash command
0128 constant fctrl1            \ Flash control registers 1, 2 and 3
012A constant fctrl2
012C constant fctrl3
\ Cold start labels
0000 constant INTenable         \ Basic interrupt setup
0002 constant int-flags
0120 constant wdctrl            \ Watchdog
0056 constant DCOctrl           \ RC oscillator
0057 constant CLKctrl
0021 constant P1out             \ IO registers for Port 1
0022 constant P1dir
0026 constant P1sel
0027 constant P1res
0041 constant P1sel2
\ RS232 (UART)
0003 constant UARTctrl0         \ Flags register
0061 constant UARTctrl1         \ For setup of clock
0062 constant UARTbaud0         \ Baudrate control
0063 constant UARTbaud1
0064 constant UARTmodctrl       \ UCBRSx bit 2 needs te be set
0067 constant uart-out          \ Data registers output and input
0066 constant uart-in
\ 0001 constant rx?              \ Char received bit
\ 0002 constant tx?              \ Hardware ready to send another char
>>all

<<noforth BDE                                                   \ =============BDE
0200 to HOT      0A00 to RAM-END
1000 to FROZEN   1100 to origin   FFDE to IVECS
\ constants for Flash programming and initialisation
A500 constant fkey              \ Access to Flash key
0002 constant fwis              \ Erase flash command
0010 constant flock             \ Close Flash access command
0040 constant fwrite            \ Write Flash command
0128 constant fctrl1            \ Flash control registers 1, 2 and 3
012A constant fctrl2
012C constant fctrl3
\ Cold start labels
0000 constant IE1               \ Basic interrupt setup
0002 constant IFG1
0003 constant IFG2
0004 constant ME1
0005 constant ME2
0120 constant wdctrl            \ Watchdog
0056 constant DCOctrl           \ RC oscillator
0057 constant CLKctrl1
0058 constant CLKctrl2
0020 constant P1in              \ IO registers for Port 1
0021 constant P1out
0022 constant P1dir
0026 constant P1sel
0027 constant P1ren
0029 constant P2out             \ IO registers for Port 2
002A constant P2dir
0018 constant P3in              \ IO registers for Port 3
0019 constant P3out
001A constant P3dir
001B constant P3sel
\ 0027 constant P3ren
\ RS232 (UART0)
0070 constant U0CTL             \ Flags register
0071 constant U0TCTL            \ For setup of clock ????
0074 constant U0BR0             \ Baudrate control
0075 constant U0BR1
0073 constant U0MCTL            \ UCBRSx bit 2 needs te be set
0077 constant U0TXBUF           \ Data registers output and input
0076 constant U0RXBUF
\ RS232 (UART1)
0078 constant U1CTL             \ Flags register
0079 constant U1TCTL            \ For setup of clock ????
007C constant U1BR0             \ Baudrate control
007D constant U1BR1
007B constant U1MCTL            \ UCBRSx bit 2 needs te be set
007F constant U1TXBUF           \ Data registers output and input
007E constant U1RXBUF
>>all

<<noforth F                                                     \ ===============F
1C00 to HOT      2000 to RAM-END
18C0 to FROZEN   C200 to ORIGIN   FF7E to IVECS
>>all

<<noforth G                                                     \ ===============G
1C00 to HOT      2400 to RAM-END
18C0 to FROZEN   4400 to ORIGIN   FF7E to IVECS
>>all

<<noforth FG                                                    \ ===============F
\ Voor MSP430FR5739
\ Cold start labels
0000 constant INTenable         \ Basic interrupt setup
0002 constant int-flags
0120 constant wdctrl            \ Watchdog
0056 constant DCOctrl           \ RC oscillator
0057 constant CLKctrl
0021 constant P1out             \ IO registers for Port 1
0022 constant P1dir
0026 constant P1sel
0027 constant P1res
0041 constant P1sel2
\ RS232 (UART)
05DC constant UARTctrl0         \ Flags register
0061 constant UARTctrl1         \ For setup of clock
0062 constant UARTbaud0         \ Baudrate control
0063 constant UARTbaud1
0064 constant UARTmodctrl       \ UCBRSx bit 2 needs te be set
05CE constant uart-out          \ Data registers output and input
05CC constant uart-in
\ 0001 constant rx?               \ Char received bit
\ 0002 constant tx?               \ Hardware ready to send another char
>>all                                                           \ ================

FORTH DEFINITIONS

<----
-- compiler security numbers, names are only in the meta compiler
   Forth
11 sys\if      See then ahead while
22 sys\begin   See until again
33 sys\do      See ?do loop +loop
44 sys\:       See : ; does> ;code :doer :noname
55 sys\code    See end-code ;code does> codedoer
   Assembler
66 sys\if,      See then, ahead, repeat,
77 sys\begin,   See until, again, repeat,
---->
\ -----------------------------------------------------------------------
\ ------------------------ noForth Target code --------------------------
\ -----------------------------------------------------------------------

:::NOFORTH:::
\ NOOP = NEXT routine ------ 15feb2014
\ read ip               function
\ any even number       NORMAL NEXT
\ odd [8001,8FFF]       IF UNTIL WHILE distance [F800,7FE]
\ odd [9001,6FFF]       NUMBER         in [-3800,37FF]
\ odd [7001,7FFF]       AHEAD AGAIN    distance [F800,7FE]
\ forth: code NOOP
   ip )+ w mov
   #1 w bit   =?
   if,   w )+ pc mov            \ w even NORMAL EXIT
   then,
   9000 # w cmp   >?
   if,                          \ w odd in [-7FFF,-7001] [8001,8FFF]
\ ----- IF UNTIL WHILE
       #0 tos cmp sp )+ tos mov \ flag=?
       =? if,                   \ jump on false
          8801 # w sub          \ w even in [F800,7FE] [-800,7FE]
          w ip add
       then, NEXT
   then,
   7000 # w cmp   >?
   if,                          \ w odd in [-6FFF,6FFF] [9001,6FFF]
\ ----- NUMBER
       w rra                    \ number all in [-3800,37FF] [C800,37FF]
       tos sp -) mov
       w tos mov NEXT
   then,                        \ w odd in [7001,7FFF]
\ ----- AHEAD AGAIN
   7801 # w sub                 \ w even in [-800,7FE] [F800,7FE]
   w ip add NEXT
forth: header NOOP origin ,

\ ----- multiply & divide -----
\ --- A'dam
forth: code UM* ( x y -- plo phi )    \ tos:y sun,moon:x tos,W:phi,plo
   #0 sun mov   sp ) tos cmp u<eq?
   if,   tos moon mov   sp ) day mov    \ day=lowest
   else, LABEL-AMSTERDAM                \ for UD*S
         sp ) moon mov   tos day mov
   then, #0 w mov   #0 tos mov          \ plo=phi=0
   begin, #1 sr bic   day rrc cs?
       if,   moon w add   sun tos addc          \ x +to prod
       then, moon moon add   sun sun addc       \ x lshift
       #0 day cmp
   =? until,   w sp ) mov   NEXT
   end-code

extra: code DU*S      ( xlo xhi y -- plo phi )
   sp )+ sun mov   AMSTERDAM jmp
   end-code

\ --- A'dam R'dam
\ subroutine for UM/MOD en DU/S
   LABEL-AMSTERDAM     \ tos:y sun,moon:xhi,xlo -> sun,moon:rest,quot
   tos sun cmp u<eq?   \ overflow?
   if,   #-1 sun mov   sun moon mov
         rp )+ pc mov ( return )        \ overflow quot=rest=-1
   then, LABEL-ROTTERDAM                \ for DU/S no overflow test
   #1 day mov                           \ init counter
   begin, moon moon add   sun sun addc
       cc? if, tos sun cmp
       u<eq? if,
       2SWAP then,   tos sun sub   #1 moon add
       then,
          day day add cs?               \ bit comes out?
   until, rp )+ pc mov                  \ *** pc-fout?

forth: code UM/MOD    ( xlo xhi y -- rest quot )
   sp )+ sun mov
   sp ) moon mov
   AMSTERDAM # call
   sun sp ) mov         \ rest
   moon tos mov         \ quot
   NEXT
   end-code

extra: code DU/S ( xlo xhi y -- qlo qhi rest )  \ rest in tos!
   #0 sun mov   sp ) moon mov                   \ sun,moon:0,xhi
   ROTTERDAM # call
   moon sp ) mov        \ qhi
   2 sp x) moon mov     \ sun,moon:rest,xlo
   ROTTERDAM # call
   moon 2 sp x) mov     \ qlo
   sun tos mov          \ rest
   NEXT
   end-code

forth: code EXECUTE
   tos w mov   sp )+ tos mov   w )+ pc mov   end-code

\ --- A'dam
forth: code EXIT
   LABEL-AMSTERDAM   rp )+ ip mov   NEXT end-code
extra: code ?EXIT       ( flag -- )
   #0 tos cmp   sp )+ tos mov
   =? AMSTERDAM label-until,   NEXT end-code \ *** pc-fout?

extra: code DIVE       ( -- )
   ip w mov   rp ) ip mov   w rp ) mov   NEXT end-code
inside: code SN(        ( -- x )
   tos sp -) mov   ip )+ tos mov
   NEXT end-code        \ See LITERAL

\ ----- doers -----
\ for DOES> and :doer
   LABEL-DOESintro
   tos sp -) mov   w tos mov    \ databody >stack
   ip push
   -2 w x) ip mov       \ cfa@ to ip
   #4 ip add            \ skip 'doesintro pc mov'
   NEXT                 \ execute hi-level does-part

inside: :doer DODOER
( ) LABEL-THINGUMAJIG [ FFFF , ] ;      \ needs DOESintro
\ Forward reference. The romallotted cell will be patched with !DOER
\ The result is   :doer DODOER !DOER ;

inside: codedoer DOCOL   ip push   w ip mov   NEXT end-code
inside: codedoer DOBODY  tos sp -) mov   w   tos mov   NEXT end-code    \ docreate
inside: codedoer DO@     tos sp -) mov   w ) tos mov   NEXT end-code    \ docon & dovar
inside: codedoer DO@@    tos sp -) mov   w ) tos mov   tos ) tos mov    \ doval
                         NEXT end-code

extra: header ME ' dobody 2 + , myname,
\ ----- input output -----

<<noforth AFG                                                   \ =============AFG
\ Input and output for the Launchpad's board.
extra: code KEY?)      ( -- f )
   tos sp -) mov   #1 uartctrl0 & bit
   #0 tos mov   <>? if,   #-1 tos mov   then,   NEXT end-code
extra: code KEY)       ( -- c )
   begin,   #1 uartctrl0 & .b bit   <>? until,
   tos sp -) mov   uart-in & tos .b mov   NEXT end-code
extra: code EMIT)      ( c -- )
   begin,   #2 uartctrl0 & .b bit   <>? until,
   tos uart-out & .b mov   sp )+ tos mov   NEXT end-code
>>all

<<noforth B                                                     \ ===============B
\ noForth-B: Input and output for the F149 board UART1
\ UART1 = IFG2=03 bit4=10 RX? -- bit5=20 TX?
extra: code KEY?) ( -- f )
    tos sp -) mov   10 #  IFG2 & .b bit
    #0 tos mov   cs? if,   #-1 tos mov   then,   NEXT end-code
extra: code KEY)   ( -- c )
    begin,   10 #  IFG2 & .b bit   cs? until,
    tos sp -) mov   U1RXBUF & tos .b mov   NEXT end-code
extra: code EMIT)   ( c -- )
    begin, 20 #  IFG2 & .b bit  cs? until,
    tos U1TXBUF & .b mov   sp )+ tos mov   NEXT end-code
>>all

<<noforth D                                                     \ ===============D
\ noForth-D: Input and output for the F149 board UART0
\ UART0 = IFG1=02 bit6=40 RX? -- bit7=80 TX?
extra: code KEY?) ( -- f )
    tos sp -) mov   40 #  IFG1 & .b bit
    #0 tos mov   cs? if,   #-1 tos mov   then,   NEXT end-code
extra: code KEY)   ( -- c )
    begin,   40 #  IFG1 & .b bit   cs? until,
    tos sp -) mov   U0RXBUF & tos .b mov   NEXT end-code
extra: code EMIT)   ( c -- )
    begin, 80 #  IFG1 & .b bit  cs? until,
    tos U0TXBUF & .b mov   sp )+ tos mov   NEXT end-code
>>all

<<noforth E                                                     \ ===============E
\ noForth-D: Input and output for the F149 board UART0
\ UART0 = IFG1=02 bit6=40 RX? -- bit7=80 TX?
extra: code KEY?) ( -- f )
    tos sp -) mov   40 #  IFG1 & .b bit
    #0 tos mov   cs? if,   #-1 tos mov   then,   NEXT end-code
extra: code KEY)   ( -- c )
    begin,   40 #  IFG1 & .b bit   cs? until,
    tos sp -) mov   U0RXBUF & tos .b mov   NEXT end-code
extra: code EMIT)   ( c -- )
    begin, 80 #  IFG1 & .b bit  cs? until,
    tos U0TXBUF & .b mov   sp )+ tos mov   NEXT end-code
>>all

\ ----- inline arguments -----
\ !!! To be used in words that read an inline argument
inside: code INL# ( -- x )      \ See COMPILE(
\ Put inline number on stack.
   tos sp -) mov
   rp ) w mov           \ r@
   w )+ tos mov         \ inl#
   w rp ) mov           \ skip inl#
   NEXT end-code
inside: code INLS ( -- csa )    \ See S" C" ." ABORT"
\ Inline string -> stack (counted string)
   tos sp -) mov
   rp ) tos mov         \ counted str adr
   tos ) w .b mov       \ count
   #1 w add             \ bytes to be skipped
   #1 w bit             \ odd?
   #0 w addc            \ make even
   w rp ) add           \ skip the string
   NEXT end-code

\ Words that read inline arguments at the same level end in "("

\ ----- DO LOOP primitives -----
\ DOX = do-index register, only to be used by do-loops.
\ 3 elements on return stack: saved-dox   actual-dif   redoa
\ dif = 8000 - limit   ->   i = dox - dif
\ --- R'dam
inside: code DO(        ( lim index -- )
   LABEL-ROTTERDAM                      \ for ?do
   dox push   \ = previous index
   8000 # w mov   sp )+ w sub   w push  \ = 8000 - limit = dif
   #2 ip add   ip push                  \ = redoa \ skip leavea
   tos dox mov   w dox add              \ index + dif = dox = runtime index
   sp )+ tos mov   NEXT end-code
inside: code ?DO(                       \ See ?DO
   sp ) tos cmp
   =? ROTTERDAM label-until,                  \ -> DO(
   #2 sp add   sp )+ tos mov            \ 2drop
   ip ) ip mov NEXT  end-code           \ do not loop -> GO(

\ --- R'dam
forth: code LEAVE       ( -- )
   rp ) w mov   #2 w sub   w ) ip mov   \ redoa 2 - @ = leavea
   LABEL-ROTTERDAM                      \ for loop) & unloop
   #4 rp add                            \ throw redoaa & dif
   rp )+ dox mov   NEXT end-code        \ restore dox

forth: header UNLOOP    ( -- )
   ROTTERDAM ,

\ --- A'dam
inside: code LOOP)      ( -- )  \ See LOOP
   #1 dox add
   LABEL-AMSTERDAM              \ for +loop)
   100 # sr bit
   =? ROTTERDAM label-until,          \ -> unloop
   rp ) ip mov   NEXT end-code  \ redo
inside: code +LOOP)     ( x -- )        \ See +LOOP
   tos dox add
   sp )+ tos mov
   AMSTERDAM jmp end-code       \ -> loop)

\ 3 nested loops on rstack ( idox = dox )
\ | ?dox kdif kredoa | kdox jdif jredoa | jdox idif iredoa |
\ 10   0E   0C       0A   08   06       04   02   00

forth: code I   ( -- i )                \ real index = dox - dif
   tos sp -) mov
   dox tos mov   2 rp x) tos sub        \ i = dox - idif
   NEXT end-code

forth: code J   ( -- j )
   tos sp -) mov
   4 rp x) tos mov   8 rp x) tos sub    \ j = jdox - jdif
   NEXT end-code

<----
forth: code K          ( -- k )
   tos sp -) mov
   0A rp x) tos mov   0E rp x) tos sub  \ k = kdox - kdif
   NEXT end-code
---->

extra: 2        constant CELL
forth: 20       constant BL
extra: HOT      constant HOT
extra: FROZEN   constant FROZEN
extra: RAM-END  constant R0
extra: ORIGIN   constant ORIGIN
extra: IVECS    constant IVECS

\ ----- system data -----
\ 'cold-start' data in ram
( ) dictionary-threads cells RAMALLOT

\ C-version
inside: variable PFX-LINK       \ prefix-list, the 1st var after the threads
\ inside: variable I-LINK ***
\ - - -

( ) ' EMIT) RAMHERE !   extra: value 'EMIT
( ) ' KEY?) RAMHERE !   extra: value 'KEY?
( ) ' KEY)  RAMHERE !   extra: value 'KEY
( ) ' NOOP  RAMHERE !   extra: value APP

inside: value ROMP      \
forth: value HERE

<<noforth AFG                                                   \ =============AFG
( ) RAM-END D0 -  RAMHERE !   extra: value TIB  \ Terminal Input Buffer
( ) RAM-END 80 -  RAMHERE !   extra: value TIB/ \ tib-end & s-bottom
( ) RAM-END 40 -  RAMHERE !   extra: value S0   \ s-top & r-bottom
>>all

<<noforth BDE                                                   \ =============BDE
( ) RAM-END 150 - RAMHERE !   extra: value TIB  \ Terminal Input Buffer
( ) RAM-END 100 - RAMHERE !   extra: value TIB/ \ tib-end & s-bottom
( ) RAM-END 80 -  RAMHERE !   extra: value S0   \ s-top & r-bottom
>>all

<<noforth G                                                     \ ===============G
( ) RAM-END 150 - RAMHERE !   extra: value TIB  \ Terminal Input Buffer
( ) RAM-END 100 - RAMHERE !   extra: value TIB/ \ tib-end & s-bottom
( ) RAM-END 80 -  RAMHERE !   extra: value S0   \ s-top & r-bottom
>>all                                                           \ ================
( ) 1C0     RAMHERE !   extra: value MS)       \ See MS
( ) 7       RAMHERE !   extra: value OK         \ See .OK
( ) 10      RAMHERE !   forth: variable BASE

\ ----- only warm -----
forth: variable STATE   \ ( -- adr )
forth: variable >IN     \ ( -- adr )
extra: value #IB        \ Inputbuffer len
extra: value IB         \ Formal Inputbuffer adr
extra: value CREATED   \ LFA of last created header in dictionary.
inside: value CP        \ : {FLY fp to cp ; : FLY} ROMP to cp ;
inside: variable FP     \ See FHERE = circular buffer pointer
inside: value HLD       \ See HOLD <# #>
inside: value ERR?      \ See OK -- the last system value
( ) RAMHERE ' HERE >BODY @ !

\ ----- memory operations -----
\ for VALUEs
inside: code TO(   ip )+ w mov   tos w ) mov   sp )+ tos mov   NEXT end-code
inside: code +TO(  ip )+ w mov   tos w ) add   sp )+ tos mov   NEXT end-code
inside: code INCR( ip )+ w mov   #1 w ) add   NEXT end-code

to#   pfx-for-value to(         \ 0
+to#  pfx-for-value +to(        \ 2
incr# pfx-for-value incr(       \ 4

\ inside: code !S0
\ ( ) ' S0 >BODY @ & sp mov   NEXT end-code
inside: code !R0
( ) RAM-END # rp mov   NEXT end-code
inside: code R? ( -- n )                \ for .OK
( ) tos sp -) mov   RAM-END # tos mov
    rp tos sub   10 # tos sub    NEXT end-code
\ Store and Fetch
forth: code C!   sp )+ w mov   w tos ) .b mov   sp )+ tos mov   NEXT end-code
forth: code !    sp )+ tos ) mov   sp )+ tos mov   NEXT end-code
\ forth: code 2!   sp )+ tos ) mov   #2 tos add   sp )+ tos ) mov
\                  sp )+ tos mov   NEXT end-code
forth: code +!   sp )+ tos ) add   sp )+ tos mov   NEXT end-code

\ extra: code C+!   sp )+ w mov   w tos ) .b add   sp )+ tos mov   NEXT end-code
\ extra: code 1+!   #1 tos ) add   sp )+ tos mov   NEXT end-code

forth: code C@     tos ) tos .b mov   NEXT end-code
forth: code @      tos ) tos mov   NEXT end-code
\ forth: code 2@   tos )+ w mov   tos )+ sp -) mov
\                  w tos mov   NEXT end-code
forth: code COUNT  tos w mov   w )+ tos .b mov   w sp -) mov   NEXT end-code
extra: code @+     tos w mov w )+ tos mov   w sp -) mov   NEXT end-code

\ ----- Return stack -----
forth: code >R    tos push   sp )+ tos mov   NEXT end-code
forth: code R>    tos sp -) mov   rp )+ tos mov   NEXT end-code
forth: code 2>R   sp )+ push   tos push   sp )+ tos mov   NEXT end-code
forth: code 2R>   #4 sp sub   tos 2 sp x) mov   rp )+ tos mov
                  rp )+ sp ) mov   NEXT end-code
forth: code R@    tos sp -) mov   rp ) tos mov   NEXT end-code
forth: code 2R@   #4 sp sub   tos 2 sp x) mov   rp )+ tos mov
                  rp ) sp ) mov   #2 rp sub   NEXT end-code
extra: code RDROP #2 rp add   NEXT end-code

\ ----- stack operations -----
forth: code 2DROP   #2 sp add   sp )+ tos mov   NEXT end-code
forth: code 2DUP   #4 sp sub   4 sp x) sp ) mov
                   tos 2 sp x) mov   NEXT end-code
forth: code 2NIP   sp ) w mov   #4 sp add   w sp ) mov   NEXT end-code
forth: code 2OVER  #4 sp sub   tos 2 sp x) mov   8 sp x) sp ) mov
                   6 sp x) tos mov   NEXT end-code
forth: code 2SWAP  4 sp x) w mov   sp ) 4 sp x) mov   w sp ) mov
                   2 sp x) w mov   tos 2 sp x) mov   w tos mov NEXT end-code
\ : 2TUCK   2SWAP 2OVER ; ( x1 x2 x3 x4 -- x3 x4 x1 x2 x3 x4 )
\ : 2ROT    2>R 2SWAP 2R> 2SWAP ;
\ : -2ROT   2SWAP 2>R 2SWAP 2R> ;
\ --- A'dam
forth: code DUP    LABEL-AMSTERDAM   tos sp -) mov   NEXT end-code
forth: code ?DUP   #0 tos cmp
                   =? AMSTERDAM label-until,
                   NEXT end-code \ *** pc-fout?
forth: code DROP   sp )+ tos mov NEXT end-code
forth: code OVER   sp ) w mov   tos sp -) mov
                   w tos mov   NEXT end-code
forth: code SWAP   sp ) w mov   tos sp ) mov   w tos mov   NEXT end-code
forth: code TUCK   sp ) w mov   w sp -) mov   tos 2 sp x) mov NEXT end-code
forth: code NIP    #2 sp add   NEXT end-code
forth: code PICK   tos tos add   sp tos add   tos ) tos mov NEXT end-code
forth: code ROT    2 sp x) w mov   sp ) 2 sp x) mov   tos sp ) mov
                   w tos mov   NEXT end-code
\ forth: code -ROT
\ tos w mov   sp ) tos mov   2 sp x) sp ) mov   w 2 sp x) mov NEXT end-code

\ ----- comparing -----
\ A'dam \ R'dam
forth: code MIN    sp )+ w mov   tos w cmp
                   LABEL-AMSTERDAM
                   >? if,   w tos mov   then, NEXT end-code
forth: code MAX    sp )+ w mov   w tos cmp   AMSTERDAM jmp   end-code
forth: code UMIN   sp )+ w mov   tos w cmp
                   LABEL-ROTTERDAM
                   u>? if,   w tos mov then, NEXT end-code
forth: code UMAX   sp )+ w mov   w tos cmp   ROTTERDAM jmp   end-code

\ A'dam \ R'dam
forth: code TRUE  tos sp -) mov   LABEL-AMSTERDAM   #-1 tos mov  NEXT end-code
forth: code FALSE tos sp -) mov   LABEL-ROTTERDAM   #0 tos mov   NEXT end-code
forth:
code =   sp )+ tos cmp    =? ROTTERDAM label-until,   AMSTERDAM jmp end-code
code <>  sp )+ tos cmp   <>? ROTTERDAM label-until,   AMSTERDAM jmp end-code
code >   sp )+ tos cmp    >? ROTTERDAM label-until,   AMSTERDAM jmp end-code
code u>  sp )+ tos cmp   u>? ROTTERDAM label-until,   AMSTERDAM jmp end-code
code 0=  #0 tos cmp       =? ROTTERDAM label-until,   AMSTERDAM jmp end-code
code 0<> #0 tos cmp      <>? ROTTERDAM label-until,   AMSTERDAM jmp end-code
code 0<  #0 tos cmp       >? ROTTERDAM label-until,   AMSTERDAM jmp end-code
code <   sp )+ w mov
         tos w cmp        >? ROTTERDAM label-until,   AMSTERDAM jmp end-code
code u<  sp )+ w mov
         tos w cmp       u>? ROTTERDAM label-until,   AMSTERDAM jmp end-code
\ code 0>  #0 w mov
\         tos w cmp       >? ROTTERDAM label-until,   AMSTERDAM jmp end-code

forth: code WITHIN
( ) sp ) tos sub   sp )+ sp ) sub   ' U< @ jmp   end-code
forth: code S>D
( ) tos sp -) mov   ' 0< @ jmp   end-code

\ ----- logical operations -----
forth: code AND   sp )+ tos and>  NEXT end-code
forth: code OR    sp )+ tos bis   NEXT end-code
forth: code XOR   sp )+ tos xor>  NEXT end-code
forth: code INVERT #-1 tos xor>   NEXT end-code

forth: code LSHIFT ( x1 n -- x2 ) \ qqq
   tos w mov
   sp )+ tos mov
   #0 w cmp <>?
   if,   begin,   tos tos add
                  #1 w sub   =?
         until,
   then,
   NEXT end-code
forth: code RSHIFT ( x1 n -- x2 ) \ qqq
   tos w mov
   sp )+ tos mov
   #0 w cmp <>?
   if,   begin,   clrc   tos rrc
                  #1 w sub   =?
         until,
   then,
   NEXT end-code

extra: code ><   tos swpb   NEXT end-code ( swap bytes )

\ 16-bit bitset and bitclear operators (noForth style)
code **BIS  ( m a -- )   sp )+ tos ) bis  sp )+ tos mov  NEXT  end-code
code **BIC  ( m a -- )   sp )+ tos ) bic  sp )+ tos mov  NEXT  end-code
code **BIX  ( m a -- )   sp )+ tos ) xor>  sp )+ tos mov  NEXT  end-code
code BIT**  ( m a -- f ) tos ) tos mov  sp ) tos and> #2 sp add  NEXT  end-code

extra: code *BIS        ( mask addr -- )
   sp ) tos ) .b bis   #2 sp add   sp )+ tos mov   NEXT end-code
extra: code *BIC        ( mask addr -- )
   sp ) tos ) .b bic   #2 sp add   sp )+ tos mov   NEXT end-code
extra: code *BIX        ( mask addr -- )
   sp ) tos ) .b xor>  #2 sp add   sp )+ tos mov   NEXT end-code
extra: code BIT*        ( mask addr -- x )
   tos ) tos .b mov    sp ) tos .b and>   #2 sp add   NEXT end-code

\ ----- arithmetic -----
\ --- A'dam
forth: code 1+   #1 tos add   NEXT end-code
forth: code 1-   #1 tos sub   NEXT end-code
forth: code 2*   LABEL-AMSTERDAM   tos tos add   NEXT end-code
forth: header CELLS  AMSTERDAM ,        \ end-code
forth: code 2/   tos rra   NEXT end-code
forth: code +    sp )+ tos add   NEXT end-code
forth: code -    sp )+ w mov   tos w sub   w tos mov   NEXT end-code
forth: code D2*  sp ) sp ) add   tos tos addc   NEXT end-code
forth: code D2/  tos rra   sp ) rrc   NEXT end-code
extra: code DU2/ clrc   tos rrc   sp ) rrc   NEXT end-code

\ --- R'dam
forth: code NEGATE
   LABEL-ROTTERDAM   #-1 tos xor>   #1 tos add NEXT end-code
extra: code ?NEGATE     ( x1 y -- x2 )
   #0 tos cmp   sp )+ tos mov
   pos? ROTTERDAM label-until,   NEXT end-code       \ *** pc-fout?
forth: code ABS   #0 tos cmp
   pos? ROTTERDAM label-until,   NEXT end-code       \ *** pc-fout?

\ extra: : ?NEGATE        ( x1 y -- x2 )   0< if negate then ;
\ forth: : ABS   dup ?negate ;

\ --- R'dam
forth: code DNEGATE    ( xlo xhi -- ylo yhi )
   LABEL-ROTTERDAM
   #-1 tos xor>   #-1 sp ) xor>   #1 sp ) add   #0 tos addc   NEXT end-code
extra: code ?DNEGATE
   #0 tos cmp   sp )+ tos mov
   pos? ROTTERDAM label-until,   NEXT end-code       \ *** pc-fout?
forth: code DABS   #0 tos cmp
   pos? ROTTERDAM label-until,   NEXT end-code       \ *** pc-fout?

\ extra: : ?DNEGATE ( xlo xhi y -- xlo2 xhi2 )   0< if DNEGATE then ;
\ forth: : DABS   dup ?dnegate ;

forth: code D+   ( dx dy -- dz )
   sp )+ 2 sp x) add   sp )+ tos addc   NEXT end-code

forth: : M*     ( n1 n2 -- d )
   over abs over abs um*
   2swap xor ?dnegate ;

forth: : FM/MOD ( d1 n1 -- n2 n3 )
   >r tuck              \ dhi dlo dhi
   dabs r@ abs um/mod   \ dhi r quot
   swap r@ ?negate      \ dhi quot r*
   swap rot r@ xor 0<   \ r quot neg?
   if   negate over     \ r quot* r
   if   1-              \ r quot-1
   r@ rot - swap        \ n-r quot-1
   then
   then rdrop ;
forth: : *      ( x y -- x*y )   m* drop ;
forth: : /MOD   >r s>d r> fm/mod ;
forth: : /   /mod nip ;
forth: : MOD   /mod drop ;
forth: : */MOD   >r m* r> fm/mod ;
forth: : */   */mod nip ;

\ ----- moving -----
forth: code FILL        ( c-addr u char -- )
   sp )+ sun mov        \ count
   sp )+ w mov          \ addr
   #0 sun cmp   <>? if,
   begin,   tos w ) .b mov   #1 w add   #1 sun sub
   =? until,   then,
   sp )+ tos mov   NEXT end-code
forth: code MOVE
   sp )+ w mov   sp )+ sun mov  \ sun) -> w)
   #0 tos cmp   <>? if,         \ tos = length
   w sun cmp   u>? if,          \ Backward
   tos sun add   tos w add
   begin,   #1 sun sub   #1 w sub
     sun ) w ) .b mov   #1 tos sub
   =? until,
   else,                        \ Forward
   begin,   sun )+ w ) .b mov
     #1 w add   #1 tos sub
   =? until,
   then,
   then,
   sp )+ tos mov   NEXT end-code
extra: code BOUNDS      ( a n -- a+n a )
   tos w mov   sp ) tos mov   w sp ) add   NEXT end-code
\ extra: : BOUNDS       ( a n -- a+n a )   over + swap ;

\ High level versie erbij als documentatie voor deze drie woorden ***
\ an 16jul12 -- not the usual parameters!
extra: code SKIP       ( end adr1 ch -- end adr2 )
   tos day mov   sp )+ tos mov
   ahead,
   begin, 2swap   tos ) day .b cmp
   =? while,   #1 tos add
   then,   sp ) tos cmp
   2SWAP   u<eq? until, then,   NEXT end-code   \ *** pc-fout?
<----
inside: : skip  ( end adr1 ch -- end adr2 )
   >r
   begin 2dup u>
   while dup c@ r@ =
   while 1+
   again then then rdrop ;
---->

extra: code SCAN       ( end adr1 ch -- end adr2 )
   tos day mov   sp )+ tos mov
   ahead,
   begin, 2swap   tos ) day .b cmp
   <>? while,   #1 tos add
   then,   sp ) tos cmp
   2SWAP   u<eq? until,   then,   NEXT end-code         \ *** pc-fout?
<----
inside: : scan  ( end adr1 ch -- end adr2 )
   >r
   begin 2dup u>
   while dup c@ r@ <>
   while 1+
   again then then rdrop ;
---->
extra: code s<> ( a1 n1 a2 n2 -- t/f )
  sp )+ sun mov
  sp )+ day mov
  sp )+ moon mov
  day tos cmp
  #-1 tos mov
  =? if,                                        \ n1=n2 ?
         #0 day cmp
         <>? if,                                \ n1=n2=nonzero ?
                 begin, sun )+ moon ) .b cmp    \ ch1=ch2
                        =? while, #1 moon add
                                  #1 day sub    \ ready?
                 =? until,
         2swap then,                            \ n1=n2=0 ?
         #0 tos mov
                        then,                   \ ch1<>ch2
  then,                                         \ n1<>n2
  next end-code

inside: : {FLY fp to cp ;
inside: : FLY}
( ) [ ' ROMP >BODY @ ] literal to cp ;

\ ----- catch & throw -----
\ A'dam
forth: code THROW       \ find catch-frame and restore sp and rp.
   LABEL-AMSTERDAM
   #0 tos cmp =? if, sp )+ tos mov   NEXT   then,
( ) RAM-END # w mov
   begin,   w rp cmp u>?        \ rp r0 u<
   while,   rp )+ rp cmp =?
   until,   rp )+ sp mov        \ restore sp
   rp )+ ip mov
   NEXT                         \ throw# in tos
   then,

( ) ' S0 >BODY @ & sp mov       \ init sp
( ) w rp mov                    \ init rp
( ) ' NOOP & nxt mov            \ set nxt register
    #0 sp -) mov
( ) lastname # tos mov
( ) LABEL-NOCATCHFRAME
( ) FFFF # ip mov               \ -> NOCATCHFRAME in QUIT
   NEXT end-code

inside: code ?ABORT(    ( flag -- )
   ip )+ w mov   #0 tos cmp   <>?
   if,   w tos mov   AMSTERDAM jmp   then,   \ -> THROW
   sp )+ tos mov   NEXT   end-code

\ --- A'dam \ --- R'dam
   LABEL-AMSTERDAM              \ {CATCH
( ) ROMHERE 2 + ,
   sp push
   rp w mov   w push            \ rp push is not ok
   NEXT
   LABEL-ROTTERDAM              \ CATCH}
( ) ROMHERE 2 + ,
   #4 rp add                    \ discard sp/rp frame
   tos sp -) mov   #0 tos mov NEXT
forth: : CATCH [ AMSTERDAM  , ] execute [ ROTTERDAM  , ] ;
forth: : ABORT  ( ? -- )   -1 THROW ;

<<noforth ABDE                                                  \ ============ABDE
\ ----- flashrom -----
inside: code {W ( -- )          \ Activate Flash write sequence
   dint
   fkey #   fctrl3 & mov
   fkey fwrite + # fctrl1 & mov
   NEXT end-code
inside: code W}   ( -- )        \ End Flash write sequence
   fkey # fctrl1 & mov
   fkey flock + # fctrl3 & mov
   eint
   NEXT end-code
\ Blocks are erased in quantities of 0200 or 040 bytes.
inside: code RECYCLE    ( addr -- )     \ Recycle one block of Flash
   dint
   fkey #   fctrl3 & mov
   fkey fwis + #   fctrl1 & mov
   #0   tos ) mov       ( Dummy write to erase )
   sp )+   tos mov
( ) ' W} >BODY jmp
   end-code
extra: : ROM!          ( x a -- )    {w !   w} ;
extra: : ROMC!         ( ch a -- )   {w c! w} ;
extra: : ROMMOVE ( a1 a2 len -- )   bounds ?do   count i romc! loop   drop ;
>>all

<<noforth FG                                                    \ ==============FG
extra: header ROM!      ( x a -- )      ' ! >body ,
extra: header ROMC!     ( ch a -- )     ' c! >body ,
>>all
                                                          \ ================

extra: : CHERE  ( -- a )        cp @ ;
inside: : CHERE?  ( -- chere )  cp @ dup ivecs 1- u< ?exit true ?abort ;
forth: : ,      ( x -- )        chere? rom! cell cp +! ;
forth: : C,     ( ch -- )       chere? romc! 1 cp +! ;

\ allot the string a,u. No count, no align.
extra: : M,     ( a u -- )
   4F over u< ?abort bounds ?do   i c@ c,   loop ;

forth: : ALIGN  ( -- )  chere 1 and if true c, then ;

\ C-version
extra: code LFA>N       ( lfa -- nfa )   #1 tos add   NEXT end-code
\ --- A'dam
extra: code LFA>        ( lfa -- cfa )
   #1 tos add           \ lfa>n
   tos )+ day .b mov   1F # day and>    \ wegens byte 6
   day tos add
   LABEL-AMSTERDAM
   \ #1 tos bit  #0 tos addc  NEXT end-code     \ *** an 12mei2013
   #1 day mov   tos day and>
   day tos add   NEXT end-code
\ - - -

forth: header ALIGNED  AMSTERDAM ,      \ end-code

\ --- A'dam
forth: code >BODY     LABEL-AMSTERDAM
   #2 tos add   NEXT end-code
forth: header CELL+   AMSTERDAM ,       \ end-code
\ : LFA>N   1+ ;        \ C-Version
\ : >BODY   2 + ;
\ : CELL+   2 + ;
\ : CELL-   2 - ;

extra: code CELL-       #2 tos sub   NEXT end-code

\ nieuw 08apr2015
inside: : fhere ( -- a )        \ less than hx 20 free bytes? -> reset buffer
   tib fp @ 20 + u< if here aligned fp ! then fp @ ;   \ 13

\ copy counted string to RAM if there is enough space

\ nieuw 08apr2015
inside: : >fhere        ( a n -- csa )  \ save at FHERE (unprotected)
                                        \ or abort if lack of free RAM
        fhere                   \ a n fhere
        tuck c!                 \ a fhere - store count
        tuck                    \ fhere a fhere
        count                   \ fhere a fhere+1 count
        2dup + tib u> ?abort    \ enough free ram?
        move ;  \ 13

\ msb=0 -> immediate,  lsb=1 not immediate
inside: : @IMM  ( lfa -- 1|-1 ) lfa>n c@ 7F > 2* 1+ ;

\ ----- input/output -----
forth: : KEY    ( -- c )   'key execute ;
forth: : KEY?   ( -- t/f ) 'key? execute ;
forth: : EMIT   ( c -- )   'emit execute ;
forth: : CR     ( -- )     0D emit 0A emit ;
forth: : SPACE  ( -- )     bl emit ;
forth: : SPACES ( n -- )   false ?do space loop ;
forth: : TYPE   ( a n -- ) false ?do count emit loop drop ;
\ extra: : BSPACE       ( -- )   8 bl over emit emit emit ;

forth: : ACCEPT         ( a n -- n2 )
   over + >r                    \ r: enda s: a
   dup                          \ s: a a        \ starta & actual-position
   begin key   0D over -        \ s: a a c f    \ continue?
   while 8 over =               \ s: a a c f    \ backspace?
   if   drop 2dup u<            \ s: a a f      \ not 1st position?
   if 8 bl over emit emit emit 1- then  \ s: a a
   else over r@   u<                    \ s: a a c f    \ enough space?
   if bl max dup emit over c! 1+        \ a a
   dup                          \ a a x
   then drop                    \ a a
   then
   repeat rdrop drop - negate ; \ n2

\ ----- number output -----
extra: code >DIG        ( n -- ch )
   0A # tos cmp   <eq? if,   7 # tos add   then,
   [char] 0 # tos add   NEXT end-code
<----
extra: : >DIG   ( n -- ch )
   dup 0A < 7 and + [char] 0 + ;
---->
inside: code GNIRTS     ( adr len -- )  \ retro-string, See #>
   sp )+ w mov                          \ w: adr of first chr
   #2 tos cmp u<eq?
   if,   w tos add   #1 tos sub         \ tos: adr of last chr
   begin, tos ) day .b mov
   w ) tos ) .b mov
   day w ) .b mov
   #1 w add   #1 tos sub
   tos w cmp u<eq? ( cs? )              \ halfway?
   until,
   then, sp )+ tos mov   NEXT end-code

forth: : <#     ( -- )          fhere to hld ;
forth: : HOLD   ( ch -- )       hld c! incr hld ;
forth: : SIGN   ( hi -- )       0< if [char] - hold then ;
forth: : #      ( lo hi -- lo2 hi2 )    base @ du/s >dig hold ;
forth: : #S     ( lo hi -- 0 0 )        begin # 2dup or 0= until ;
forth: : #>     ( lo hi -- a n )        2drop fhere hld over - 2dup gnirts ;

inside: : DU.STR        ( du -- a n )   <# #s #> ;

inside: : D.STR ( dn -- a n )   tuck dabs <# #s rot sign #> ;
extra: : RTYPE  ( a n r -- )    2dup min - spaces type ;
extra: : DU.    ( du -- )       du.str type space ;     \ los erbij
forth: : D.     ( d -- )        d.str   type space ;
forth: : U.     ( u -- )        false   du. ;
forth: : .      ( n -- )        s>d d.   ;
forth: : U.R    ( u r -- )      >r false   du.str r> rtype ;
forth: : .R     ( n r -- )      >r s>d d.str   r> rtype ;

forth: : DECIMAL   0A base ! ;
forth: : HEX       10 base ! ;

inside: : !INPUT        ( ib #ib >in@ -- )   >in !   to #ib   to ib ;
inside: : @INPUT        ( -- ib #ib >in@ )   ib #ib >in @ ;

\ forth: : SOURCE         ( -- adr n )   ib #ib ; \ current input buffer

forth: code /STRING     ( a n i -- a+i n-i )
   tos w mov   sp )+ tos mov   w tos sub   w sp ) add   NEXT end-code

\ forth: : /STRING        ( a n i -- a+i n-i )   tuck - >r + r> ;

inside: code UPC)       ( adr -- )      \ capitalize chr at ramadr
   tos ) day .b mov
   CHAR a 1 - # day cmp <eq?   if,   CHAR z 1 + # day cmp >? if,
   20 # day bic   day tos ) .b mov   then, then,
   sp )+ tos mov   NEXT end-code
<----
inside: : UPC) ( adr -- ) \ capitalize chr at ramadr
   dup c@
   dup [char] a [ char z 1+ ] literal within
   if bl - swap c! exit then 2drop ;
---->
extra: : UPPER ( a n -- )   over + swap ?do i upc) loop ;

\ ----- parsing -----
extra: code ?STACK      ( -- )
   #-1 day mov
( ) ' TIB/ >BODY @ & w mov   sp w cmp   u>?             \ s over
( ) if, ' S0 >BODY @ & w mov   sp w cmp   u<eq?         \ s under
       if,   rp w cmp   u>?                             \ r over
( )       if, RAM-END # w mov   rp w cmp   u<eq?   \ r under
   if, NEXT
   then, then, then, then,
   #2 ip mov   pc ip add   NEXT
   ] true ?abort                \ lo -> hi
   [ end-code

inside: : ?BASE           ( -- )
    base @ [ 61 7 - 30 - ] literal 2 within
( ) if [ ' BASE >BODY @ HOT - FROZEN + ] literal @ base !
       true ?abort
    then ;
extra: : ?COMP         ( -- )   state @ 0= ?abort ;
inside: : COMPILE(      ( -- )   inl# , ;

forth: : [      ( -- )   false state ! ; IMMEDIATE
forth: : ]      ( -- )   true state ! ;

\ FLYER for state smart words
extra: : FLYER         ( -- )
   state @ ?exit
   fhere r> 2>r         \ Address of temp. hi-level code
   {FLY                 \ Set chere to fhere
   ] dive               \ Continue caller in Fly-mode, then return here.
   postpone EXIT        \ Close temp. code with an EXIT.
   postpone [ FLY}      \ Stop compiling & repair CHERE
   ;    \ This exit jumps to the temp. code

forth: code DEPTH       ( -- n )
( ) ' S0 >BODY @ & w mov
   sp w sub
   tos sp -) mov
   w tos mov   tos rra   NEXT end-code

inside: : S"(   ( -- a n )      INLS count ;
inside: : ."(   ( -- )          INLS count type ;
\ inside: : C"(   ( -- csa )      INLS ;
\ inside:: ABORT"(       ( flag -- ) if INLS ( -2 ? ) throw then INLS drop ;

inside: : .OK   ( -- )
   ?base   ?stack
   err? 0=
   if ok 1 and if state @ if ."  ok" else ."  OK" then then     \ OK ok
      ok 2 and if ." ." depth [char] 0 + emit then              \ depth
   then
   cr
   state @ 0=
   if ok 4 and
      if r? ?dup if 0< if [char] - else [char] + then emit then \ rdepth
         base @ [char] 0 + emit ." )"                   \ base
      then
   then
   ok 0<
   if err? if 15 else 6 then emit             \ ack/nak
   then 0 to err?
    ;

forth: : REFILL ( -- t/f )
   false tib ib - ?exit
   >in !   .ok
   tib tib/ over -
   ok 2* 0<
   if 11 emit accept 13 emit     \ xon/xoff
   else accept
   then   to #ib space
   true ;
forth: code PAREA ( -- a2 a1 ) \ 14
   tos sp -) mov
   ' >in >body @ # w mov
   w )+ tos mov     \ >in @
   w )+ day mov     \ #ib
   w ) tos add      \ a1
   w ) day add      \ a2
   day sp -) mov
   next end-code
forth: : WORD ( ch -- csa ) \ 19
  >r
  parea        \ a2 a1
  r@ skip tuck \ van a2 van
  r> scan tuck \ van tot a2 tot
LABEL-AMSTERDAM
  1+ umin      \ van tot tot(+1?)
  ib - >in !   \ van tot
  over -       \ a n
  >fhere ;     \ csa
extra: : BL-WORD ( -- csa ) \ 21
  bl
  >r
  begin parea     \ a2 a1
     r@ skip tuck \ van a2 van
     2dup =       \ nulstring?
  while 2drop drop
     refill 0=
  until fhere dup dup
  then            \ van a2 van
  r> scan tuck    \ van tot a2 tot
AMSTERDAM goto (;)

forth: : CHAR   ( <name> -- ch )   bl-word count 0= ?abort c@ ;
forth: : PARSE ( ch -- a n ) \ 16
  >r
  parea     tuck \ van a2 van
  r> scan tuck \ van tot a2 tot
  1+ umin      \ van tot tot(+1?)
  ib - >in !   \ van tot
  over - ;
extra: : BEYOND ( ch -- ) \ 21
  >r
  begin parea     \ a2 a1
     r@ scan tuck \ tot a2 tot
     =            \ nodelim?
  while drop
     refill 0= ?abort
  repeat          \ tot
  rdrop
  1+
  ib - >in ! ;
forth: : (    [char] ) beyond ;   IMMEDIATE
forth: : .(     ( <txt> -- )   [char] ) parse type ; IMMEDIATE
extra: : ?ABORT ( -- ) flyer postpone ?abort(   created LFA>N   , ; IMMEDIATE

forth: : S"     ( ccc" -- a n | - )
   flyer postpone s"(
   LABEL-ROTTERDAM [char] " parse dup c, m, align ; IMMEDIATE
forth: : ."     ( ccc" -- )
   flyer postpone ."(  ROTTERDAM goto (;) IMMEDIATE

\ : C"    ( ccc" -- a | - )  flyer postpone c" ( ROTTERDAM goto (;) IMMEDIATE
\ : ABORT"  ( <txt"> -- ) flyer postpone abort"( ROTTERDAM goto (;) IMMEDIATE

\ print something between parenthesis
extra: : (.)   ( -- )  ." (" DIVE ." )" ;

\ ----- Inputstream -----

inside: code THREAD     ( bl-word -- adr )   \ thread addr
   tos )+ day .b mov    \ count
   tos )+ tos .b mov    \ ch1
   day tos xor>         \ x
   dictionary-threads 1 - # tos and>         \ n   [0,7]
   tos tos add          \ 2n
( ) HOT # tos add       \ hot+2n = threada
   NEXT end-code
<----
inside: : THREAD        ( bl-word -- adr )      \ thread addr
   count swap c@ xor dictionary-threads 1- and 2* HOT + ;
---->

inside: : CFOK ( cfa -- )       \ fill cfa if empty
   dup @                \ CFA   FFFE or FFFF or written
   cell+                \ CFA      0 or 1    or written
   dup cell u<          \ 0 or 1 ?
( ) if if   [ ' DOBODY >BODY  ] literal         \ FFFF
( )    else [ ' DO@    >BODY  ] literal         \ FFFE
     then swap rom!
        exit
   then 2drop ;

\ C-version
\ LNK@ is for byte-links
inside: code LNK@       ( lfa -- prev-lfa ) \ for FIND ^^^
   tos ) w .b mov       \ lees bytelink
   #0 w cmp
   =? if,               \ zero?
         #2 tos sub
         tos ) tos mov  \ absolute link
         NEXT
   then,
   w w add              \ cells>bytes
   w tos sub            \ relative byte link
   NEXT
   end-code
forth: : FIND   ( csa   -- csa 0 | xt imm? )    \ ^^^
   dup count upper      \ csa
   dup thread @         \ csa toplfa
                ahead
   begin lnk@   /then
   LABEL-AMSTERDAM              \ csa (top)lfa?
      dup 0= ?exit              \ csa 0
\      2dup lfa>n cs<> 0=       \ csa lfa found?
       2dup lfa>n count 1F and rot count s<> 0=         \ csa lfa found?
   until nip                    \ lfa-yes
   dup lfa>                     \ lfa cfa
   dup cfok
   swap @imm                    \ cfa imm
   ;
<---- ***
extra: : FIND-ALL       ( csa   -- csa 0 | xt imm )     \ also find iword
   find dup ?exit drop
   i-link @ AMSTERDAM goto (;)
---->
inside: : FIND-PFX   pfx-link @ AMSTERDAM goto (;)
\ - - -

forth: : LITERAL        ( x -- ? )
\   state @ if postpone sn( , then ; IMMEDIATE
   state @
   if dup 3800 C800 within
   if   postpone sn(
   else 2* 1+
   then ,
   then ; immediate
forth: : 2LITERAL       ( lo hi -- ? )
   state @
   if swap postpone literal postpone literal
   then ; immediate

\ ----- string>number -----
extra: code DIG?        ( ch base@ -- n true | char false )
   sp ) w mov           \ ch
   tos day mov          \ base@
   #0 tos mov
   3A # w cmp                   \ 9 <
   <eq? if,   41 # w cmp        \ A >
   u>? if, ( ||| )  NEXT        \ 9..A gap      \ *** pc-fout?
   else,   7 # w sub
   then,
   then,
   30 # w sub                   \ ch-30
   day w cmp                    \ base <
   u>? if,   w sp ) mov   #-1 tos mov   \ ok
   then,
   NEXT end-code

forth: : >NUMBER        ( ulo uhi a n -- ulo uhi a n )  \ string>number
   begin dup
   while over c@ base @ dig?
     0= if drop exit then
     >r 2swap base @ du*s
     r> false d+ 2swap
     1 /string
   repeat ;

<---- 18nov13 ---->
\ minus-char allowed as 1st char, comma accepted.
inside: : NUMBER?  ( a n -- lo hi false | ? ? true   )  \ signed, 0 = ok
   dup
   if over c@ [char] - = dup >r if 1 /string            \ a n   r: sign
   then
   begin dup while over c@ [char] , = while 1 /string
   again then then
   dup
   if false dup 2swap                                   \ lo hi a+ n-
      begin >number dup while over c@ [char] , =
      while 1 /string dup 0=
      until then then                                   \ lo hi a+ n-
      nip r> swap
      >r ?dnegate r>   0<> exit
   then rdrop
   then true ;
inside: : DNUM?  ( a n -- lo hi flag )  \ 0=ok
   number? ?dup ?exit postpone 2literal 0 ;
inside: : SNUM?  ( a n -- x flag )      \ 0=ok
   number? ?dup ?exit s>d - ?dup ?exit postpone literal 0 ;
extra: : DN     \ double number input
   bl-word count 2dup upper dnum? ?abort ; immediate

\ make newest word findable.
extra: : REVEAL         \ No problem if used twice on the same word
   created dup LFA>N thread ! ;         \ make findable


<<noforth FG                                                    \ ==============FG
extra: : VEC!  rom! ;
>>all                                                           \ ================


<<noforth ABDE                                                  \ ============ABDE
inside: : TIDY) ( a1 a2 n -- )
   >r   2dup r@ rommove         \ save a1,n at a2
   over recycle                 \ recycle a1-block
   swap r> rommove ;            \ restore a1,n

extra: code ROMTEST     ( -- adr )      \ *** Highlevel als commentaar
   tos sp -) mov
   FFDE # tos mov   #-1 w mov
   begin,   #2 tos sub   tos ) w cmp   <>? until,
   #2 tos add   NEXT end-code
<----
extra: : ROMTEST        ( -- adr )   FFDE begin cell- dup @ until cell+ ;
---->
\ inside: -200 constant -200 ( -rom )
inside: : BLOCKA        ( a1 -- a2 )
   1- -200 ( -rom ) and 200 ( -rom negate ) + ; \ a2 = block after a1
inside: : {VV   ivecs tib/ over negate move ;
inside: : VV}   tib/ ivecs dup negate rommove ;

\ extra: : .byte  ( u -- )  FF and 10 /mod  >dig emit  >dig emit ;
\ extra: : .word  ( u -- )  dup >< .byte .byte ;
extra: : VEC!   ( i-routine vectorlocation -- )
   dup ivecs u< ?abort
   2dup @ over and = if rom! exit then
   -200 romp u< ?abort
   {vv  tib/ + ivecs - !
   -200 recycle vv} ;
>>all                                                           \ ================

inside: : TIDY  ( -- )

<<noforth ABDE                                                  \ ============ABDE
   romtest   ROMP
   dup  1FF ( -rom invert ) and >r      \ #restbytes in here.block
   2dup u>                              \ not (1)or(2)   H u< testH
   if   blocka swap blocka over
   \ H+ testH+   not(3)   H not in last
   if over -200 ( -rom ) =              \ (4)(6)   H in last-1
     over 0=                            \ (4)(5)   testH in last
     or dup >r                          \ flag: vectors involved?
     if  {vv -200 ( -rom ) recycle
        -200 ( -rom ) or                \ FE00-block already done
     then 2dup swap
     ?do i recycle 200 ( -rom negate ) +loop
     drop r>                            \ H+ vectors?
     r@                                 \ restbytes
     if over   dup -200 ( -rom ) +   over  \ H+ H H+
        r@ TIDY)   recycle
     then
     dup if vv} then
   then
   then rdrop 2drop
   romtest  ( CR ." Romtest " DUP .WORD )  to ROMP
>>all                                                           \ ================
\ vectortest
   chere 'emit u< if ['] EMIT) to 'emit then
   chere 'key? u< if ['] KEY?) to 'key? then
   chere 'key  u< if ['] KEY)  to 'key  then
   chere app   u< if ['] NOOP  to app   then
   ivecs
   begin cell+ dup -2 u<                \ vector location
   while dup @ chere ivecs within       \ forgotten?
         if ivecs over vec! then
   repeat drop ;

extra: : FREEZE
   HOT
   [ ' state 2 + @ HOT - ] literal
   FROZEN over
   s<> 0= ?exit

   HOT FROZEN
   [ ' state 2 + @ HOT - ]       \ state is de eerste warm-only
   literal

<<noforth BDE                                                   \ =============BDE
   >r
   dup r@ + fhere               \ buffera = fhere
   80 r@ -   tidy)
   r> rommove ;
>>all

<<noforth A                                                     \ ===============A
   >r
   dup r@ + fhere               \ buffera = fhere
   40 r@ -   tidy)
   r> rommove ;
>>all

<<noforth FG                                                    \ ==============FG
   move ;
>>all                                                           \ ================

\ ----- OK loop -----
extra: : INTERPRET
   begin bl-word dup c@
   while find dup
     if   state @ = if , else execute then
     else drop count
          snum? ?abort         \ 18nov13
     then
   repeat drop ;
inside: : SWALLOW       ( -- )  \ ignore rest of a file sent without (n)ack
   ok 0< ?exit                                                  \ ***
   cr   C000
   begin 1+ ?dup
   while key? if drop key 0D =
                 if  [char] , emit
                 then C000
              then
   repeat [char] . emit ;

inside: : !CREATED      ( -- )
   false
   HOT [ dictionary-threads cells ] literal bounds
   do   i @ umax   cell +loop   to created ;

forth: : QUIT
   cr 0 to err?                                                 \ ***
   begin !r0            \ empty the return stack
   !created
   tib false dup ( ib #ib >in@ ) !input
   postpone [
   fly}
   ['] interpret catch
   dup -38 = if drop QUIT then
[
( ) ROMHERE NOCATCHFRAME 2 + !
]
   cr >in @ spaces
   dup ORIGIN ROMP within
   if dup 1 and
      if ." Msg from " then     \ ?abort nfa = odd
      dup count 7F and 20 min type
   then true over -
   if ."  \ Error # " dup u. then       \ type throw# when not -1
   drop
   -1 to err? swallow                                         \ ***
   again
(;)

\ C-version
inside: : CREATE)       \ The new word is not yet linked,
                        \ has an empty cfa.
                        \ REVEAL is done in ;  END-CODE  CREATE
                        \ msb in count byte is 1 (not immediate)
   here 1 and if incr here then
   align bl-word                \ csa (at fhere)
   dup c@ 20 1 within ?abort    \ nul-string or too long
   dup find nip         \ csa exists? ***
   if space dup count type ."  is not new "
   then                 \ csa
   dup thread @         \ csa lfa (of previous word)
\ : LNK,                ( predecessor-lfa -- )
   chere over - 2/      \ top distance/2
   dup 1 100 within and
   dup 0=               \ too far away?
   if over , then       \ insert cell-link
   c,                   \ byte-link
   drop
\ ;                     \ csa
   chere 1- to created  \ for REVEAL
   count dup 80 or      \ not immediate
   c,                   \ icount-byte
   m,                   \ name
   align
   true , ;             \ codefield
\ - - -

forth: : CREATE  create) reveal ;

\ C-version
\ CURTAIL cuts byte-link lists
inside: : CURTAIL       ( fence linkholder   --   )     \ See domarker
   tuck @       ahead
   begin lnk@   /then
         2dup u>
   until                \ linkholder fence link u>?
   nip swap ! ;

inside: :doer DOMARKER  \ ramhere and romhere order changed an 07/05/2013
   @+ to romp   @ aligned to here
   ROMP HOT [ dictionary-threads 1 + ] literal  ( threads + pfx-link )  \ ***
   false do 2dup CURTAIL   cell+ loop 2drop
   !created tidy freeze
   ;

only: : MARKER create domarker
   created ( lfa )
   dup c@ 0= if cell- then ,    \ chere
   here , ;                     \ here
only: : SHIELD create domarker chere cell+ cell+ , here , ;
\ - - -

extra: \ S? ( -- x ) \ x=0 -> switch S2 pressed

<<noforth A     : S?    08 20  bit* ;   \ SW=P3.4       >>all
<<noforth B     : S?    08 18  bit* ;   \ SW=P3.4       >>all
<<noforth D     : S?    08 20  bit* ;   \ SW=P1.4       >>all
<<noforth E     : S?    02 20  bit* ;   \ SW=P1.1       >>all
<<noforth F     : S?    02 221 bit* ;   \ SW=P4.1       >>all
<<noforth G     : S?    20 221 bit* ;   \ SW=P4.5       >>all

<<noforth BDE   extra: : !leds  ( u -- )        29 c! ; >>all   \ =============BDE

<---- 18nov13 ---->
inside: : PRET?  ( bl-word-find -- flag )    \ 0=ok
   dup
   if state @ =
      if ,
      else execute
      then 0 exit
   then drop count snum? ;

\ C-version
\ extra: : INSIDE         ( -- )  ( bl-word find-all pret? ?abort )
\ ; IMMEDIATE \ ***
\ - - -

<----
LABEL-ROTTERDAM                 \ WARM - hilevel part
   ]
   fly} postpone [
   cr
   quit
   [

inside: code WARM               \ warm inspringpunt
( ) ROMHERE WARM> 2 + !
( ) ROTTERDAM # ip mov
( ) ' S0 >BODY @ & sp mov       \ init sp
( ) RAM-END # rp mov            \ init rp
( ) ' NOOP & nxt mov            \ set nxt register
    NEXT end-code               \ jump to WARM - hilevel part
---->

LABEL-AMSTERDAM                 \ COLD - hilevel part
   ]

<<noforth ABDE  w}      >>all   \ flash write off               \ ============ABDE

   FROZEN  HOT
   state over - move            \ cold users -> warm
   !created                     \ lfa of last build word
\   fresh        \ V-version only
   here fp !                    \ reset fhere
   fly}
   S? if app execute then       \ start user application
   tidy
   cr me count type             \ show startup message
<----
   ahead
   [ romhere NOCATCHFRAME 2 + ! ]  \  coming from THROW
   cr ." Oops!"
   then
---->
   quit
   [

extra: code COLD        ( -- )  \ lolevel part
ROMHERE FFFE !
\ Prepare for executing hilevel forth
AMSTERDAM # ip mov
( ) ' S0 >BODY @
( ) HOT - FROZEN + & sp mov     \ init sp
( ) RAM-END # rp mov            \ init rp
( ) ' NOOP & nxt mov            \ set nxt register

\ hardware initialisation

<<noforth A \ Launchpad MSP430G2553 hardware stuff              \ ===============A
\ stop watchdog & set oscillator freq.
   5A80 # wdctrl & mov          \ stop watchdog
   10FD & clkctrl & .b mov      \ set DCO to 8 MHz
   10FC & dcoctrl & .b mov
\ uart setup starts with i/o pin direction
   6 # tos mov
   tos p1sel & .b mov           \ init p1.1 & p1.2
   tos p1sel2 & .b mov
   80 # uartctrl1 & .b bis      \ uart use smclk
   41 # uartbaud0 & .b mov      \ 8 mhz, baudrate 9600
   3 # uartbaud1 & .b mov       \ idem
   #4 uartmodctrl & .b mov      \ modulation ucbrsx = 2
   #1 uartctrl1 & .b bic        \ initialize usci state machine
   #0 intenable & .b mov        \ erase interrupt flags ***
\ flash initialisation
   A550 # fctrl2 & mov          \ mclk/20 for flash timing ***
\ activate I/O ports, leds & switch
   71 # 22 & .b bis             \ Was 41, set output dir for p1.0 and p1.6
   79 # 21 & .b bis             \ Was 49, set p1.0, 4, 5 en ..
   #8 27 & .b bis               \ p1 pullup p1.3
   #-1 2A & .b bis              \ p2 all outputs
\ Updated version for two port Launchpad
\   F1 # 22 & .b bis            \ Set input dir for p1.1 to p1.3
\   41 # 21 & .b bis            \ Set p1.0 and p1.6
\   #8 27 & .b bis              \ Activate p1.3 resistor
\   #-1 2A & .b bis             \ p2 to all outputs
>>all

<<noforth B                                                     \ ===============B
    5A80 # WDctrl & mov     \ watchdog uit
    #0 IE1 & .b mov         \ erase interrupt flags

\ Activate I/O ports, leds & switch
    40 # P3dir & .b mov     \ Output dir for P3
    FF # P2dir & .b mov     \ Output dir for P2
\   11 #  32 & .b bis       \ P5.4 & P5.0 = output

\   #1  31 & .b bis         \ P5.0 high
\ Klok selectie en test
    80 # CLKctrl1 & .b bic  \ XT2 aan
    BEGIN,                  \ Wacht tot osc op gang is.
        #2 IFG1 & .b bic    \ Reset osc. foutvlag
        FF # sun mov        \ Start wachtlus
        BEGIN,
            #-1 sun add
        =? UNTIL,
        #2 IFG1 & .b bit    \ Stop als de vlag nul is
    cc? UNTIL,
    88 # CLKctrl2 & .b bis  \ Mclk = SMclk = XT2
    3C # P2out & .b mov     \ Set 4 leds on

\ 8 MHz op P5.4 naar buiten
\   10 #  33 & .b bis       \ P5.4 = MCLK option select
\   #1  31 & .b bic         \ P5.0 low

\ RS232
    C0 # P3sel & .b bis     \ Use Uart-1
    30 # ME2 & .b mov       \ Uart-1 aan
    10 # U1CTL & .b bis     \ 8-bit karakters
    20 # U1TCTL & .b bis    \ Uclk = SMclk
    41 # U1BR0 & .b mov     \ 8 MHz: baudrate 9600
    03 # U1BR1 & .b mov     \ idem
    09 # U1MCTL & .b mov    \ Adjust modulation
\   45 # U1BR0 & .b mov     \ 8 MHz: baudrate = 115200
\   #0 U1BR1 & .b mov
\   #0 U1MCTL & .b mov      \ ...
    #1 U1CTL & .b bic       \ Init. UART

\ flash initialisation
    A554 # fctrl2 & mov     \ mclk/xx for flash timing ***
>>all

<<noforth D                                                     \ ===============D
    5A80 # WDctrl & mov     \ watchdog uit
    #0 IE1 & .b mov         \ erase interrupt flags

\ Activate I/O ports, leds & switch
\   40 # P3dir & .b mov     \ Output dir for P3
    FF # P2dir & .b mov     \ Output dir for P2

\   11 #  32 & .b bis       \ P5.4 & P5.0 = output
\   #1  31 & .b bis         \ P5.0 high
\ Klok selectie en test
    80 # CLKctrl1 & .b bic  \ XT2 aan
    BEGIN,                  \ Wacht tot osc op gang is.
        #2 IFG1 & .b bic    \ Reset osc. foutvlag
        FF # sun mov        \ Start wachtlus
        BEGIN,
            #-1 sun add
        =? UNTIL,
        #2 IFG1 & .b bit    \ Stop als de vlag nul is
    cc? UNTIL,
    88 # CLKctrl2 & .b bis   \ Mclk = SMclk = XT2
    3C # P2out & .b mov      \ Set 4 leds on

\ 8 MHz op P5.4 naar buiten
\   10 #  33 & .b bis        \ P5.4 = MCLK option select
\   #1  31 & .b bic          \ P5.0 low

\ RS232
    30 # P3sel & .b bis      \ Use Uart-0
    C0 # ME1 & .b mov        \ Uart-0 aan
    10 # U0CTL & .b bis      \ 8-bit karakters
    20 # U0TCTL & .b bis     \ Uclk = SMclk
    41 # U0BR0 & .b mov      \ 8 MHz: baudrate 9600
    03 # U0BR1 & .b mov      \ idem
    09 # U0MCTL & .b mov     \ Adjust modulation
\   45 # U0BR0 & .b mov      \ 8 MHz: baudrate = 115200
\   #0 U0BR1 & .b mov
\   #0 U0MCTL & .b mov       \ ...
    #1 U0CTL & .b bic        \ Init. UART

\ flash initialisation
    A554 # fctrl2 & mov      \ mclk/xx for flash timing ***
>>all

<<noforth E                                                     \ ===============E
    5A80 # WDctrl & mov     \ watchdog uit
    #0 IE1 & .b mov         \ erase interrupt flags

\ Activate I/O ports, leds & switch
\   40 # P3dir & .b mov     \ Output dir for P3
    FF # P2dir & .b mov     \ Output dir for P2

\   11 #  32 & .b bis       \ P5.4 & P5.0 = output
\   #1  31 & .b bis         \ P5.0 high
\ Klok selectie en test
    80 # CLKctrl1 & .b bic  \ XT2 aan
    BEGIN,                  \ Wacht tot osc op gang is.
        #2 IFG1 & .b bic    \ Reset osc. foutvlag
        FF # sun mov        \ Start wachtlus
        BEGIN,
            #-1 sun add
        =? UNTIL,
        #2 IFG1 & .b bit    \ Stop als de vlag nul is
    cc? UNTIL,
    88 # CLKctrl2 & .b bis   \ Mclk = SMclk = XT2
    06 # P2out & .b mov      \ Set 2 leds on

\ 8 MHz op P5.4 naar buiten
\   10 #  33 & .b bis        \ P5.4 = MCLK option select
\   #1  31 & .b bic          \ P5.0 low

\ RS232
    30 # P3sel & .b bis      \ Use Uart-0
    C0 # ME1 & .b mov        \ Uart-0 aan
    10 # U0CTL & .b bis      \ 8-bit karakters
    20 # U0TCTL & .b bis     \ Uclk = SMclk
    41 # U0BR0 & .b mov      \ 8 MHz: baudrate 9600
    03 # U0BR1 & .b mov      \ idem
    09 # U0MCTL & .b mov     \ Adjust modulation
\   45 # U0BR0 & .b mov      \ 8 MHz: baudrate = 115200
\   #0 U0BR1 & .b mov
\   #0 U0MCTL & .b mov       \ ...
    #1 U0CTL & .b bic        \ Init. UART

\ flash initialisation
    A554 # fctrl2 & mov      \ mclk/xx for flash timing ***
>>all

<<noforth F     \ MSP430FR5739 hardware stuff                   \ ===============F
\ Cold code is now 23 instructions and all I/O is initialised
   5A80 # 15C & MOV             \ Stop watchdog
   A5 # 161 & .B MOV            \ Set DCO to 8 MHz
   6 # 162 & BIS
   133 # 164 & MOV
   #0 166 & MOV
   #1 161 & .B MOV
\   #8 1B0 & MOV                \ Bandgap ref. off (saves power)
   3 # 223 & .B MOV             \ P4: switches
   3 # 227 & .B MOV
   F8 # 224 & .B MOV            \ P3: 4 leds, LDR & accelerometer
   0F # 324 & .B MOV            \ PJ: 4 leds
   F0 # 326 & .B MOV
   F0 # 322 & .B MOV
   #-1 206 & .B MOV             \ P1: bit 4 is NTC, rest NC
   #-1 202 & .B MOV
   80 # 205 & .B MOV            \ P2: RS232 & NTC/LDR voeding bit 7
   7F # 203 & .B MOV
   7C # 207 & .B MOV
   3 # 20D & .B BIS             \ Set UART function to pins
\ Configure UART:
   80 # 5C0 & .B MOV            \ Select clock source
   34 # 5C6 & .B MOV            \ Set baud 0
   #0 5C7 & .B MOV              \ Set baud 1
   4911 # 5C8 & MOV             \ Set modulation control
>>all

<<noforth G     \ MSP430FR5969 hardware stuff                   \ ===============G
\ Cold code is now 23 instructions and all I/O is initialised
   5A80 # 15C & MOV             \ Stop watchdog
   A5 # 161 & .B MOV            \ Set DCO to 8 MHz
   6 # 162 & BIS
   133 # 164 & MOV
   #0 166 & MOV
   #1 161 & .B MOV
\   #8 1B0 & MOV                \ Bandgap ref. off (saves power)

   20 # 223 & .B MOV            \ P4.5 switch 1
   20 # 227 & .B MOV
   02 # 202 & .B MOV            \ P1.1 switch 2
   02 # 206 & .B MOV

   40 # 223 & .B BIS           \ Led 1 on P4.6
   40 # 225 & .B BIS
   01 # 202 & .B BIS           \ Led 2 on P1.0
   01 # 204 & .B BIS

\  #FE 206 & .B MOV             \ P1: bit 4 is NTC, rest NC
\  #-1 202 & .B MOV
\  80 # 205 & .B MOV            \ P2: RS232 & NTC/LDR voeding bit 7
\  7F # 203 & .B MOV
\  7C # 207 & .B MOV
   03 # 20D & .B BIS            \ Set UART function to pins
\ Configure UART:
   80 # 5C0 & .B MOV            \ Select clock source
   34 # 5C6 & .B MOV            \ Set baud 0
   #0 5C7 & .B MOV              \ Set baud 1
   4911 # 5C8 & MOV             \ Set modulation control
>>all                                                           \ ================

NEXT end-code   \ jump to COLD - hilevel part

forth: : EVALUATE       ( a n -- )
   @input ( ib #ib >in@ ) >r 2>r
   false ( ib #ib >in@ ) !input
   ['] interpret catch
   2r> r> ( ib #ib >in@ ) !input
   throw ;
forth: : '      ( <name> -- xt | ABORT )   bl-word find 0= ?abort ;
forth: : [CHAR] ( <name> -- )   char postpone literal ; IMMEDIATE
extra: : CH     ( <name> -- )   char postpone literal ; IMMEDIATE

\ extra: : CTRL   char 1F and postpone literal ; IMMEDIATE

forth: : \      ( <tekst> -- )   #ib >in ! ;   IMMEDIATE

\ nieuw 07apr2015
forth: : allot  ( n -- )        \ 07apr2015
   here over +   tib 20 - u> ?abort
   created lfa> @+ + 1+ chere =         \ cfa@ is -1 and chere is pfa
   if -2 chere cell- rom!
   here ,
   then +to here
   fp @ here umax aligned fp !  ;       \ 32

forth: : :      ( <name> -- sys\: )
   create)
( ) [ ' DOCOL >BODY ] literal
   chere cell- dup s0 ! rom!
   0 44 ] ;             \ S0 for recurse
forth: : CONSTANT       ( x <name> -- )   create do@ , ;        \ *** an 12jun13
forth: : VARIABLE       ( <name -- )   create cell allot ;

forth: : VALUE  ( <name> -- )   variable do@@ ;
forth: : CODE   ( <name> -- sys\code )
   create) chere -2 cp +! , 0 55 ;      \ C-version
\ C-version
inside: :DOER DOPREFIX  ( pfx# <name> -- )      \ an 27apr2014 ***
   @ ' >r                       \ pfx# r: data-xt
   r@ >body swap                \ rom pfx#
   r> @ +                       \ rom pfx-id
   fhere 1+                     \ for 'name' as counted string
        2 over c!               \ count
        tuck 1+ !               \ 'name'
   find-pfx   dup 0= ?abort     \ rom pfx-cfa not-imm?
   0<
   flyer
   if  @ , @ ,                  \ compile action and ramlocation
   else @ execute               \ execute action ( with rombody on stack )
   then ;
\ - - -

\ global prefixes
forth: to#    prefix TO   \ 0
forth: +to#   prefix +TO  \ 2
forth: incr#  prefix INCR \ 4

\ ----- conditionals -----

\ --- A'dam
forth: : IF     ( -- a sys\if ) chere 8FFF , 11 ; immediate
forth: : AHEAD  ( -- a sys\if ) chere 7FFF , 11 ; immediate

\ ---
forth: : WHILE  ( a x-id -- a sys\if a x-id )   postpone if 2swap ; IMMEDIATE
inside: : DIST ( x -- )    801 - dup -1000 u< ?abort ;
extra: : ?pair ( x y -- )      - ?abort ;

forth: : THEN   ( a sys\if -- )  \ &&&
   ?comp 11 ?pair
   chere over -         \ distance, positive
   dist                 \ [F800,0)
   over @ and swap rom!
   ; IMMEDIATE

forth: : BEGIN  ( -- a sys\begin ) ?comp chere 22 ; IMMEDIATE
forth: : UNTIL  ( a sys\begin -- )
   8FFF
   LABEL-AMSTERDAM
   >r  22 ?pair
   chere -              \ distance, negative
   dist                 \ [F000,F800)
   r>  and ,  ; IMMEDIATE
forth: : AGAIN  ( a sys\begin -- )
   7FFF AMSTERDAM goto (;) IMMEDIATE
forth: : ELSE   ( a sys\if1 -- a sys\if2 )
   postpone ahead 2swap postpone then ; IMMEDIATE
forth: : REPEAT ( a sys\if a sys\begin -- )
   postpone again postpone then ; IMMEDIATE

\ --- A'dam
forth: : DO     ( -- adr sys\do )
   postpone do(
   LABEL-AMSTERDAM
   chere 33 true , ; IMMEDIATE
forth: : ?DO    ( -- adr sys\do )
   postpone ?do(   AMSTERDAM goto (;) IMMEDIATE

\ --- A'dam
forth: : LOOP   ( sys\do -- )
   postpone loop)
   LABEL-AMSTERDAM
   33 ?pair   chere swap rom!
   ; IMMEDIATE

forth: : +LOOP  ( sys\do -- )
   postpone +loop)   AMSTERDAM goto (;) IMMEDIATE
\ ---

forth: : ;      ( sys\: -- )
   44 ?pair postpone exit   postpone [
   [ immediate ]   \ !!! to prevent ; to be compiled at the end of this word
   ?exit reveal ;               \ :noname has sys = -1 44
forth: : END-CODE       ( sys\code -- )
   55 ?pair ?exit reveal ;      \ C-version

inside: : !DOER ( DOERa -- )
   created lfa>
   dup @ -2 u< ?abort   \ cf already filled
   rom! ;
( ) ' !DOER THINGUMAJIG !       \ Patch in DODOER (Forward reference)

forth: : IMMEDIATE
   created lfa>n 7F over c@ and swap romc! ;

inside: :DOER DOFFBASE  base @ >r   @ base !
   bl-word find pret? r> base ! ?abort ;
extra: : FFBASE ( tempbase <name> -- ) create , doffbase immediate ;

inside: : DOES>)        ( -- ) r> !doer ;
\ inside: : DOES,       ( -- )
\ 4030 ( # mov pc ) , [ doesintro ] literal , ;
forth: : DOES>  ( sys\: -- sys\: )
   dup 44 ?pair   postpone does>)
   4030 ( # pc mov ) , [ doesintro ] literal ,    \ does,
   0 s0 !           \ for recurse
   ; immediate

forth: : [']    ( <name> -- )   ' postpone literal ; IMMEDIATE
forth: : POSTPONE       ( <name> -- )
   ?comp
   bl-word find dup 0= ?abort
   0< if compile( compile(   then   , ; IMMEDIATE

forth: : MS    ( n -- ) 0 ?do ms) 0 ?do loop  loop ;
forth: 10 ffbase HX
forth: 0A ffbase DM
forth: 02 ffbase BN
only: : C: ; immediate
only: : V: postpone \ ; immediate


\ C-version
forth: shield NOFORTH
\ - - -
\ ---------------------------------- noforth C Tools
extra: : STOP?     ( - true/false )
   key? dup 0= ?EXIT  drop
   key  bl over = if drop key then
   01B over = ?abort
   bl <> ;
\ .CH is needed for DUMP MSEE & SEE
extra: : .CH   ( x -- ) dup 7F < and bl max emit ;

\ C-version only
<---- ***
extra: : TICK  ( -- xt )   bl-word find-all 0= ?abort ;
---->
\ - - -
forth: : .S        ( -- )
   ?stack (.) space
   depth false
   ?do  depth i - 1- pick
      base @ 0A = if . else u. then
   loop ;

\ ----- WORDS IWORDS using STOP?
forth: : WORDS     ( -- )
   sn( [ ' 0= , ]
   \   begin ( for IWORDS )
   >r
   false >r                     \ counting
   fhere pfx-link hot - hot dup
   2over move drop              \ threads > fhere
   bounds
   cr
   BEGIN false dup              \ threada lfa
      2over
      do
         dup i @ u< if 2drop i dup @ then       \ threada lfa
      2 +loop dup stop? 0= and
   WHILE        \ threada lfa
        dup 1+ c@ 40 and
        2r@ drop execute        ( 0= for words, noop for iwords *** )
        if r> 1+ >r             \ counting
           dup lfa>n count 1F and
           0C over 2swap space type
           - dup 0<   r@ 6 mod 0=   or       \ too long or too many
           if drop cr else spaces then
        then
        lnk@ swap !          \ unlink
   REPEAT
   2drop 2drop 2r> (.) 0 .r drop ;

forth: : IWORDS         ['] noop [ ' words 6 + 22 ] again ;
\ forth: : ALLWORDS       ['] 1+   [ ' words 6 + 22 ] again ;

forth: : DUMP  ( a n -- )       \ n must be in [0,7FFF]
   over + 1- >r                                 \ r: enda-1
   0FF s>d du.str nip 1+ swap                   \ .r a
   base @ swap
   BEGIN cr dup 2over drop 2* u.r ." : "        \ .r base a
      over s>d do count 2over drop .r
               loop over - ."  |"               \ .r base a
      over s>d do count .ch loop ." |"          \ .r base a+
      r@ over - 0< stop? or
   UNTIL r> 2drop 2drop ;

\ ----- SEE & MSEE
extra: : >NFA          ( a -- nfa | 0 )
   dup origin
   chere within                 \ in noForth ROM?
   IF dup 1 and 0=              \ even?
   IF 1- dup c@ FF = +          \ skip alignment char
   0 begin over c@ 21 7F within \ char?
     while -1 /string 20 over < \ walk backwards through name
     until then ?dup            \ nfa? count-non-zero?
   IF over c@ 3F and =          \ count ok?
   IF dup 1 and ?EXIT           \ nfa odd -> ok
   THEN THEN THEN THEN drop 0 ;
extra: : DECOM         ( a -- )
   cr dup 6 u.r space   \ .adr
   dup count .ch c@ .ch \ .2chars
   dup @ 6 u.r space    \ .contents
   dup >nfa ?dup
   if ." ----- "
      count tuck 1F and type            \ .name
      80 < if ."  (imm)" then
      @ cell- >nfa ?dup
      if ."  does " count 1F and type   \ .doer
      then EXIT                         \ ----
   then dup @                           \ contents
   dup 1 and                            \ odd
   if dup -7800 < if ." UNTIL -" else
      dup -7000 < if ." IF +"   else
      dup  7000 < if 2/ ." #" u. drop EXIT then    \ a number
      dup  7800 < if ." AGAIN -" else ." AHEAD +"
      then then then FFF and 7FF - + ." > " u. EXIT
   then nip   dup >nfa ?dup
   if count 1F and type drop EXIT       \ .name of compiled word
   then dup hot here within             \ RAM location?
   if origin
      begin begin cell+ chere over u<
                  if 2drop EXIT
                  then 2dup @ =
         until dup cell- >nfa ?dup
            if count 1F and type ."   RAM location "
            then
      again
   then
   4F00 = if ." nxt jmp "  then
   ;
forth: : MSEE  ( a -- ) FFFE and begin dup decom cell+ stop? until drop ;
forth: : SEE   ( <name> -- )  ' msee ; \ ***

extra: : MANY  ( -- )  >in @ stop? and >in ! ;

forth: shield TOOLS\
( ) ROMHERE ' ROMP >BODY @ !
\ - - -

;;;NOFORTH;;;
ihx
<---- until the end

   ANS:
Throw#   Reserved for

 -1   ABORT
 -2   ABORT"
 -3   stack overflow
 -4   stack underflow
 -5   return stack overflow
 -6   return stack underflow
 -7   do-loops nested too deeply during execution
 -8   dictionary overflow
 -9   invalid memory address
-10   division by zero
-11   result out of range
-12   argument type mismatch
-13   undefined word
-14   interpreting a compile-only word
-15   invalid FORGET
-16   attempt to use zero-length string as a name
-17   pictured numeric output string overflow
-18   parsed string overflow
-19   definition name too long
-20   write to a read-only location
-21   unsupported operation (e.g., AT-XY on a too-dumb terminal)
-22   control structure mismatch
-23   address alignment exception
-24   invalid numeric argument
-25   return stack imbalance
-26   loop parameters unavailable
-27   invalid recursion
-28   user interrupt
-29   compiler nesting
-30   obsolescent feature
-31   >BODY used on non-CREATEd definition
-32   invalid name argument (e.g., TO xxx)
-33   block read exception
-34   block write exception
-35   invalid block number
-36   invalid file position
-37   file I/O exception
-38   non-existent file
-39   unexpected end of file
-40   invalid BASE for floating point conversion
-41   loss of precision
-42   floating-point divide by zero
-43   floating-point result out of range
-44   floating-point stack overflow
-45   floating-point stack underflow
-46   floating-point invalid argument
-47   compilation word list deleted
-48   invalid POSTPONE
-49   search-order overflow
-50   search-order underflow
-51   compilation word list changed
-52   control-flow stack overflow
-53   exception stack overflow
-54   floating-point underflow
-55   floating-point unidentified fault
-56   QUIT
-57   exception in sending or receiving a character
-58   [IF], [ELSE], or [THEN] exception
---->
