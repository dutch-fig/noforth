\ Tools for noForth V
\ (C) 2015, Albert Nijhof & Willem Ouwerkerk

\ updated april 2015

HEX     \ until end of file!
fresh inside
: stop?     ( - true/false )
   key? dup 0= ?exit  drop
   key  bl over = if drop key then
   01B over = ?abort
   bl <> ;
\ .ch is needed for dump msee & see
: .ch   ( x -- ) dup 7F < and bl max emit ;

: .s        ( -- )
   ?stack (.) space
   depth false
   ?do  depth i - 1- pick
      base @ 0A = if . else u. then
   loop ;

\ ----- words
only definitions forth also extra also inside
: words     ( -- )
   false >r                     \ counting
   fhere pfx-link hot - hot dup
   2over move drop              \ threads > fhere
   bounds
   cr
   begin false dup              \ threada lfa
      2over
      do
         dup i @ u< if 2drop i dup @ then       \ threada lfa
      2 +loop dup stop? 0= and
   while        \ threada lfa
        dup @voc vp c@ =        \ the right vocabulary?
        if r> 1+ >r             \ counting
           dup lfa>n count 1F and
           0C over 2swap space type
           - dup 0<   r@ 6 mod 0=   or       \ too long or too many
           if drop cr else spaces then
        then
        @ swap !          \ unlink
   repeat
   2drop 2drop r> (.) 0 .r  ;
\ - - -
fresh inside
: dump  ( a n -- )       \ n must be in [0,7FFF]
   over + 1- >r                                 \ r: enda-1
   0FF s>d du.str nip 1+ swap                   \ .r a
   base @ swap
   begin cr dup 2over drop 2* u.r ." : "        \ .r base a
      over s>d do count 2over drop .r
               loop over - ."  |"               \ .r base a
      over s>d do count .ch loop ." |"          \ .r base a+
      r@ over - 0< stop? or
   until r> 2drop 2drop ;

\ ----- see & msee
fresh inside definitions
: >nfa          ( a -- nfa | 0 )
   dup origin
   chere within                 \ in noforth rom?
   if dup 1 and 0=              \ even?
   if 1- dup c@ FF = +          \ skip alignment char
   0 begin over c@ 21 7F within \ char?
     while -1 /string 20 over < \ walk backwards through name
     until then ?dup            \ nfa? count-non-zero?
   if over c@ 7F and =          \ count ok?
   if dup 1 and ?exit           \ nfa odd -> ok
   then then then then drop 0 ;
: decom         ( a -- )
   cr dup 6 u.r space   \ .adr
   dup count .ch c@ .ch \ .2chars
   dup @ 6 u.r space    \ .contents
   dup >nfa ?dup
   if ." ----- " dup 1- c@ 7F and .voc  \ v-version only
      count tuck 7F and type            \ .name
      80 < if ."  (imm)" then
      @ cell- >nfa ?dup
      if ."  does " count 7F and type   \ .doer
      then exit                         \ ----
   then dup @                           \ contents
   dup 1 and                            \ odd
   if dup -7800 < if ." until -" else
      dup -7000 < if ." if +"   else
      dup  7000 < if 2/ ." #" u. drop exit then    \ a number
      dup  7800 < if ." again -" else ." ahead +"
      then then then FFF and 7FF - + ." > " u. exit
   then nip   dup >nfa ?dup
   if count 7F and type drop exit       \ .name of compiled word
   then dup hot here within             \ ram location?
   if origin
      begin begin cell+ chere over u<
                  if 2drop exit
                  then 2dup @ =
            until dup cell- >nfa ?dup
            if count 7F and type ."   ram location "
            then
      again
   then
   4F00 = if ." nxt jmp "  then
   ;
fresh inside
: msee  ( a -- ) FFFE and begin dup decom cell+ stop? until drop ;
: see   ( <name> -- )  ' msee ;

: many  ( -- )  >in @ stop? and >in ! ;
fresh
shield tools\ freeze

\ <><>
