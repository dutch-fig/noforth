Poortstructuur MSP430 100 serie (Op goedkope Chinese printen):

De I/O poorten van de MSP430 hebben een vaste structuur.
Een basisadres en een offset tot elk besturingsregister.
De basis adressen voor een MSP vindt je in zijn persoonlijke datasheet.
De adressen van de IO-poorten zijn:

Functie         P1    P2    P3    P4    P5    P6  Label naam
-------------------------------------------------------------
Ingang          20    28    18    1C    30    34    PxIN
Uitgang         21    29    19    1D    31    35    PxOUT
Richting        22    2A    1A    1E    32    36    PxDIR
Int. vlag       23    2B    1B    1F    33    37    PxIFG
Int. edge aan   24    2C    --    --    --    --    PxIES
Int. aan        25    2D    --    --    --    --    PxIE
Select          26    2E    --    --    --    --    PxSEL
---------------------------------------------------------

De meeste registers worden op nul geinitialiseerd, lees
het datasheet van de gebruikte MSP voor meer details.

Als een I/O-pin niet aangesloten wordt beveelt TEXAS aan
de pin als uitgang te configureren.


De drie registers PxDIR, PxREN en PxOUT worden gebruikt 
om een I/O-pin als volgt te configuren:

PxDIR     PxOUT   Pin configuratie
-------------------------------------------------
  0         x      Zwevende ingang
  1         x      Uitgang
-------------------------------------------------


De registers PxSEL0 en PxSEL2 worden gebruikt om een
speciale functie aan een I/O-pin te koppelen. Hiermee
kan b.v. de ADC of de UART ingeschakeld worden. In het
datasheet van de gebruikte MSP is meer info te vinden.
Om precies te zijn bij de poort schemas van de I/O-poorten.
Bij de MSP430F149 begint dat op blz: 40 over de P1-functies.

Als volgt:

 PxSEL       I/O-functie
------------------------------------------
   0         Gewone I/O
   1         Basis extra functie
   0         Controller specifiek!
   1         Tweede extra functie
------------------------------------------


Interrupt vectoren MSP430F149  (China print)
FFDE    - EINDE FLASH
FFE0    - ...
FFE2    - P2
FFE4    - USART1 TX
FFE6    - USART1 RX
FFE8    - P1
FFEA    - TIMER A3 CCR1 CCR2
FFEC    - TIMER A3 CCR0  
FFEE    - ADC12  
FFF0    - USART0 TX
FFF2    - USART0 RX
FFF4    - WATCHDOG
FFF6    - COMPARATOR
FFF8    - TIMER B7 CCR1 CCR2 CCR3 ...
FFFA    - TIMER B7 CCR0
FFFC    - NMI
FFFE    - RESET


\ IO-poort layout MSP430F149  (China print)
P1.0    - ...
P1.1    - Bootloader TX
P1.2    - ...
P1.3    - ...
P1.4    - ...
P1.5    - ...
P1.6    - ...
P1.7    - ...

P2.0    - Led
P2.1    - Led
P2.2    - Led/Bootloader RX
P2.3    - Led
P2.4    - Led
P2.5    - Led
P2.6    - Led
P2.7    - Led

P3.0    - ...
P3.1    - ...
P3.2    - SW K1
P3.3    - SW K2
P3.4    - SW K3/TX0
P3.5    - RX0
P3.6    - TX1
P3.7    - RX1

P4.0    - ...
P4.1    - ...
P4.2    - ...
P4.3    - ...
P4.4    - ...
P4.5    - ...
P4.6    - ...
P4.7    - ...

P5.0    - ...
P5.1    - ...
P5.2    - ...
P5.3    - ...
P5.4    - ...
P5.5    - ...
P5.6    - ...
P5.7    - ...

P6.0    - ...
P6.1    - ...
P6.2    - ...
P6.3    - ...
P6.4    - ...
P6.5    - ...
P6.6    - ...
P6.7    - ...
