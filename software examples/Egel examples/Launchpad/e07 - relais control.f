( For noForth C2553 lp.0, C&V version:
  Port input at P1.3 and output at P2.2 with MSP430G2553 )

( P2.2 must be wired to transistor, placed on the launchpad experimenters kit.
  Wire P2.2 to the base of a BC549C using a 1K resistor. The emitter to ground
  and the collector to a relais and the anode of a suppression diode {1N4001}. 
  Connect the other side of the relais and diode to each other and VCC. 
  Take care that the relais does not pull to much current. An USB power supply
  usually gives up at about 300 mA. Port-1 bit 3 holds a switch on the 
  Launchpad board. This is a delayed on, off and on/off example!!
  )

( Address 020 = p1in, port-1 input register
  Address 022 = p1dir, port-1 direction register
  Address 029 = p2out, port-2 output with 8 leds
  Address 02A = p2dir, port-2 direction register
  Address 02E = p2sel, port-2 selection register 
)

HEX
: RELAIS        04 029 ;                \ Relais output P2.2
: SWITCH        08 020 ;                \ Switch input P1.3

: RELAIS-ON     relais *bis ;
: RELAIS-OFF    relais *bic ;
: SETUP-RELAIS  4 02A *bis relais-off ; \ Start with relais off
    
\ Relais control demonstration, debounced input by securing
\ that the switch is on for at least 100 ms
: READ-INPUT        ( -- flag )         \ Sample switch
    0                                   \ Begin value
    14 0 do                             \ Take 20 samples
        5 ms  switch bit* 0=  -         \ Wait, then count sample to value
    loop
    14 = ;                              \ All 20 samples true?

: RELAIS-OFF-DELAY  ( -- )              \ Relais quick on, and slow off 
    read-input if                       \ Switch pressed?
        relais-on                       \ Yes, ...
        begin  read-input 0= until      \ Switch released?
        dm 4000 ms  relais-off          \ Yes, after 4 sec. relais off
    then ;

: RELAIS-ON-DELAY   ( -- )              \ Relais slow on, and quick off 
    read-input if                       \ Switch pressed?
        dm 1000 ms  relais-on           \ Yes, wait 1 sec. then switch on
        begin  read-input 0= until      \ Switch released?
        relais-off                      \ Yes, relais off
    then ;

: RELAIS-ON/OFF-DELAY ( -- )            \ Relais slow on, and off 
    read-input if                       \ Switch pressed?
        dm 1000 ms  relais-on           \ Yes, wait 1 sec. then switch on
        begin  read-input 0= until      \ Switch released?
        dm 4000 ms  relais-off          \ Yes, after 4 sec. relais off
    then ;

\ Choose one of three relais examples
: RELAIS-CONTROL    ( -- )              \ Control relais using switch S2
    setup-relais                        \ Set relais off
    8 22 *bic  8 27 *bis                \ Input P1.3 with pullup
    dm 500 ms  relais-on                \ Show startup by switching 
    dm 1000 ms  relais-off              \ relais on and off
    begin  relais-off-delay  key? until ;
\   begin  relais-on-delay  key? until ;
\   begin  relais-on/off-delay  key? until ;

setup-relais  freeze

                    ( End )
